package com.techxi.goodgut.clients.model;


import java.util.Date;

import com.techxi.goodgut.clients.common.PayType;
import com.techxi.goodgut.clients.common.SubscriptionOrderStatus;



public class SubscriptionOrder {

    private int orderId;
    private Subscription subscription;
    private User user;
    private SubscriptionOrderStatus subscriptionOrderStatus;
    private Date subscriptionOrderDate;
    private Date subscriptionStartDate;
    private Date subscriptionEndDate;
    private boolean couponApplied;
	private int discountAmount;
	private String couponId;
    private String subscriptionAddr;
    private PayType payType;

    //formateddates
    private String formatedSubOrderDate;
    private String formatedSubStartDate;
    private String formatedSubEndDate;
    
    public SubscriptionOrder() {
        super();
    }

    public String getFormatedSubOrderDate() {
		return formatedSubOrderDate;
	}

	public void setFormatedSubOrderDate(String formatedSubOrderDate) {
		this.formatedSubOrderDate = formatedSubOrderDate;
	}

	public String getFormatedSubStartDate() {
		return formatedSubStartDate;
	}

	public void setFormatedSubStartDate(String formatedSubStartDate) {
		this.formatedSubStartDate = formatedSubStartDate;
	}

	public String getFormatedSubEndDate() {
		return formatedSubEndDate;
	}

	public void setFormatedSubEndDate(String formatedSubEndDate) {
		this.formatedSubEndDate = formatedSubEndDate;
	}

	public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getSubscriptionStartDate() {
        return subscriptionStartDate;
    }

    public void setSubscriptionStartDate(Date subscriptionStartDate) {
        this.subscriptionStartDate = subscriptionStartDate;
    }

    public String getSubscriptionAddr() {
        return subscriptionAddr;
    }

    public void setSubscriptionAddr(String subscriptionAddr) {
        this.subscriptionAddr = subscriptionAddr;
    }

    public Date getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(Date subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }

    public PayType getPayType() {
		return payType;
	}

	public void setPayType(PayType payType) {
		this.payType = payType;
	}

	public SubscriptionOrderStatus getSubscriptionOrderStatus() {
		return subscriptionOrderStatus;
	}

	public void setSubscriptionOrderStatus(SubscriptionOrderStatus subscriptionOrderStatus) {
		this.subscriptionOrderStatus = subscriptionOrderStatus;
	}

	public Date getSubscriptionOrderDate() {
		return subscriptionOrderDate;
	}

	public void setSubscriptionOrderDate(Date subscriptionOrderDate) {
		this.subscriptionOrderDate = subscriptionOrderDate;
	}

	public boolean isCouponApplied() {
		return couponApplied;
	}

	public void setCouponApplied(boolean couponApplied) {
		this.couponApplied = couponApplied;
	}

	public int getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	@Override
    public String toString() {
        return "SubscriptionOrder{" +
                "orderId=" + orderId +
                ", subscription=" + subscription +
                ", payType=" + payType +
                ", subscriptionOrderDate=" + subscriptionOrderDate +
                ", subscriptionStartDate=" + subscriptionStartDate +
                ", subscriptionOrderStatus=" + subscriptionOrderStatus +
                ", subscriptionEndDate=" + subscriptionEndDate +
                ", subscriptionAddr='" + subscriptionAddr + '\'' +
                '}';
    }
}
