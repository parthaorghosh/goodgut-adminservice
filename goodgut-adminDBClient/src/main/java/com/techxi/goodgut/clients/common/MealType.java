package com.techxi.goodgut.clients.common;

public enum MealType {
    BREAKFAST,
    LUNCH,
    PBREAKFAST,
    SNACKS,
    DINNER,
    PDINNER;
}
