package com.techxi.goodgut.clients.model;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Meals implements Serializable{
		
	   @JsonInclude(JsonInclude.Include.NON_NULL)
	    private int mealId;
	   
	   private String imageUrl;
	    
	    private String mealName;
	    private String mealDescription;
	    private float mealPrice;
	    private String mealType;
	    private String mealSubscriptionCatagory;
	    private int mealViewRow;
	    private int mealViewColumn;
	    private float offerPrice;
	    private boolean offerAvail;
	    private Set<MenuItem> mealItems;
	    private String availableDays;
	    private String mealProductName;
	    private boolean sellable;
	    
	    
	    
		
	    public String getMealProductName() {
			return mealProductName;
		}
		public void setMealProductName(String mealProductName) {
			this.mealProductName = mealProductName;
		}
		public String getAvailableDays() {
			return availableDays;
		}
		public void setAvailableDays(String availableDays) {
			this.availableDays = availableDays;
		}
		public String getImageUrl() {
			return imageUrl;
		}
		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}
		public String getMealName() {
			return mealName;
		}
		public void setMealName(String mealName) {
			this.mealName = mealName;
		}
		public String getMealDescription() {
			return mealDescription;
		}
		public void setMealDescription(String mealDescription) {
			this.mealDescription = mealDescription;
		}
		public float getMealPrice() {
			return mealPrice;
		}
		public void setMealPrice(float mealPrice) {
			this.mealPrice = mealPrice;
		}
		public String getMealType() {
			return mealType;
		}
		public void setMealType(String mealType) {
			this.mealType = mealType;
		}
		public String getMealSubscriptionCatagory() {
			return mealSubscriptionCatagory;
		}
		public void setMealSubscriptionCatagory(String mealSubscriptionCatagory) {
			this.mealSubscriptionCatagory = mealSubscriptionCatagory;
		}
		public int getMealViewRow() {
			return mealViewRow;
		}
		public void setMealViewRow(int mealViewRow) {
			this.mealViewRow = mealViewRow;
		}
		public int getMealViewColumn() {
			return mealViewColumn;
		}
		public void setMealViewColumn(int mealViewColumn) {
			this.mealViewColumn = mealViewColumn;
		}
		public float getOfferPrice() {
			return offerPrice;
		}
		public void setOfferPrice(float offerPrice) {
			this.offerPrice = offerPrice;
		}
		public boolean isOfferAvail() {
			return offerAvail;
		}
		public void setOfferAvail(boolean offerAvail) {
			this.offerAvail = offerAvail;
		}
		public Set<MenuItem> getMealItems() {
			return mealItems;
		}
		public void setMealItems(Set<MenuItem> mealItems) {
			this.mealItems = mealItems;
		}
		
		public int getMealId() {
			return mealId;
		}
		public void setMealId(int mealId) {
			this.mealId = mealId;
		}
		@Override
		public String toString() {
			return "Meals [mealId=" + mealId + ", imageUrl=" + imageUrl + ", mealName=" + mealName
					+ ", mealDescription=" + mealDescription + ", mealPrice=" + mealPrice + ", mealType=" + mealType
					+ ", mealSubscriptionCatagory=" + mealSubscriptionCatagory + ", mealViewRow=" + mealViewRow
					+ ", mealViewColumn=" + mealViewColumn + ", offerPrice=" + offerPrice + ", offerAvail=" + offerAvail
					+ ", mealItems=" + mealItems + ", availableDays=" + availableDays + "]";
		}

	   

}
