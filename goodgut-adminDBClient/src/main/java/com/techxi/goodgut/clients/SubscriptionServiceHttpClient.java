package com.techxi.goodgut.clients;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.techxi.goodgut.clients.common.DBResponseCodes;
import com.techxi.goodgut.clients.model.AddSubscriptionRequest;
import com.techxi.goodgut.clients.model.Subscription;

@Component
public class SubscriptionServiceHttpClient {

	@Autowired
	RestTemplate restTemplate;
	@Value("${db.service.endpoint}")
	private String dbServiceEndpoint;

	public String addSubscription(AddSubscriptionRequest subscription) {
		final String uri = dbServiceEndpoint + "subs/addSubs";
		ResponseEntity<String> entity = restTemplate.postForEntity(uri, subscription, String.class);
		if (HttpServletResponse.SC_OK==entity.getStatusCode().value()) {
			return entity.getBody();
		} else
			return DBResponseCodes.ERROR;

	}
	
	public Optional<Subscription> getSubById(int subid,boolean lazy) {
		
		
		final String uri = dbServiceEndpoint + (lazy?"subs/getSubsById/lazy":"subs/getSubsById");
		 UriComponentsBuilder builder = UriComponentsBuilder
	        	    .fromUriString(uri)
	        	    .queryParam("subid",subid);
	        ResponseEntity<Subscription> result = restTemplate.exchange(builder.toUriString(),
	        		HttpMethod.GET, null, new ParameterizedTypeReference<Subscription>() {
	         });

	        if(result.getStatusCode().is2xxSuccessful()){
	        	return Optional.of(result.getBody());
	        }
	        return Optional.empty();
	}
	

}
