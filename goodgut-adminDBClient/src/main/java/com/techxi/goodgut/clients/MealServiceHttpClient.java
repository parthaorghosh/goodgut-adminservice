package com.techxi.goodgut.clients;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Subscription;

@Component
public class MealServiceHttpClient {

	@Autowired
	RestTemplate restTemplate;
	@Value("${db.service.endpoint}")
	private String dbServiceEndpoint;

	public void addMealItems(MenuItem menuItem) {
		final String uri = dbServiceEndpoint + "addmealsItems/addItems";

		restTemplate.postForEntity(uri, menuItem, String.class);
		

	}

	public void editMealItems(MenuItem menuItem) {
		final String uri = dbServiceEndpoint + "addmealsItems/editItems";

		ResponseEntity<String> result = restTemplate.postForEntity(uri, menuItem, String.class);
		

	}

	public List<MenuItem> getAllMenuItems(int count, int pageNumber) {
		final String uri = dbServiceEndpoint + "mealsItems/getAllItems?count="+count+"&pageNumber="+pageNumber;
		ResponseEntity<List<MenuItem>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuItem>>() {
				});
		List<MenuItem> menuItems = result.getBody();
		return menuItems;

	}

	public List<MenuItem> getMenuItemsByName(String itemName) {
		final String uri = dbServiceEndpoint + "mealsItems/getItemsByName?searchItem=" + itemName;
		ResponseEntity<List<MenuItem>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuItem>>() {
				});
		List<MenuItem> menuItems = result.getBody();
		return menuItems;

	}

	public List<Meals> getMealsByName(String mealName) {
		final String uri = dbServiceEndpoint + "mealsItems/getMealsByName?searchMealName=" + mealName;
		ResponseEntity<List<Meals>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Meals>>() {
				});
		List<Meals> meals = result.getBody();
		return meals;

	}

	public void addMeal(Meals meals) {
		final String uri = dbServiceEndpoint + "mealsItems/addMeals";

		ResponseEntity<String> result = restTemplate.postForEntity(uri, meals, String.class);


	}

	public void editMeal(Meals meals) {
		final String uri = dbServiceEndpoint + "mealsItems/updateMeals";

		ResponseEntity<String> result = restTemplate.postForEntity(uri, meals, String.class);
	

	}

	public List<Meals> getMeals() {
		final String uri = dbServiceEndpoint + "mealsItems/getAllMeals";
		ResponseEntity<List<Meals>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Meals>>() {
				});
		List<Meals> meals = result.getBody();
		return meals;

	}

	public void addSubscription(Subscription subscription) {
		final String uri = dbServiceEndpoint + "addmealsItems/addSubscription";

		ResponseEntity<String> result = restTemplate.postForEntity(uri, subscription, String.class);


	}

	public List<Subscription> getSubscription() {
		final String uri = dbServiceEndpoint + "/mealsItems/getAllSubscription";
		ResponseEntity<List<Subscription>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Subscription>>() {
				});
		List<Subscription> subscriptions = result.getBody();
		return subscriptions;

	}
}
