package com.techxi.goodgut.clients.common;

public enum ItemType {
    VEG,
    NONVEG,
    COMBO
}
