package com.techxi.goodgut.clients.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MenuItem {
	
	private String itemDescription;
    private String imageUrl;
    private String itemName;
    private float price;
    private String mealType;
    private int itemViewRow;
    private int itemViewColumn;
    private float offerPrice;
    private boolean offerAvail;
    private String itemMetadata;
    private String itemType;
    private Integer itemId;
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getMealType() {
		return mealType;
	}
	public void setMealType(String mealType) {
		this.mealType = mealType;
	}
	public int getItemViewRow() {
		return itemViewRow;
	}
	public void setItemViewRow(int itemViewRow) {
		this.itemViewRow = itemViewRow;
	}
	public int getItemViewColumn() {
		return itemViewColumn;
	}
	public void setItemViewColumn(int itemViewColumn) {
		this.itemViewColumn = itemViewColumn;
	}
	public float getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(float offerPrice) {
		this.offerPrice = offerPrice;
	}
	public boolean isOfferAvail() {
		return offerAvail;
	}
	public void setOfferAvail(boolean offerAvail) {
		this.offerAvail = offerAvail;
	}
	public String getItemMetadata() {
		return itemMetadata;
	}
	public void setItemMetadata(String itemMetadata) {
		this.itemMetadata = itemMetadata;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	@Override
	public String toString() {
		return "MenuItem [itemDescription=" + itemDescription + ", imageUrl=" + imageUrl + ", itemName=" + itemName
				+ ", price=" + price + ", mealType=" + mealType + ", itemViewRow=" + itemViewRow + ", itemViewColumn="
				+ itemViewColumn + ", offerPrice=" + offerPrice + ", offerAvail=" + offerAvail + ", itemMetadata="
				+ itemMetadata + ", itemType=" + itemType + ", itemId=" + itemId + "]";
	}


}
