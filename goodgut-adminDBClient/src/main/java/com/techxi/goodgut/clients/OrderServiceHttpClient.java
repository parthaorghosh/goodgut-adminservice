package com.techxi.goodgut.clients;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.SubscriptionOrder;

@Component
public class OrderServiceHttpClient {
	
	
	 @Autowired
	 RestTemplate restTemplate;
	 @Value("${db.service.endpoint}")
	 private String dbServiceEndpoint;
	 
	 
	 public List<Order> getOrder(String searchCriteria) {
	        final String uri = dbServiceEndpoint+"order/getOrderBasedOnSC";
	        UriComponentsBuilder builder = UriComponentsBuilder
	        	    .fromUriString(uri)
	        	    .queryParam("searchCriteria",searchCriteria);
	        ResponseEntity<List<Order>> result = restTemplate.exchange(builder.toUriString(),
	        		HttpMethod.GET, null, new ParameterizedTypeReference<List<Order>>() {
	         });
	        return result.getBody();
	    }


	 
	 public List<SubscriptionOrder> getSubOrder(String searchCriteria) {
	        final String uri = dbServiceEndpoint+"order/getSUbOrderBasedOnSC";
	        UriComponentsBuilder builder = UriComponentsBuilder
	        	    .fromUriString(uri)
	        	    .queryParam("searchCriteria",searchCriteria);
	        ResponseEntity<List<SubscriptionOrder>> result = restTemplate.exchange(builder.toUriString(),
	        		HttpMethod.GET, null, new ParameterizedTypeReference<List<SubscriptionOrder>>() {
	         });
	        return result.getBody();
	    }
	 
	 
	public String updateOrder(String updateCriteria) {
		final String uri = dbServiceEndpoint+"order/updateOrderBasedOnUC";
        UriComponentsBuilder builder = UriComponentsBuilder
        	    .fromUriString(uri)
        	    .queryParam("updateCriteria",updateCriteria);
        ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(),
        		HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
         });
    
        return result.getBody();
    }



	public Optional<Order> getOrderById(int orderId) {
		// TODO Auto-generated method stub

		final String uri = dbServiceEndpoint+"order/getOrderByIdWithMeal";
        UriComponentsBuilder builder = UriComponentsBuilder
        	    .fromUriString(uri)
        	    .queryParam("id",orderId);
        ResponseEntity<Order> result = restTemplate.exchange(builder.toUriString(),
        		HttpMethod.GET, null, new ParameterizedTypeReference<Order>() {
         });
        if(result.getStatusCode().is2xxSuccessful()){
        	return Optional.of(result.getBody());
        }
		
		return Optional.empty();
	}
	
	 
}
