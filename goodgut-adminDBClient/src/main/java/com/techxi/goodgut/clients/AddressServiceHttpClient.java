package com.techxi.goodgut.clients;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.Order;

@Component
public class AddressServiceHttpClient {
	
	
	 @Autowired
	 RestTemplate restTemplate;
	 @Value("${db.service.endpoint}")
	 private String dbServiceEndpoint;
	 
	 
	 public Address getAddressById(Integer id) {
	        final String uri = dbServiceEndpoint+"user/getAddressById";
	        UriComponentsBuilder builder = UriComponentsBuilder
	        	    .fromUriString(uri)
	        	    .queryParam("addId",id);
	        ResponseEntity<Address> result = restTemplate.exchange(builder.toUriString(),
	        		HttpMethod.GET, null, new ParameterizedTypeReference<Address>() {
	         });
	        //System.out.println("The Result is "+result);
	        
	        return result.getBody();
	    }


	
	
	 
}
