package com.techxi.goodgut.clients.common;

public enum DeliveryPerformanceType {
    BEFORETIME,
    ONTIME,
    DELAYED
}
