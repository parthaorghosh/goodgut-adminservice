package com.techxi.goodgut.clients.model;

import java.util.List;
import java.util.Optional;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * This class is a model which represents React Material-Table Filter model This
 * model will be use to create native SQL query to get entities
 * 
 * @author ANBC
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class ReactEntityQueryResponseModel {

	private List<?> entities;
	private int page;
	private int pageSize;
	private int totalCount;
	public ReactEntityQueryResponseModel(){}
	public ReactEntityQueryResponseModel(List<?> entities, int page, int pageSize, int totalCount) {
		this.entities=entities;
		this.page=page;
		this.pageSize=pageSize;
		this.totalCount=totalCount;
	}
	public List<?> getEntities() {
		return entities;
	}
	public void setEntities(List<?> entities) {
		this.entities = entities;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
public static class Builder {
	private List<?> entities;
	private int page;
	private int pageSize;
	private int totalCount;
    public Builder(){}
    public Builder withPage(int page) {
        this.page = page;
        return this;
    }
    public Builder withPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }
    public Builder withTotalCount(int totalCount) {
        this.totalCount = totalCount;
        return this;
    }
    public Builder withEntities(List<?> entities) {
		this.entities = entities;
		return this;
	}
   

    public ReactEntityQueryResponseModel build() {
        return new ReactEntityQueryResponseModel(entities,page,pageSize,totalCount);
    }
}
}