package com.techxi.goodgut.clients.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * This class is a model which represents React Material-Table Filter model This
 * model will be use to create native SQL query to get entities
 * 
 * @author ANBC
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReactEntityQueryRequestModel {

	private List<Filter> filters;
	private OrderBy orderBy;
	private String orderDirection;
	/**
	 * starts from 0
	 */
	private int page;
	private int pageSize;
	private String search;
	private int totalCount;
	private String entityName;

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public List<Filter> getFilters() {
		return filters;
	}

	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}

	public OrderBy getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * Represent react material-table filter
	 * 
	 * @author ANBC
	 *
	 */
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Filter {
		private Column column;
		private String operator;
		private Object value;

		public Column getColumn() {
			return column;
		}

		public void setColumn(Column column) {
			this.column = column;
		}

		public String getOperator() {
			return operator;
		}

		public void setOperator(String operator) {
			this.operator = operator;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Column {
		private String titile;
		private String field;

		public String getTitile() {
			return titile;
		}

		public void setTitile(String titile) {
			this.titile = titile;
		}

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

	}

	static class OrderBy {
		private String field;
		private boolean filtering;
		Map<String, Object> tableData;
		private String title;

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

		public boolean isFiltering() {
			return filtering;
		}

		public void setFiltering(boolean filtering) {
			this.filtering = filtering;
		}

		public Map<String, Object> getTableData() {
			return tableData;
		}

		public void setTableData(Map<String, Object> tableData) {
			this.tableData = tableData;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}
}
