package com.techxi.goodgut.clients.model;

public class User {
	
	
    private String userName;
    private String password;
    private String status;
    private String userAuthType;
    private String firstName;
    private String lastName;
    private String city;
    private String state;
    private String pincode;
    private String primaryAddress;
    
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserAuthType() {
		return userAuthType;
	}
	public void setUserAuthType(String userAuthType) {
		this.userAuthType = userAuthType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	public String getPrimaryAddress() {
		return primaryAddress;
	}
	public void setPrimaryAddress(String primaryAddress) {
		this.primaryAddress = primaryAddress;
	}
	
	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", status=" + status + ", userAuthType="
				+ userAuthType + ", firstName=" + firstName + ", lastName=" + lastName + ", city=" + city + ", state="
				+ state + ", pincode=" + pincode + ", primaryAddress=" + primaryAddress + "]";
	}
    

}
