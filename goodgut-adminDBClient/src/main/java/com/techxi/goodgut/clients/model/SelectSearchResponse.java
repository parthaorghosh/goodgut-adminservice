package com.techxi.goodgut.clients.model;

import java.util.List;

public class SelectSearchResponse {
	private List<SelectedItem> results;
	
	public List<SelectedItem> getResults() {
		return results;
	}

	public void setResults(List<SelectedItem> results) {
		this.results = results;
	}

	/** [{value: 'chocolate', label: 'Chocolate' }] **/
	public static class SelectedItem {

		private String value;
		private String label;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

	}

}
