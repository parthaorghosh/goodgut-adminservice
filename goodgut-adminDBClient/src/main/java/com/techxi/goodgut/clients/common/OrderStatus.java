package com.techxi.goodgut.clients.common;

public enum OrderStatus {
	ORDERPLACED,
    ORDERACTIVE,
    PAYMENT_PENDING,
    PAYMENT_FAILURE,
    PREPARING,
    PICKEDUP,
    TRANSIT,
    DELIVERED,
    INKITCHEN
}
