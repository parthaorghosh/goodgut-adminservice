package com.techxi.goodgut.clients.model;

public class Address {

	private String contactNo;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String pinCode;
	private String city;
	private String name;
	private User userAddresses;
	private String state;
	
	public Address() {
		this.contactNo="";
		this.addressLine1="";
		this.addressLine2="";
		this.addressLine3="";
		this.pinCode="";
		this.city="";
		this.name="";
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	

	public User getUserAddresses() {
		return userAddresses;
	}

	public void setUserAddresses(User userAddresses) {
		this.userAddresses = userAddresses;
	}

	@Override
	public String toString() {
		return "Address [contactNo=" + contactNo + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2
				+ ", addressLine3=" + addressLine3 + ", pinCode=" + pinCode + ", city=" + city + ", name=" + name
				+ ", user=" + userAddresses + ", state=" + state + "]";
	}

	
	
	
}
