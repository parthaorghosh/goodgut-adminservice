package com.techxi.goodgut.clients.model;


import java.util.Date;
import java.util.List;

import com.techxi.goodgut.clients.common.DeliveryPerformanceType;
import com.techxi.goodgut.clients.common.MealType;
import com.techxi.goodgut.clients.common.OrderCatagory;
import com.techxi.goodgut.clients.common.OrderStatus;
import com.techxi.goodgut.clients.common.PayType;


public class Order {


    public int getOrderId() {
        return orderId;
    }

    private int orderId;
    private OrderStatus orderStatus;
    private User user;
    private OrderCatagory orderCatagory;
    private Date orderDate;
    private Date deliveryDate;
    private String deliveredBy;
    private int deliveryAddressId;
    private DeliveryPerformanceType deliveryPerformanceType;
    private MealType mealType;
    private List<Meals> meals;
    private PayType payType;
    private Subscription subscription;
    private String formatedOrderDate;
    private String formateddeliveryDate;
    private boolean couponApplied;
	private int discountAmount;
	private String couponId;
	private GuestUser guestuser;
    
    
    public boolean isCouponApplied() {
		return couponApplied;
	}

	public void setCouponApplied(boolean couponApplied) {
		this.couponApplied = couponApplied;
	}

	public int getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public Order() {
        super();
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderCatagory getOrderCatagory() {
        return orderCatagory;
    }

    public void setOrderCatagory(OrderCatagory orderCatagory) {
        this.orderCatagory = orderCatagory;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public DeliveryPerformanceType getDeliveryPerformanceType() {
        return deliveryPerformanceType;
    }

    public void setDeliveryPerformanceType(DeliveryPerformanceType deliveryPerformanceType) {
        this.deliveryPerformanceType = deliveryPerformanceType;
    }

    public MealType getMealType() {
        return mealType;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public int getDeliveryAddressId() {
		return deliveryAddressId;
	}

	public void setDeliveryAddressId(int deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}



	public List<Meals> getMeals() {
		return meals;
	}

	public void setMeals(List<Meals> meals) {
		this.meals = meals;
	}

	public PayType getPayType() {
		return payType;
	}

	public void setPayType(PayType payType) {
		this.payType = payType;
	}

	public String getFormatedOrderDate() {
		return formatedOrderDate;
	}

	public void setFormatedOrderDate(String formatedOrderDate) {
		this.formatedOrderDate = formatedOrderDate;
	}

	public String getFormateddeliveryDate() {
		return formateddeliveryDate;
	}

	public void setFormateddeliveryDate(String formateddeliveryDate) {
		this.formateddeliveryDate = formateddeliveryDate;
	}

	public GuestUser getGuestuser() {
		return guestuser;
	}

	public void setGuestuser(GuestUser guestuser) {
		this.guestuser = guestuser;
	}

	@Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                "deliveryAddressId=" + deliveryAddressId +
                ", orderStatus=" + orderStatus +
                ", payType=" + payType +
                ", user=" + user +
                ", orderCatagory=" + orderCatagory +
                ", orderDate=" + orderDate +
                ", deliveryDate=" + deliveryDate +
                ", deliveredBy=" + deliveredBy +
                ", deliveryPerformanceType=" + deliveryPerformanceType +
                ", mealType=" + mealType +
                ", subscription=" + subscription +
                '}';
    }
}
