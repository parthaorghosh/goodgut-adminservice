package com.techxi.goodgut.clients.common;

public enum SubscriptionOrderStatus {
    PAYMENT_PENDING,
    PAYMENT_FAILURE,
    DONE,
    ACTIVE
}
