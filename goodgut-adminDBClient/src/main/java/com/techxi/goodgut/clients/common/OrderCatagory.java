package com.techxi.goodgut.clients.common;

public enum OrderCatagory {
	SINGLEORDER(1,"SINGLEORDER"),
    SUBSCRIPTIONORDER(2,"SUBSCRIPTIONORDER"),
    GUESTSINGLEORDER(3,"GUESTSINGLEORDER");



    private final Integer key;
    private final String messageKey;

    public Integer getKey() {
        return key;
    }

    public String getMessageKey() {
        return messageKey;
    }

    OrderCatagory(Integer key, String messageKey) {
        this.key = key;
        this.messageKey = messageKey;
    }
}
