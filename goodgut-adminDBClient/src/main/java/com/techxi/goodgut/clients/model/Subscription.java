package com.techxi.goodgut.clients.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Subscription {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private int subscriptionId;
    private String imageUrl;
    private String name;
    private String subDescription;
    private float price;
    private float offerPrice;
    private boolean offerAvail;
    private int calories;
    private int protein;
    private String mealSubscriptionCatagory;
    private String subscriptionPeriodType;
    private Set<Meals> meals;
	public int getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(int subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubDescription() {
		return subDescription;
	}
	public void setSubDescription(String subDescription) {
		this.subDescription = subDescription;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(float offerPrice) {
		this.offerPrice = offerPrice;
	}
	public boolean isOfferAvail() {
		return offerAvail;
	}
	public void setOfferAvail(boolean offerAvail) {
		this.offerAvail = offerAvail;
	}
	public int getCalories() {
		return calories;
	}
	public void setCalories(int calories) {
		this.calories = calories;
	}
	public String getMealSubscriptionCatagory() {
		return mealSubscriptionCatagory;
	}
	public void setMealSubscriptionCatagory(String mealSubscriptionCatagory) {
		this.mealSubscriptionCatagory = mealSubscriptionCatagory;
	}
	public String getSubscriptionPeriodType() {
		return subscriptionPeriodType;
	}
	public void setSubscriptionPeriodType(String subscriptionPeriodType) {
		this.subscriptionPeriodType = subscriptionPeriodType;
	}
	public Set<Meals> getMeals() {
		return meals;
	}
	public void setMeals(Set<Meals> meals) {
		this.meals = meals;
	}
	
	public int getProtein() {
		return protein;
	}
	public void setProtein(int protein) {
		this.protein = protein;
	}
	@Override
	public String toString() {
		return "Subscription [subscriptionId=" + subscriptionId + ", imageUrl=" + imageUrl + ", name=" + name
				+ ", subDescription=" + subDescription + ", price=" + price + ", offerPrice=" + offerPrice
				+ ", offerAvail=" + offerAvail + ", calories=" + calories + ", mealSubscriptionCatagory="
				+ mealSubscriptionCatagory + ", subscriptionPeriodType=" + subscriptionPeriodType + ", meals=" + meals
				+ "]";
	}
    
    

}
