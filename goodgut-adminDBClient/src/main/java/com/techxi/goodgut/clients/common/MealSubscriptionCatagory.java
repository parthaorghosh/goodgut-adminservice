package com.techxi.goodgut.clients.common;

public enum MealSubscriptionCatagory {
    VEG,
    NONVEG,
    COMBO
}
