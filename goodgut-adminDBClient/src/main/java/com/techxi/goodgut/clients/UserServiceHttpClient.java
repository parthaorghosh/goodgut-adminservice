package com.techxi.goodgut.clients;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.User;



@Component

public class UserServiceHttpClient 
{
	 @Autowired
	 RestTemplate restTemplate;
	 @Value("${db.service.endpoint}")
	 private String dbServiceEndpoint;
	 
	 public void addUserAndAddress(Address address) {
	        final String uri = dbServiceEndpoint+"/user/addAddress";
	 
	        ResponseEntity<String> result = restTemplate.postForEntity(uri, address, String.class);
	       // System.out.println("The Result is "+result);
	        
	    }
	 
	 public List<User> loadUser()
	 {
		 final String uri = dbServiceEndpoint+"/user/getUsers";
		 ResponseEntity<List<User>> result =  restTemplate.exchange(uri,
                 HttpMethod.GET, null, new ParameterizedTypeReference<List<User>>() {
         });
		 List<User> user = result.getBody();
		 return user;
		 
	 }
	 
	 public User getUserByEmail(String userName)
	 {
		 final String uri = dbServiceEndpoint+"/getUserByEmail?"+userName;
		 ResponseEntity<User> result =  restTemplate.exchange(uri,
                 HttpMethod.GET, null, new ParameterizedTypeReference<User>() {
         });
		 User user = result.getBody();
		 return user;
		 
	 }
	 
}
