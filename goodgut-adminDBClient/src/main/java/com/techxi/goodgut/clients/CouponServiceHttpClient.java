package com.techxi.goodgut.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.techxi.goodgut.clients.model.Coupon;

@Component
public class CouponServiceHttpClient {
	
	
	 @Autowired
	 RestTemplate restTemplate;
	 @Value("${db.service.endpoint}")
	 private String dbServiceEndpoint;
	 
	 
	 public void addCoupon(Coupon coupon) {
	        final String uri = dbServiceEndpoint+"coupon/add";
	        ResponseEntity<String> result = restTemplate.postForEntity(uri, coupon, String.class);
	        //System.out.println("The Result is "+result);
	        
	        
	    }


	
	
	 
}
