package com.techxi.goodgut.clients;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.techxi.goodgut.clients.common.OrderStatus;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.ReactEntityQueryRequestModel;
import com.techxi.goodgut.clients.model.ReactEntityQueryResponseModel;
import com.techxi.goodgut.clients.model.SelectSearchResponse;
import com.techxi.goodgut.clients.model.Subscription;

@Component
public class EntitySearchServiceHttpClient {

	@Autowired
	RestTemplate restTemplate;
	@Value("${db.service.endpoint}")
	private String dbServiceEndpoint;

	public List<?> getEntites(String entityName, String whereClause) {
		final String uri = dbServiceEndpoint + "search/entity/" + entityName + "?whereClause=" + whereClause;

		if (entityName.equals("meal")) {
			ResponseEntity<List<Meals>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Meals>>() {
					});
			return result.getBody();
		} else if (entityName.equals("menuItem")) {
			ResponseEntity<List<MenuItem>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<MenuItem>>() {
					});
			return result.getBody();
		} else if (entityName.equals("subscription")) {
			ResponseEntity<List<Subscription>> result = restTemplate.exchange(uri, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Subscription>>() {
					});
			return result.getBody();
		}
		return Collections.emptyList();

	}
	
	public SelectSearchResponse getEntites(String entityName, String fieldName,String fieldValue) {
		final String uri = dbServiceEndpoint + "search/entity/" + entityName + "?fieldName=" + fieldName+"&fieldValue=" + fieldValue;
		ResponseEntity<SelectSearchResponse> result = restTemplate.exchange(uri, HttpMethod.GET, null,
				new ParameterizedTypeReference<SelectSearchResponse>() {
				});
		return result.getBody();
	}

	public ReactEntityQueryResponseModel getEntityWithFilters(ReactEntityQueryRequestModel filterModel, String entity) {

		final String uri = dbServiceEndpoint + "search/entity/" + entity;
		ResponseEntity<ReactEntityQueryResponseModel> result = restTemplate.postForEntity(uri, filterModel,
				ReactEntityQueryResponseModel.class);
		return result.getBody();
	}

	public boolean updateEntity(Map<?, ?> entityMap, String entity) {
		final String uri = dbServiceEndpoint + "update/entity/" + entity;
		ResponseEntity<String> result = restTemplate.postForEntity(uri, entityMap,
				String.class);
		return "GDB200".equals(result.getBody());
	}

	public SelectSearchResponse getEntites(String entity, Map<?, ?> searchModel) {
		final String uri = dbServiceEndpoint + "search/entity/select/" + entity;
		ResponseEntity<SelectSearchResponse> result = restTemplate.postForEntity(uri, searchModel,
				SelectSearchResponse.class);
		return result.getBody();
	}

}
