package com.techxi.goodgut.clients.model;


import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.techxi.goodgut.clients.common.DiscountType;

public class Coupon implements Serializable {

    private Date validFrom;
    private Date validTo;
    private Date createdOn;
    private String marketingMessage;
    private int consumptionPerUser;
    private DiscountType discountType;
    private int discountValue;
    private int maxDiscountValue;
    private String status;
    private Set<Meals> couponApplicableForMeals;
    private Set<Subscription> couponApplicableForSubs;
    private  String id;

    public Coupon() {
        super();
    }


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Date getValidFrom() {
		return validFrom;
	}


	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}


	public Date getValidTo() {
		return validTo;
	}


	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getMarketingMessage() {
		return marketingMessage;
	}


	public void setMarketingMessage(String marketingMessage) {
		this.marketingMessage = marketingMessage;
	}


	public int getConsumptionPerUser() {
		return consumptionPerUser;
	}


	public void setConsumptionPerUser(int consumptionPerUser) {
		this.consumptionPerUser = consumptionPerUser;
	}


	public DiscountType getDiscountType() {
		return discountType;
	}


	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}


	public int getDiscountValue() {
		return discountValue;
	}


	public void setDiscountValue(int discountValue) {
		this.discountValue = discountValue;
	}


	public int getMaxDiscountValue() {
		return maxDiscountValue;
	}


	public void setMaxDiscountValue(int maxDiscountValue) {
		this.maxDiscountValue = maxDiscountValue;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Set<Meals> getCouponApplicableForMeals() {
		return couponApplicableForMeals;
	}


	public void setCouponApplicableForMeals(Set<Meals> couponApplicableForMeals) {
		this.couponApplicableForMeals = couponApplicableForMeals;
	}


	public Set<Subscription> getCouponApplicableForSubs() {
		return couponApplicableForSubs;
	}


	public void setCouponApplicableForSubs(Set<Subscription> couponApplicableForSubs) {
		this.couponApplicableForSubs = couponApplicableForSubs;
	}

}
