package com.techxi.goodgut.clients.common;

public enum PayType {

	COD,
	PAYTM,
	CARD,
	UPI,
	NETBANKING
	
}
