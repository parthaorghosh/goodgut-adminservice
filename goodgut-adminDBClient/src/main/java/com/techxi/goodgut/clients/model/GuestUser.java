package com.techxi.goodgut.clients.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown=true)
public class GuestUser implements Serializable {

	private int id;
	@JsonProperty(required=true)
	private String contact;
	@JsonProperty(required=true)
	private String name;
	@JsonProperty(required=true)
	private String addresslineone;
	@JsonProperty(required=true)
	private String addresslinetwo;
	@JsonProperty(required=false)
	private String addresslinethree;
	@JsonProperty(required=true)
	private String state;
	@JsonProperty(required=true)
	private String pincode;
	@JsonProperty(required=true)
	private String city;
	
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddresslineone() {
		return addresslineone;
	}

	public void setAddresslineone(String addresslineone) {
		this.addresslineone = addresslineone;
	}

	public String getAddresslinetwo() {
		return addresslinetwo;
	}

	public void setAddresslinetwo(String addresslinetwo) {
		this.addresslinetwo = addresslinetwo;
	}

	public String getAddresslinethree() {
		return addresslinethree;
	}

	public void setAddresslinethree(String addresslinethree) {
		this.addresslinethree = addresslinethree;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public Address getAddress(){
		
		Address add = new Address();
		add.setAddressLine1(this.getAddresslineone());
		add.setAddressLine2(this.getAddresslinetwo());
		add.setAddressLine3(this.getAddresslinethree());
		add.setCity(this.getCity());
		add.setState(this.getState());
		add.setPinCode(this.getPincode());
		add.setContactNo(this.getContact());
		add.setName(this.getName());
		
		return add;
	}

}
