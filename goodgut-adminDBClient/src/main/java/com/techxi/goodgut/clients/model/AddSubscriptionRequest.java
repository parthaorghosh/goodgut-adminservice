package com.techxi.goodgut.clients.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddSubscriptionRequest {

	private int subscriptionId;
	private String imageUrl;
	private String name;
	private String subDescription;
	private float price;
	private float offerPrice;
	private boolean offerAvail;
	private int calories;
	private int protein;
	private String mealSubscriptionCatagory;
	private String subscriptionPeriodType;
	private List<Integer> mealIds;

	public int getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(int subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubDescription() {
		return subDescription;
	}

	public void setSubDescription(String subDescription) {
		this.subDescription = subDescription;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(float offerPrice) {
		this.offerPrice = offerPrice;
	}

	public boolean isOfferAvail() {
		return offerAvail;
	}

	public void setOfferAvail(boolean offerAvail) {
		this.offerAvail = offerAvail;
	}

	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public String getMealSubscriptionCatagory() {
		return mealSubscriptionCatagory;
	}

	public void setMealSubscriptionCatagory(String mealSubscriptionCatagory) {
		this.mealSubscriptionCatagory = mealSubscriptionCatagory;
	}

	public String getSubscriptionPeriodType() {
		return subscriptionPeriodType;
	}

	public void setSubscriptionPeriodType(String subscriptionPeriodType) {
		this.subscriptionPeriodType = subscriptionPeriodType;
	}

	public List<Integer> getMealIds() {
		return mealIds;
	}

	public void setMealIds(List<Integer> mealIds) {
		this.mealIds = mealIds;
	}

	public int getProtein() {
		return protein;
	}

	public void setProtein(int protein) {
		this.protein = protein;
	}

}
