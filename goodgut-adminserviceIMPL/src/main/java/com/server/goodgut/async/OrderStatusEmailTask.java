package com.server.goodgut.async;

import java.text.SimpleDateFormat;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.server.goodgut.util.AmazonMailService;
import com.server.goodgut.util.ObjectMapperUtil;
import com.server.goodgut.util.OrderEmailModel;
import com.server.goodgut.util.TemplateModel;
import com.techxi.goodgut.clients.common.OrderStatus;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.User;

public class OrderStatusEmailTask implements Task {

	private Order order;

	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy h:mm a");

	public static final Logger EATFIT_ERROR = LoggerFactory.getLogger("ERROR");

	public OrderStatusEmailTask(Order order) {
		this.order = order;

	}

	@Override
	public void run() {

		try {
			//TODO fix needed here
			Meals meal = order.getMeals().get(0);
			User user = order.getUser();
			if (meal != null && user != null) {
				float price = 0;
				if (meal.isOfferAvail())
					price = meal.getOfferPrice();
				else {
					price = meal.getMealPrice();
					if (order.isCouponApplied()) {
						double withouTaxPrice = price / 1.05;
						withouTaxPrice = withouTaxPrice - order.getDiscountAmount();
						price = (float) (withouTaxPrice * 1.05);

					}
				}

				float withouTaxPrice = (float) (price / 1.05);
				float taxes = price - withouTaxPrice;

				String orderdate = sdf.format(order.getOrderDate());
				OrderEmailModel model = new OrderEmailModel(order, withouTaxPrice, taxes, orderdate);

				OrderStatus status = order.getOrderStatus();
				switch (status) {
				case TRANSIT:
					model.setSubject("Order Id " + order.getOrderId() + " is in transit.");

					break;
				case DELIVERED:
					model.setSubject("Order Id " + order.getOrderId() + " delivery confirmation");

					break;
				default:
					break;
				}

				AmazonMailService.sendMail(
						Collections.singletonMap( user.getUserName() ,
								ObjectMapperUtil.writeValueAsString(model)),
						AmazonMailService.GYMFOOD_ORDER_TEMPLATE, AmazonMailService.FROMNAME,
						AmazonMailService.FROMEMAIL, ObjectMapperUtil.writeValueAsString(model));

			}
		} catch (Exception e) {
			EATFIT_ERROR.error("Error Wile Sending Mail", e);

		}

	}

	private void sendMail(TemplateModel model, String username) throws JsonProcessingException {

	}

}
