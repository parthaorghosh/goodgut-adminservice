package com.server.goodgut.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskExecutor {
	private TaskExecutor() {
	}

	private static ExecutorService executorService = Executors.newFixedThreadPool(10);

	public static void executeTask(Task task) {

		executorService.submit(task);
		//executorService.shutdown();
	}

}
