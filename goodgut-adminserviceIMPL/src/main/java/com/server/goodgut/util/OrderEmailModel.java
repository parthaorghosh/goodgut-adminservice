package com.server.goodgut.util;

import com.techxi.goodgut.clients.model.Order;

public class OrderEmailModel extends TemplateModel{

	private String orderby;
	private String orderid;
	private String orderdate;
	private String orderstatus;
	private String mealname;
	private String mealimage;
	private String baseprice;
	private String taxes;
	private String total;
	private String subject;
	
	public OrderEmailModel() {
		// TODO Auto-generated constructor stub
	}
	public OrderEmailModel(Order order, float withouTaxPrice, float taxes,String orderdate) {
		
		
		this.orderby = order.getUser().getFirstName()+" "+order.getUser().getLastName();
		this.orderdate = orderdate;
		this.orderid = String.valueOf(order.getOrderId());
		this.mealimage=order.getMeals().get(0).getImageUrl();
		this.mealname = order.getMeals().get(0).getMealName();
		this.baseprice= String.format("%.2f", withouTaxPrice);
		this.taxes= String.format("%.2f", taxes);
		this.total= String.format("%.2f", withouTaxPrice+taxes);
		this.orderstatus=order.getOrderStatus().toString();
		
	}
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public String getMealname() {
		return mealname;
	}
	public void setMealname(String mealname) {
		this.mealname = mealname;
	}
	public String getMealimage() {
		return mealimage;
	}
	public void setMealimage(String mealimage) {
		this.mealimage = mealimage;
	}
	public String getBaseprice() {
		return baseprice;
	}
	public void setBaseprice(String baseprice) {
		this.baseprice = baseprice;
	}
	public String getTaxes() {
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	
}
