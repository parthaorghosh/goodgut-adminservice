package com.server.goodgut.util;

import java.util.Collections;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.pinpoint.AmazonPinpoint;
import com.amazonaws.services.pinpoint.AmazonPinpointClient;
import com.amazonaws.services.pinpoint.model.AddressConfiguration;
import com.amazonaws.services.pinpoint.model.ChannelType;
import com.amazonaws.services.pinpoint.model.DirectMessageConfiguration;
import com.amazonaws.services.pinpoint.model.MessageRequest;
import com.amazonaws.services.pinpoint.model.SMSMessage;
import com.amazonaws.services.pinpoint.model.SendMessagesRequest;
import com.amazonaws.services.pinpoint.model.SendMessagesResult;

@Component("amazonSMSService")
public class AmazonSMSService {
	private static final String appId = "e2807c1bda614c4586181a9f3c3b0109";
	public static final String TYPE_TRANSACTIONAL = "TRANSACTIONAL";
	public static final String TYPE_PROMOTIONAL = "PROMOTIONAL";
	private static final String ACCESSKEY = "AKIASFSF7RY7YMGFKPQZ";
	private static final String SECRET = "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";
	private static BasicAWSCredentials CREDS = new BasicAWSCredentials(ACCESSKEY, SECRET);
	private static final AmazonPinpoint client = AmazonPinpointClient.builder().withRegion(Regions.AP_SOUTH_1)
			.withCredentials(new AWSStaticCredentialsProvider(CREDS)).build();
	

	public SendMessagesResult sendSMS(String destinationNumber, String messageType, String message) {
		Map<String, AddressConfiguration> addressMap = Collections.singletonMap(destinationNumber,
				new AddressConfiguration().withChannelType(ChannelType.SMS));

		// TODO add sender ID once you get it
		SendMessagesRequest request = new SendMessagesRequest().withApplicationId(appId).withMessageRequest(
				new MessageRequest().withAddresses(addressMap).withMessageConfiguration(new DirectMessageConfiguration()
						.withSMSMessage(new SMSMessage().withBody(message).withMessageType(messageType))));
		return client.sendMessages(request);
	}
	
	
	public static void main(String args[]){
		try{
			new AmazonSMSService().sendSMS("+919836184102", TYPE_TRANSACTIONAL, "OTP is 1234");
		}
		catch(Exception w){
			w.printStackTrace();
		}
	}

}
