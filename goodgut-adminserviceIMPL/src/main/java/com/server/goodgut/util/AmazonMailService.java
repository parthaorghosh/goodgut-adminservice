package com.server.goodgut.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.BulkEmailDestination;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendBulkTemplatedEmailRequest;
import com.amazonaws.services.simpleemail.model.SendBulkTemplatedEmailResult;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component("amazonMailService")
public class AmazonMailService {

	public static final String NEW_SUB_ORDER_TEMPLATE = "new_sub_order_mail_template";
	public static String NEW_USER_ADD_TEMPLATE = "new_user_mail_validation_template";
	public static String NEW_ORDER_TEMPLATE = "new_order_mail_template";
	public static String FORGOT_PASSWROD_TEMPLATE = "forgot_password_mail_template";
	public static final String FROMNAME = "My Gym Food";
	public static final String FROMEMAIL = "ask@mygymfood.com";
	public static final String GYMFOOD_ORDER_TEMPLATE = "gymfood_order_mail_template";

	private static AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
			.withRegion(Regions.US_EAST_1).withCredentials(new DefaultAWSCredentialsProviderChain()).build();

	public static void sendMail(Map<String, String> templateModelMap, String templateName, String fromName,
			String fromEmail, String defaultTempalteData) throws JsonProcessingException {
		List<BulkEmailDestination> destinations = generateEmailDestination(templateModelMap);
		SendBulkTemplatedEmailRequest request = null;

			
		request = new SendBulkTemplatedEmailRequest().withDestinations(destinations)
				.withDefaultTemplateData(defaultTempalteData).withSource(fromName + "<" + fromEmail + ">")
				.withReplyToAddresses(fromEmail).withReturnPath(fromEmail).withTemplate(templateName);

		SendBulkTemplatedEmailResult result = client.sendBulkTemplatedEmail(request);
		

	}

	public void sendOrderMail(List<String> toMailAdresses, TemplateModel model, String template)
			throws JsonProcessingException {
		String defaultData = ObjectMapperUtil.writeValueAsString(model);
		List<BulkEmailDestination> destinations = new ArrayList<>();

		SendBulkTemplatedEmailRequest request = null;

		for (String mailAddress : toMailAdresses) {
			destinations.add(new BulkEmailDestination().withReplacementTemplateData(defaultData).withDestination(
					new Destination().withToAddresses(mailAddress).withBccAddresses("ask@mygymfood.com")));

		}
		request = new SendBulkTemplatedEmailRequest().withDestinations(destinations)
				.withDefaultTemplateData(defaultData).withSource(model.getFromName() + "<" + model.getFromEmail() + ">")

				.withReplyToAddresses(model.getFromEmail()).withReturnPath(model.getFromEmail()).withTemplate(template);

		client.sendBulkTemplatedEmail(request);
	}

	private static List<BulkEmailDestination> generateEmailDestination(List<String> useremails,
			Map<String, String> templateModelMap) {
		List<BulkEmailDestination> bulkList = new ArrayList<>();
		for (String email : useremails) {
			if (templateModelMap.containsKey(email)) {
				BulkEmailDestination bulk = new BulkEmailDestination()
						.withReplacementTemplateData(templateModelMap.get(email))
						.withDestination(new Destination().withToAddresses(email));
				bulkList.add(bulk);
			}

		}

		return bulkList;
	}

	private static List<BulkEmailDestination> generateEmailDestination(Map<String, String> templateModelMap) {
		List<BulkEmailDestination> bulkList = new ArrayList<>();

		templateModelMap.forEach((key, val) -> {
			if (key != null & val != null) {
				System.out.println(key);
				System.out.println(val);
				BulkEmailDestination bulk = new BulkEmailDestination().withReplacementTemplateData(val)
						.withDestination(new Destination().withToAddresses(key));
				bulkList.add(bulk);
			}

		});
		return bulkList;
	}

}
