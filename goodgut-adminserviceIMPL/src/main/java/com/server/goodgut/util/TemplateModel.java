package com.server.goodgut.util;

public class TemplateModel {
	private String toEmail="";
	private String fromName="My Gym Food";
	private String toName="";
	private String fromEmail;
	private String gymfoodcontactno="+91-7304604900";
	
	
	
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	public String getGymfoodcontactno() {
		return gymfoodcontactno;
	}
	public void setGymfoodcontactno(String gymfoodcontactno) {
		this.gymfoodcontactno = gymfoodcontactno;
	}
	
	
	


}
