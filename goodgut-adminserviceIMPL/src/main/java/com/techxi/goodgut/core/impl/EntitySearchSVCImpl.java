package com.techxi.goodgut.core.impl;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.InternalParseException;
import org.springframework.stereotype.Service;

import com.amazonaws.services.pinpoint.model.SendMessagesResult;
import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.server.goodgut.admin.response.json.BaseResponse;
import com.server.goodgut.admin.response.json.GoodgutResponseCode;
import com.server.goodgut.async.OrderStatusEmailTask;
import com.server.goodgut.async.TaskExecutor;
import com.server.goodgut.util.AmazonSMSService;
import com.techxi.goodgut.clients.EntitySearchServiceHttpClient;
import com.techxi.goodgut.clients.common.OrderStatus;
import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.ReactEntityQueryRequestModel;
import com.techxi.goodgut.clients.model.ReactEntityQueryResponseModel;
import com.techxi.goodgut.clients.model.SelectSearchResponse;
import com.techxi.goodgut.core.services.EntitySearchSVCApi;
import com.techxi.goodgut.core.services.OrderSVCApi;

@Service
public class EntitySearchSVCImpl implements EntitySearchSVCApi {

	AmazonSMSService smsSVC = new AmazonSMSService();
	@Autowired
	EntitySearchServiceHttpClient entitySVC;

	@Autowired
	OrderSVCApi orderSVC;
	
	
	@Override
	public SelectSearchResponse getSelectSearchEntityResponse(String inputValue, String entityName,
			String searchField) {
		return entitySVC.getEntites(entityName, searchField, inputValue);

	}

	@Override
	public BaseEntityResponse getEntityWithFilters(ReactEntityQueryRequestModel filterModel, String entity) {
		return convertTOBaseEntityResponse(entitySVC.getEntityWithFilters(filterModel, entity));

	}

	private BaseEntityResponse convertTOBaseEntityResponse(ReactEntityQueryResponseModel response) {
		BaseEntityResponse resp = new BaseEntityResponse();
		if (response != null) {
			resp.setTotalCount(response.getTotalCount());
			resp.setData(response.getEntities());
			resp.setPage(response.getPage());
			resp.setPageSize(response.getPageSize());
			resp.setResponseCode(GoodgutResponseCode.SUCCESS);
		} else {
			resp.setResponseCode(GoodgutResponseCode.FAIL);
		}

		return resp;
	}

	@Override
	public BaseResponse updateEntity(Map<?, ?> entityMap, String entity) {

		BaseResponse resp = new BaseResponse();
		String message = "Your order has been delivered. Enjoy your meal. For any queries and feedback please call on 7304604900 ";
		if (entitySVC.updateEntity(entityMap, entity)) {
			if ("order".equals(entity)) {
				if (entityMap.containsKey("orderStatus")) {
					OrderStatus status = OrderStatus.valueOf(entityMap.get("orderStatus").toString());
					switch (status) {
					case TRANSIT:
						message = "Your order is in transit. Our delivery executive will deliver you in shortly. Thank you for ordering with us, for any queries and feedback please call on 7304604900 ";
					case DELIVERED:
						if (entityMap.get("primaryKey") != null) {
							try {
								
								Optional<Order> oo = orderSVC.getOrder((Integer)entityMap.get("primaryKey"));
								if(oo.isPresent()){
									if(oo.get().getGuestuser()!=null){
										String contact = oo.get().getGuestuser().getContact();
										String destinationNumber = "+91" + contact;
										
										SendMessagesResult smsResult = smsSVC.sendSMS(destinationNumber, AmazonSMSService.TYPE_TRANSACTIONAL, message);

									}
									else{
										OrderStatusEmailTask task = new OrderStatusEmailTask(oo.get());
										TaskExecutor.executeTask(task);
									}
									
								}
								
							} catch (InternalParseException e) {
								e.printStackTrace();
							}

						}

						break;
					default:
						break;
					}

				}
			}

			resp.setResponseCode(GoodgutResponseCode.SUCCESS);
		}

		else
			resp.setResponseCode(GoodgutResponseCode.FAIL);

		return resp;
	}

	@Override
	public SelectSearchResponse getSelectSearchEntityResponse(String entity, Map<?, ?> searchModel) {
		return entitySVC.getEntites(entity, searchModel);
	}

}
