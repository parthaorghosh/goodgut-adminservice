package com.techxi.goodgut.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techxi.goodgut.clients.UserServiceHttpClient;
import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.User;
import com.techxi.goodgut.core.services.UserServiceSVCApi;


@Service
public class UserServiceSVCImpl implements UserServiceSVCApi {

	Map<String,User> cache = new HashMap<String,User> ();
	
	@Autowired
	UserServiceHttpClient usrServiceHttpClient;
	
	@Override
	public User getUserDetailByUser(String userName) {
		//System.out.println("cache hits"+cache.get(userName));
		User user = cache.get(userName);
		if (user==null)
		{
			usrServiceHttpClient.getUserByEmail(userName);
		}
		return user;
	}
	
	@PostConstruct
	private void loadUsers()
	{
		List <User> users = usrServiceHttpClient.loadUser();
		users.forEach(user->{
			cache.put(user.getUserName(),user);
		});
		//System.out.println(" I am here in postconstruct "+cache);
	}


	@Override
	public String addUserAndAddress(Address address) {
		usrServiceHttpClient.addUserAndAddress(address);
		return "done";
	}

}
