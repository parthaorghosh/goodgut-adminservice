package com.techxi.goodgut.core.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techxi.goodgut.clients.SubscriptionServiceHttpClient;
import com.techxi.goodgut.clients.model.AddSubscriptionRequest;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.SubscriptionSVCApi;

@Service
public class SubscriptionSVCImpl implements SubscriptionSVCApi {
	@Autowired
	SubscriptionServiceHttpClient httpClient;
	@Override
	public String addSubscription(AddSubscriptionRequest subscription) {
		return httpClient.addSubscription(subscription);
	}

	@Override
	public List<Subscription> getSubscription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Subscription> getSubsById(int subscriptionId, boolean lazy) {
		return httpClient.getSubById(subscriptionId, lazy);

	}}
