package com.techxi.goodgut.core.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techxi.goodgut.clients.OrderServiceHttpClient;
import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.SubscriptionOrder;
import com.techxi.goodgut.core.services.OrderSVCApi;

@Service
public class OrderSCVCImpl implements OrderSVCApi{
	@Autowired
	OrderServiceHttpClient client;
	@Override
	public List<Order> getOrders(String searchCriteria) {
		return client.getOrder(searchCriteria);
	}
	@Override
	public List<SubscriptionOrder> getSubOrders(String searchCriteria) {
		return client.getSubOrder(searchCriteria);
	}
	@Override
	public String updateOrders(String updateCriteria) {
		return client.updateOrder(updateCriteria);
	}
	@Override
	public Optional<Order> getOrder(int orderId) {
		// TODO Auto-generated method stub
		return client.getOrderById(orderId);
	}}
