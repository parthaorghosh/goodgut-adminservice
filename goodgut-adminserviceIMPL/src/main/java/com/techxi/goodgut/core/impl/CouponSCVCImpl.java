package com.techxi.goodgut.core.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techxi.goodgut.clients.CouponServiceHttpClient;
import com.techxi.goodgut.clients.model.Coupon;
import com.techxi.goodgut.core.services.CouponSVCApi;

@Service
public class CouponSCVCImpl implements CouponSVCApi{
	@Autowired
	CouponServiceHttpClient client;

	@Override
	public void addCoupon(Coupon coupon) {
		client.addCoupon(coupon);
	}
}
