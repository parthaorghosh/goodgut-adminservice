package com.techxi.goodgut.core.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techxi.goodgut.clients.AddressServiceHttpClient;
import com.techxi.goodgut.clients.OrderServiceHttpClient;
import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.core.services.AddressSVCApi;

@Service
public class AddressSCVCImpl implements AddressSVCApi{
	@Autowired
	AddressServiceHttpClient client;

	@Override
	public Address getAddressById(int id) {
		// TODO Auto-generated method stub
		return client.getAddressById(id);
	}
	



}
