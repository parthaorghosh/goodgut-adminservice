package com.techxi.goodgut.core.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.server.goodgut.admin.response.json.GoodgutResponseCode;
import com.techxi.goodgut.clients.MealServiceHttpClient;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.MealItemsSVCApi;

@Service
public class MealItemsSCVCImpl implements MealItemsSVCApi {

	@Autowired
	MealServiceHttpClient mealServiceHttpClient;

	@Override
	public void addMenutItem(MenuItem menuItem) {
		mealServiceHttpClient.addMealItems(menuItem);

	}

	@Override
	public BaseEntityResponse getMenuItems(int count, int pageNumber) {

		List<MenuItem> menuitems = mealServiceHttpClient.getAllMenuItems(count, pageNumber);

		BaseEntityResponse response = new BaseEntityResponse(menuitems, 100, 1);
		if (menuitems != null && !menuitems.isEmpty()) {
			response.setResponseCode(GoodgutResponseCode.SUCCESS);
		} else {
			response.setResponseCode(GoodgutResponseCode.FAIL);
		}

		return response;
	}

	@Override
	public void addMeals(Meals meals) {
		mealServiceHttpClient.addMeal(meals);

	}

	@Override
	public BaseEntityResponse getMeals() {
		
		List<Meals> meals = mealServiceHttpClient.getMeals();

		BaseEntityResponse response = new BaseEntityResponse(meals, 100, 1);
		if (meals != null && !meals.isEmpty()) {
			response.setResponseCode(GoodgutResponseCode.SUCCESS);
		} else {
			response.setResponseCode(GoodgutResponseCode.FAIL);
		}

		return response;
		
	}
	
	@Override
	public List<Meals> getAllMeals() {
		return mealServiceHttpClient.getMeals();
	}

	@Override
	public void addSubscription(Subscription subscription) {
		mealServiceHttpClient.addSubscription(subscription);

	}

	@Override
	public List<Subscription> getSubscription() {

		return mealServiceHttpClient.getSubscription();
	}

	@Override
	public List<MenuItem> getMenuItemByName(String itemName) {
		return mealServiceHttpClient.getMenuItemsByName(itemName);
	}

	@Override
	public void editMenutItem(MenuItem menuItem) {
		mealServiceHttpClient.editMealItems(menuItem);

	}

	@Override
	public List<Meals> getMealsByName(String mealName) {
		return mealServiceHttpClient.getMealsByName(mealName);
	}

	@Override
	public void editMeals(Meals meals) {
		mealServiceHttpClient.editMeal(meals);

	}

}
