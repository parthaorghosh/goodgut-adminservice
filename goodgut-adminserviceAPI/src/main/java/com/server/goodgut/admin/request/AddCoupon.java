package com.server.goodgut.admin.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



/**
 * @author ANBC
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class AddCoupon {
	
	  @NotNull
	  private String couponCode;
	  private String  marketingMessag;
	  private String discountType;
	  private String status;
	  private int  discountValue;
	  private int maxDiscountValue;
	  private String[]  mealArray;
	  private String[]  subArray;
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getMarketingMessag() {
		return marketingMessag;
	}
	public void setMarketingMessag(String marketingMessag) {
		this.marketingMessag = marketingMessag;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(int discountValue) {
		this.discountValue = discountValue;
	}
	public int getMaxDiscountValue() {
		return maxDiscountValue;
	}
	public void setMaxDiscountValue(int maxDiscountValue) {
		this.maxDiscountValue = maxDiscountValue;
	}
	public String[] getMealArray() {
		return mealArray;
	}
	public void setMealArray(String[] mealArray) {
		this.mealArray = mealArray;
	}
	public String[] getSubArray() {
		return subArray;
	}
	public void setSubArray(String[] subArray) {
		this.subArray = subArray;
	}
	
	
}
