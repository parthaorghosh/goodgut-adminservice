package com.server.goodgut.admin.response.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponse {

private String responseCode;
private String responseMessage;
@JsonProperty("GOODGUT_RESP_CODE")
public String getResponseCode() {
	return responseCode;
}
public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
}
@JsonProperty("GOODGUT_RESP_MSG")
public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}

}
