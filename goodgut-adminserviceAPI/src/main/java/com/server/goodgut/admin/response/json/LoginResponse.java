package com.server.goodgut.admin.response.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse extends BaseResponse{

	private String globalAuthToken;
	@JsonProperty("GOODGUT_GLOBAL_AUTH_TOKEN")
	public String getGlobalAuthToken() {
		return globalAuthToken;
	}
	public void setGlobalAuthToken(String globalAuthToken) {
		this.globalAuthToken = globalAuthToken;
	}
}
