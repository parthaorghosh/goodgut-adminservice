package com.server.goodgut.admin.response.json;

import java.util.List;

public class BaseEntityResponse extends BaseResponse {

	private List<?> data;
	private int totalCount;
	private int page;
	private int pageSize;
	public BaseEntityResponse() {}
	public BaseEntityResponse(List<?> items, int count, int pageNumber) {
		this.data = items;
		this.totalCount = count;
		this.page = pageNumber;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
