package com.techxi.goodgut.core.services;

import java.util.Map;

import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.server.goodgut.admin.response.json.BaseResponse;
import com.techxi.goodgut.clients.model.ReactEntityQueryRequestModel;
import com.techxi.goodgut.clients.model.SelectSearchResponse;

public interface EntitySearchSVCApi {
	
	public SelectSearchResponse getSelectSearchEntityResponse(String inputValue,String entityName, String searchField);

	public BaseEntityResponse getEntityWithFilters(ReactEntityQueryRequestModel filterModel, String entity);

	public BaseResponse updateEntity(Map<?, ?> entityMap, String entity);

	public SelectSearchResponse getSelectSearchEntityResponse(String entity, Map<?, ?> searchModel);
	

}
