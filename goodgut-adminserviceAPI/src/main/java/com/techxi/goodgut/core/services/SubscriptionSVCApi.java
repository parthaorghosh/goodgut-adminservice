package com.techxi.goodgut.core.services;

import java.util.List;
import java.util.Optional;

import com.techxi.goodgut.clients.model.AddSubscriptionRequest;
import com.techxi.goodgut.clients.model.Subscription;


public interface SubscriptionSVCApi {
	public String addSubscription(AddSubscriptionRequest subscription);
	public List<Subscription> getSubscription();
	public Optional<Subscription> getSubsById(int subscriptionId, boolean lazy);

}
