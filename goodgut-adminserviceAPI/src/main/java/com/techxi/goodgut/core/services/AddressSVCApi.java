package com.techxi.goodgut.core.services;

import com.techxi.goodgut.clients.model.Address;

public interface AddressSVCApi {
	
	public Address getAddressById(int id);
	

}
