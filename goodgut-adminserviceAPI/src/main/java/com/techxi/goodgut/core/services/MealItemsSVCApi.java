package com.techxi.goodgut.core.services;

import java.util.List;

import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Subscription;


public interface MealItemsSVCApi {
	
	public void addMenutItem(MenuItem menuItem);
	public void editMenutItem(MenuItem menuItem);
	
	public BaseEntityResponse getMenuItems(int count,int pageNumber);
	public List<MenuItem> getMenuItemByName(String itemName);
	
	public void addMeals(Meals meals);
	public void editMeals(Meals meals);
	public BaseEntityResponse getMeals();
	public List<Meals> getMealsByName(String mealName);
	
	public void addSubscription(Subscription subscription);
	public List<Subscription> getSubscription();
	List<Meals> getAllMeals();

}
