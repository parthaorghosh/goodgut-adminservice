package com.techxi.goodgut.core.services;

import java.util.List;
import java.util.Optional;

import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.SubscriptionOrder;

public interface OrderSVCApi {
	
	public List<Order> getOrders(String searchCriteria);
	public List<SubscriptionOrder> getSubOrders(String searchCriteria);
	public String updateOrders(String updateCriteria);
	public Optional<Order> getOrder(int orderId);
	

}
