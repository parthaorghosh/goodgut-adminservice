package com.techxi.goodgut.core.services;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.User;

public interface UserServiceSVCApi {
	public User getUserDetailByUser(String userName);
	public String addUserAndAddress(Address address);

	
}
