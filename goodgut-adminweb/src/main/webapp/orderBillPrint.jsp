<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<style media='print,screen'>
.bill-wrapper{
	width: 300px;
}
.address-desc {
	font-size: 20px;
}

.header {
	text-align: center;
	font-weight: bold;
	font-size: 26px;
}

.name {
	margin-top: 3px;
	font-size: 24px;
	font-weight: bold;
}

.ptype, .contact {
	font-size: 20px;
}

.section-two {
	padding: 10px 2px;
	border-top: dashed;
	border-bottom: dashed;
}

.oid {
	text-align: right;
}

.total {
	margin-top: 50px;
	border-top: dashed 1px;
	border-bottom: dashed 1px;
}
.totaltext{
	margin-top: 50px;
	text-align: right;
	padding-right: 20px;
}
</style>

<div class="bill-wrapper">
	<div class="section-one">
		<div class="header">My Gym Food</div>
		<div style="font-weight: bold;font-size: 24px;">Delivery Address</div>
		<div class="address-desc">${address.addressLine1}</div>
		<div class="address-desc">${address.addressLine2}</div>
		<div class="address-desc">${address.addressLine3}</div>
		<div class="address-desc">${address.city}&nbsp;&nbsp;${address.state}-${address.pinCode}</div>
		<div class="name">${address.name}</div>
		<div class="contact">${address.contactNo}</div>
		<c:choose>
			<c:when test="${order.payType==COD}">
				<div class="ptype">Pay Type : Cash</div>
			</c:when>
			<c:otherwise>
				<div class="ptype">Pay Type : Paid</div>
			</c:otherwise>
		</c:choose>

	</div>
	<div style="padding: 10px 2px;" class="section-two">
		<div class="oid">Order Id #&nbsp;${order.orderId}</div>
	</div>
	<div style="display: flex" class="section-three">
		
		
		<table>
			<tr>
				<td>${meal.mealName}</td>
				<td style="text-align: end;">${mp}</td>
			</tr>
			<c:if test="${couponApplied}">
			<tr>
				<td class="totaltext">Discount :-</td>
				<td style="text-align: end;">- ${discount}</td>
			</tr>
			</c:if>
			<tr>
				<td class="totaltext">CGST @2.5% :-</td>
				<td style="text-align: end;">${tax}</td>
			</tr>
			<tr>
				<td class="totaltext">SGST @2.5% :-</td>
				<td  style="text-align: end;">${tax}</td>
			</tr>
			<tr>
				<td class="totaltext">Total :-</td>
				<td style="text-align: end;border-top: dotted;border-bottom: dotted;">${total}</td>
			</tr>
		</table>
		
	</div>

</div>
