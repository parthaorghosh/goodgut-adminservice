<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Safer</title>
    <link rel="icon" type="image/ico" href="<%=request.getContextPath()%>/img/favicon.ico" />
    <!-- Le styles -->
   
   

	
</head>

<!-- 
<c:set var="orientation" value="ltr" scope="page" />
<c:if test="${pageContext.response.locale == 'ar'}">
	<c:set var="orientation" value="rtl" scope="page" />
</c:if>
 -->

<body class="am-animate">
	<div class="am-wrapper am-fixed-sidebar">
		<c:set var="loggedUser" value="<%=request.getUserPrincipal().getName()%>" />
		<nav class="navbar navbar-default navbar-fixed-top am-top-header">
			<div class="container-fluid">
				<div class="navbar-header">
					<div class="page-title">
						<span><spring:message code="header.menu.safer" /></span>
					</div>
					<a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed">
					<span class="icon-bar">
						<span></span>
						<span></span>
						<span></span>
					</span>
					</a>
					<a href="#" class="navbar-brand"></a>
				</div>

				<div id="am-navbar-collapse" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right am-user-nav">
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
								<span class="user-name"><security:authentication property="principal" /></span>
								<span class="angle-down s7-angle-down"></span>
							</a>
							<ul role="menu" class="dropdown-menu">
								<li>
									<a href="#" onclick="showModalEditUser('<%=request.getContextPath()%>/profile/modal','<%=request.getContextPath()%>/user/tableUsers','<c:out value="${loggedUser}"/>');"><spring:message code="header.logging.profile" /></a>
								</li>
								<!--<li>
									<a href="#"><spring:message code="header.menu.language" /></a>
									<ul>
										<li><a onclick='changeLanguage("ar")'> <spring:message code="header.menu.language.ar" /></a></li>
										<li><a onclick='changeLanguage("en")'> <spring:message code="header.menu.language.en" /></a></li>
									</ul>
								</li>-->
								<li>
									<a href="<%=request.getContextPath()%>/auth/logout"><spring:message code="header.logging.signout" /></a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="am-left-sidebar">
			<div class="content">
				<div class="am-logo"></div>
				<ul class="sidebar-elements">

					<li>
						<a href="<%=request.getContextPath()%>/">
							<i class="icon s7-home"></i>
							<span class="item-menu"><spring:message code="header.menu.home" /></span>
						</a>
					</li>

					<li>
						<a href="<%=request.getContextPath()%>/tracking/list/">
							<span>
								<i class="icon s7-target"></i>
								<span class="hidden-tablet"><spring:message code="header.menu.track" /></span>
							</span>
						</a>
					</li>

					<li class="parent">
						<a href="" class="disabled has-sub-menu" disabled="disabled">
							<i class="icon s7-bell"></i>
							<span><spring:message code="header.menu.alert" /></span>
						</a>
						<ul class="sub-menu">
							<li class="title">
								<spring:message code="header.menu.alert" />
							</li>
							<li class="nav-items">
								<div class="am-scroller nano has-scrollbar">
									<div class="content nano-content">
										<ul>
											<li>
												<a href="<%=request.getContextPath()%>/alert/configuration">
													<span><spring:message code="header.menu.alert.templates" /></span>
												</a>
											</li>
											<li>
												<a href="<%=request.getContextPath()%>/alertsMgt/view">
													<span><spring:message code="header.menu.alert.management" /></span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>

					<security:authorize access="hasAnyRole('ROLE_ADMINISTRATOR','ROLE_LOCATION_AGENT')">
						<li>
							<a href="<%=request.getContextPath()%>/location/export">
						<span>
							<i class="icon s7-download"></i>
							<span class="hidden-tablet"><spring:message code="header.menu.export.location"/></span>
						</span>
							</a>
						</li>
					</security:authorize>

					<security:authorize access="hasRole('ROLE_ADMINISTRATOR')">
						<li class="parent">
							<a href="" class="disabled has-sub-menu" disabled="disabled">
								<i class="icon s7-tools"></i>
								<span><spring:message code="header.menu.alert.administration" /></span>
							</a>
							<ul class="sub-menu">
								<li class="title">
									<spring:message code="header.menu.alert.administration" />
								</li>
								<li class="nav-items">
									<div class="am-scroller nano has-scrollbar">
										<div class="content nano-content">
											<ul>
												<li>
													<a href="<%=request.getContextPath()%>/user/list">
														<span><spring:message code="header.menu.users" /></span>
													</a>
												</li>
												<li>
													<a href="<%=request.getContextPath()%>/import/view">
														<span><spring:message code="header.menu.location.update"/></span>
													</a>
												</li>
												<li>
													<a href="<%=request.getContextPath()%>/lastDevicesOwners/view">
														<span><spring:message code="header.provisionning.upload"/></span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</li>
							</ul>
						</li>
					</security:authorize>

					<security:authorize access="hasRole('ROLE_ADMINISTRATOR')">
						<li class="parent">
							<a href="#">
								<span>
									<i class="icon s7-graph2"></i>
									<span class="hidden-tablet"><spring:message code="header.menu.statistics"/></span>
								</span>
							</a>
							<ul class="sub-menu">
								<li class="title">
									<spring:message code="header.menu.statistics"/>
								</li>
								<li class="nav-items">
									<div class="am-scroller nano has-scrollbar">
										<div class="content nano-content">
											<ul id="dashboards">
											</ul>
										</div>
									</div>
								</li>
							</ul>
						</li>
					</security:authorize>
				</ul>
			</div>
		</div>

		<div class="am-content">
			<div class="page-head">
				<h2>${pageTitle}</h2>
				<ol class="breadcrumb">
					<li><a href="<%=request.getContextPath()%>/"><spring:message code="header.menu.home" /></a></li>
					<c:if test="${!empty tabTitle}">
						<c:choose>
							<c:when test="${empty tabUrl}">
								<li>${tabTitle}</li>
							</c:when>
							<c:otherwise>
								<li><a href="<%=request.getContextPath()%>${tabUrl}">${tabTitle}</a></li>
							</c:otherwise>
						</c:choose>
					</c:if>
					<li class="active">${pageTitle}</li>
				</ol>
			</div>
			<div id="uploadErrorMessages"></div>
			<div class="main-content">
				<sitemesh:write property="body" />
			</div>
		</div>

		<div id="userModal" class="modal fade modal-colored-header" tabindex="-1" role="dialog" aria-hidden="true"></div>
		<div id="exportSMSModal" class="modal fade modal-colored-header" tabindex="-1" role="dialog" aria-hidden="true"></div>
		<div id="deleteUserModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>
		<div id="deleteAlertModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>
		<div id="push"></div>
	</div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="<%=request.getContextPath()%>/js/libs/jquery.dataTables.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-transition.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-alert.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-dropdown.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-scrollspy.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-tab.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-tooltip.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-popover.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-button.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-collapse.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-carousel.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-typeahead.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-datepicker.js"></script>
	<script src="<%=request.getContextPath()%>/js/libs/DT_bootstrap.js"></script>
    <script src="<%=request.getContextPath()%>/js/libs/bootstrap-typeahead-updated.js"></script>
    <script src="<%=request.getContextPath()%>/js/users.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			//initialize the javascript
			App.init();
		});

	</script>

    
    <script type="text/javascript">
		$('.tooltipmts').tooltip({
			html : true
		});
	</script>
	<script type="text/javascript">
		$.ajaxSetup({ cache: false });
	</script>
</body>
</html>
