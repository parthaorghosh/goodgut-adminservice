<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"
	language="java"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>MyGymFood</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/libs/BsMultiSelect.min.css">



<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/logsol_cmn.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script
	src="http://1000hz.github.io/bootstrap-validator/dist/validator.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

</head>

<body class="am-animate">
	<div class="am-wrapper am-fixed-sidebar">
		<nav class="navbar navbar-inverse logsol-navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" style="background-color: #111" href="#"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li style="padding-top: 22px; color: #f5c8b3; font-size: 17px;"></li>
					<li class="dropdown"><a style="background: none"
						class="dropdown-toggle" data-toggle="dropdown" href="#"> <i
							class="fa fa-user-circle" aria-hidden="true"
							style="font-size: 30px; color: bisque;"></i>
					</a>
						<ul class="dropdown-menu">
							<li style="text-align: center; border-radius: 5px;"><a
								href="<%=request.getContextPath()%>/auth/logout"><spring:message
										code="header.logging.signout" /></a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		</nav>

		<div class="logsol-left-sidebar">
			<div class="content">
				<div class="am-logo"></div>
				<ul class="sidebar-elements">

					<li><a href="<%=request.getContextPath()%>/"> <i
							class="fa fa-home" aria-hidden="true" style="font-size: 38px;"></i>
							<span class="item-menu"><spring:message
									code="header.menu.home" /></span>
					</a></li>


					<security:authorize access="hasRole('ROLE_ADMINISTRATOR')">

						<jsp:include page="../pages/adminLeftPanel.jsp" />

					</security:authorize>


				</ul>
			</div>
		</div>
	</div>
	<div class="logsol-content">

		<div id="uploadErrorMessages"></div>
		<div class="main-content logsol-main-content">



			<!-- Operations Modals -->

			<div class="menuitem-form goodgut-nodisplay operations-forms-common">

				<div class="goodgut-form-wrapper menuitem-form-wrapper"
					id="menuitem-form-div">
					<div class="goodgut-form-head">
						<div class="goodgut-form-title">Add Menu Item</div>
						<div class="goodgut-form-close" data-close="menuitem-form-div">x</div>
					</div>

					<div class="goodgut-form-body">

						<form id="menuitem-form">
							<div class="goodgut-input-wrapper">
								<label for="menuitemname">Name : </label> <input class='goodgut-input'
									name="menuitemname" title='Illegal character set for name'
									type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="menuitemdesc">Item Description : </label> <input class='goodgut-input'
									name="menuitemdesc" title='Illegal character set for name'
									type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="itemprice">Item Price : </label> <input class='goodgut-input'
									name="itemprice" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="offerAvailable">Offer Available : </label> <input class='goodgut-input'
									type="radio" name="offerAvailable" value="true"
									style="width: 10px; margin-right: 5px">True <input class='goodgut-input'
									type="radio" name="offerAvailable" value="false"
									style="width: 10px; margin-right: 5px" checked>False
							</div>
							<div class="goodgut-input-wrapper">
								<label for="itemOfferPrice">Offer Price : </label> <input class='goodgut-input'
									name="itemOfferPrice" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="itemType">Item Type : </label> <select class='goodgut-select'
									name="itemType">
									<option value="VEG" selected>Veg</option>
									<option value="NONVEG">Non-Veg</option>
								</select>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="itemAvailableFor">Available For : </label> <select class='goodgut-select'
									name="itemAvailableFor">
									<option value="BREAKFAST" selected>Breakfast</option>
									<option value="PBREAKFAST">Post Breakfast</option>
									<option value="LUNCH">Lunch</option>
									<option value="SNACKS">Snacks</option>
									<option value="DINNER">Dinner</option>
									<option value="PDINNER">Post Dinner</option>
								</select>
							</div>


							<div class="goodgut-input-wrapper">
								<label for="itemquantity">Quantity : </label> <input class='goodgut-input'
									name="itemquantity" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="itemunit">Unit : </label> <select class='goodgut-select' name="itemunit">
									<option value="gram" selected>Gram</option>
									<option value="pieces">Pieces</option>
									<option value="ml">Milliliter</option>
								</select>
							</div>

							<div class="goodgut-input-wrapper">
								<label for="calories">Calories : </label> <input class='goodgut-input' name="calories"
									pattern="[0-9]" max-length=4
									title='Please enter number and maximum value you can set is 9999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="protein">Protein : </label> <input class='goodgut-input' name="protein"
									pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="carbs">Carbs : </label> <input class='goodgut-input' name="carbs"
									pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="fat">Fat : </label> <input class='goodgut-input' name="fat"
									pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="fibre">Fibre : </label> <input class='goodgut-input' name="fibre"
									pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="imageurl">Image Url :</label> <input class='goodgut-input' name="imageurl"
									pattern="https://.*" type="url" title="Please Enter Valid URL"
									required />
							</div>

							<div class="goodgut-input-wrapper"
								style="width: 100%; position: relative; float: left;">
								<input class='goodgut-input' id="menuitem-form-submit" type="submit"
									value="Add Menu Item" />
							</div>
						</form>
					</div>
				</div>

			</div>



			<div
				class="meal-form-main-div goodgut-nodisplay operations-forms-common">

				<div class="goodgut-form-wrapper meal-form-wrapper"
					id="meal-form-div">
					<div class="goodgut-form-head">
						<div class="goodgut-form-title">Add Meals</div>
						<div class="goodgut-form-close" data-close="meal-form-div">x</div>
					</div>

					<div class="goodgut-form-body">

						<form id="meal-form">
							<div class="goodgut-input-wrapper">
								<label for="mealname">Name : </label> <input class='goodgut-input' name="mealname"
									type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="mealdesc">Meal Description : </label> <input class='goodgut-input'
									name="mealdesc" type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="mealprice">Meal Price : </label> <input class='goodgut-input'
									name="mealprice" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="offerAvailable">Offer Available : </label> <input class='goodgut-input'
									type="radio" name="offerAvailable" value="true"
									style="width: 10px; margin-right: 5px">True <input class='goodgut-input'
									type="radio" name="offerAvailable" value="false"
									style="width: 10px; margin-right: 5px" checked>False
							</div>
							<div class="goodgut-input-wrapper goodgut-nodisplay">
								<label for="mealOfferPrice">Offer Price : </label> <input class='goodgut-input'
									name="mealOfferPrice" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="productMealType">Meal Product Category : </label>
								<input class='goodgut-input'
									name="productMealType" type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="itemType">Item Type : </label> <select
									name="itemType">
									<option value="VEG" selected>Veg</option>
									<option value="NONVEG">Non-Veg</option>
								</select>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="itemAvailableFor">Available For : </label> <select class='goodgut-select'
									name="itemAvailableFor">
									<option value="BREAKFAST" selected>Breakfast</option>
									<option value="PBREAKFAST">Post Breakfast</option>
									<option value="LUNCH">Lunch</option>
									<option value="SNACKS">Snacks</option>
									<option value="DINNER">Dinner</option>
									<option value="PDINNER">Post Dinner</option>
								</select>
							</div>

							<div class="goodgut-input-wrapper">
								<label for="calories">Calories : </label> <input class='goodgut-input' name="calories"
									pattern="[0-9]" max-length=4
									title='Please enter number and maximum value you can set is 9999'
									type="number" />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="availableOn">Available On : </label> <select class='goodgut-select'
									name="availableOn[]" multiple>
									<option value="1" selected>Monday</option>
									<option value="2">Tuesday</option>
									<option value="3">Wednesday</option>
									<option value="4">Thursday</option>
									<option value="5">Friday</option>
									<option value="6">Saturday</option>

								</select>
							</div>

							<div class="goodgut-input-wrapper">
								<label for="row">Meal View Row : </label> <input class='goodgut-input' name="row"
									pattern="[0-9]" max-length=2
									title='Please enter number and maximum value you can set is 99'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="column">Meal View Column : </label> <input
									name="column" pattern="[0-9]" max-length=2
									title='Please enter number and maximum value you can set is 99'
									type="number" required />
							</div>


							<div class="goodgut-input-wrapper">
								<label for="menuitems">Menu Items : </label> <select class='goodgut-select'
									name="menuitems[]" multiple>
									
									<c:forEach items="${menuItems}" var="item" varStatus="loop">
										<option value="${item.itemId}">${item.itemName}</option>
									</c:forEach>

								</select>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="imageurl">Image Url :</label> <input name="imageurl"
									pattern="https://.*" type="url" title="Please Enter Valid URL"
									required />
							</div>

							<div class="goodgut-input-wrapper"
								style="width: 100%; position: relative; float: left;">
								<input id="meal-form-submit" type="submit" value="Add Meal" />
							</div>
						</form>
					</div>
				</div>

			</div>
			<!--  -->

			<!-- Subscription Form -->


			<div
				class="subs-form-main-div goodgut-nodisplay operations-forms-common">

				<div class="goodgut-form-wrapper subs-form-wrapper"
					id="subs-form-div">
					<div class="goodgut-form-head">
						<div class="goodgut-form-title">Add Subscription</div>
						<div class="goodgut-form-close" data-close="subs-form-main-div">x</div>
					</div>

					<div class="goodgut-form-body">

						<form id="subs-form">
							<div class="goodgut-input-wrapper">
								<label for="subsname">Name : </label> <input name="subsname"
									type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="subsdesc">Subs Description : </label> <input
									name="subsdesc" type="text" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="subsprice">subs Price : </label> <input
									name="subsprice" pattern="[0-9]" max-length=5
									title='Please enter number and maximum value you can set is 99999'
									type="number" required />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="offerAvailable">Offer Available : </label> <input
									type="radio" name="offerAvailable" value="true"
									style="width: 10px; margin-right: 5px">True <input
									type="radio" name="offerAvailable" value="false"
									style="width: 10px; margin-right: 5px" checked>False
							</div>
							<div class="goodgut-input-wrapper goodgut-nodisplay">
								<label for="subsOfferPrice">Offer Price : </label> <input
									name="subsOfferPrice" pattern="[0-9]" max-length=5
									title='Please enter number and maximum value you can set is 99999'
									type="number" />
							</div>

							<div class="goodgut-input-wrapper">
								<label for="itemType">Item Type : </label> <select class='goodgut-select'
									name="itemType">
									<option value="VEG" selected>Veg</option>
									<option value="NONVEG">Non-Veg</option>
								</select>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="calories">Calories : </label> <input name="calories"
									pattern="[0-9]" max-length=4
									title='Please enter number and maximum value you can set is 9999'
									type="number" />
							</div>





							<div class="meals-wrapper">

								<div class="monday-meal-wrapper">
									<div class="lable">Monday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${monMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>

								<div class="tuesday-meal-wrapper">
									<div class="lable">Tuesday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${tueMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>


								<div class="wed-meal-wrapper">
									<div class="lable">Wednesday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${wedMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>


								<div class="thu-meal-wrapper">
									<div class="lable">Thursday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${thuMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>


								<div class="fri-meal-wrapper">
									<div class="lable">Friday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${friMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>

								<div class="sat-meal-wrapper">
									<div class="lable">Saturday Meals :</div>
									<div class="submeal-select-box">
										<c:forEach items="${satMeals}" var="meal" varStatus="loop">
											<div class="select-meals" goodgut-data="${meal.mealId}">${meal.mealName}</div>
										</c:forEach>

									</div>
									<div class="switches">
										<div class="move-to-right">
											<i class="fa fa fa-angle-double-right"></i>
										</div>
										<div class="move-to-left">
											<i class="fa fa fa-angle-double-left"></i>
										</div>
									</div>
									<div class="submeal-selected-box"></div>
								</div>

							</div>

							<div class="goodgut-input-wrapper">
								<label for="subperiod">Sub Period : </label> <select class='goodgut-select'
									name="subperiod">
									<option value="WEEKLY" selected>WEEKLY</option>
									<option value="TWOWEEKS" selected>TWOWEEKS</option>
									<option value="FOURWEEKS" selected>FOURWEEKS</option>
									<option value="MONTHLY" selected>Monthly</option>
									<option value="QUATERLY">QUATERLY</option>
									<option value="HALFYEARLY">HALFYEARLY</option>
									<option value="YEARLY">YEARLY</option>

								</select>
							</div>

							<div class="goodgut-input-wrapper">
								<label for="imageurl">Image Url :</label> <input name="imageurl"
									pattern="https://.*" type="url" title="Please Enter Valid URL"
									required />
							</div>

							<div class="goodgut-input-wrapper"
								style="width: 100%; position: relative; float: left;">
								<input id="sub-form-submit" type="submit" value="Add Subs" />
							</div>
						</form>
					</div>
				</div>

			</div>

			<!-- Coupon Forms -->

			<div
				class="coupon-form-main-div goodgut-nodisplay operations-forms-common">

				<div class="goodgut-form-wrapper coupon-form-wrapper"
					id="coupon-form-div">
					<div class="goodgut-form-head">
						<div class="goodgut-form-title">Add New Coupon</div>
						<div class="goodgut-form-close" data-close="coupon-form-main-div">x</div>
					</div>

					<div class="goodgut-form-body">

						<form id="coupon-form" style="display: flex;flex-direction: column;width: 1100px;height: 600px;overflow: auto;">
							<div class="goodgut-input-wrapper">
								<label for="couponcode">Coupon Code : </label> <input
									name="couponcode" title='Rang 6-10 Aplha Numeric' type="text"
									required />
							</div>
							<div class="goodgut-input-wrapper">
								<label style="width:150px;"for="marketingMessage">Marketing Message : </label> <input class='goodgut-input' 
									name="marketingMessage" title='Illegal character set for name'
									type="text" />
							</div>
							<div class="goodgut-input-wrapper">
								<label style="width:150px;" for="discountType">Discount Type : </label> <select class='goodgut-select'
									name="discountType">
									<option value="PERCENTAGE">Percentage</option>
									<option value="RUPEE" selected>Rupee</option>
								</select>
							</div>
								<div class="goodgut-input-wrapper">
								<label style="width:150px;" for="status">Available For Subs : </label> <select class='goodgut-select'
									id="availableForSubs" name="availableForSubs[]"
									multiple="multiple">

									<c:forEach items="${subs}" var="sub" varStatus="loop">
										<option value="${sub.subscriptionId}">${sub.name}</option>
									</c:forEach>

								</select>
							</div>
						
							<div class="goodgut-input-wrapper">
								<label style="width:150px;" for="status">Available For Meals : </label> <select class='goodgut-select'
									id="availableForMeals" name="availableForMeals[]"
									multiple="multiple">

									<c:forEach items="${meals}" var="meal" varStatus="loop">
										<option value="${meal.mealId}">${meal.mealName}-
											${meal.mealProductName}</option>
									</c:forEach>


								</select>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="discountValue">Discount Value : </label> <input class='goodgut-input'
									name="discountValue" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label for="maxDiscountValue">Max Dixcount Value : </label> <input class='goodgut-input'
									name="maxDiscountValue" pattern="[0-9]" max-length=3
									title='Please enter number and maximum value you can set is 999'
									type="number" required />
							</div>
							<div class="goodgut-input-wrapper">
								<label style="float: left;">Valid From Date : </label>

								<div class="goodgut-datepicker-wrapper date input-group"
									id="coupondpfromdate">
									<input class='goodgut-input' id="couponfromdate" style="" type="text"
										class="form-control" name="couponfromdate" /> <span
										class="input-group-addon add-on"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
							<div class="goodgut-input-wrapper">
								<label style="float: left;">Valid Till Date : </label>

								<div class="goodgut-datepicker-wrapper date input-group"
									id="coupondptodate">
									<input class='goodgut-input' id="coupontodate" style="" type="text"
										class="form-control" name="coupondptodate" /> <span
										class="input-group-addon add-on"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
							<div class="goodgut-input-wrapper">
								<label for="status">Status : </label> <select class='goodgut-select' name="status">
									<option value="ACTIVE" selected>Active</option>
									<option value="INACTIVE">Inactive</option>
								</select>
							</div>


							<div class="goodgut-input-wrapper"
								style="width: 100%; position: relative; float: left;">
								<input class='goodgut-input' id="coupon-form-submit" type="submit" value="Add Coupon" />
							</div>
						</form>
					</div>
				</div>

			</div>



			<!--  -->
			<!-- Order Search -->

			<div
				class="order-search-box-main-div goodgut-nodisplay operations-forms-common">

				<div class="order-search-box-wrapper">

					<form id="search-order-form">
						<div class="goodgut-input-wrapper">
							<label for="orderId">Order Id : </label> <input
								class="search-oid goodgut-input" name="orderId" pattern="[0-9]" max-length=15
								title='Please enter number' type="number" />
						</div>
						<div class="goodgut-input-wrapper">
							<label style="float: left;">From Date : </label>

							<div class="goodgut-datepicker-wrapper date input-group"
								id="datepickerfromdate">
								<input class='goodgut-input' id="osearchfromdate" style="" type="text"
									class="form-control" name="date" /> <span
									class="input-group-addon add-on"><span
									class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						<div class="goodgut-input-wrapper">
							<label style="float: left;">To Date : </label>

							<div class="goodgut-datepicker-wrapper date input-group"
								id="datepickertodate">
								<input class='goodgut-input' id="osearchtodate" style="" type="text"
									class="form-control" name="date" /> <span
									class="input-group-addon add-on"><span
									class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="goodgut-input-wrapper"
							style="width: 100%; position: relative; float: left;">
							<input class='goodgut-input' id="search-order-form-submit" type="submit"
								value="Get Orders" />
						</div>

					</form>

				</div>
				<div class="order-search-box-result-wrapper"></div>

			</div>
			
			
			<!--  -->
						<div
				class="suborder-search-box-main-div goodgut-nodisplay operations-forms-common">

				<div class="suborder-search-box-wrapper">

					<form id="search-suborder-form">
						<div class="goodgut-input-wrapper">
							<label for="orderId">Order Id : </label> <input
								class="search-sub-oid goodgut-input" name="orderId" pattern="[0-9]" max-length=15
								title='Please enter number' type="number" />
						</div>
						<div class="goodgut-input-wrapper">
							<label style="float: left;">From Date : </label>

							<div class="goodgut-datepicker-wrapper date input-group"
								id="subdatepickerfromdate">
								<input class='goodgut-input' id="subosearchfromdate" style="" type="text"
									class="form-control" name="date" /> <span
									class="input-group-addon add-on"><span
									class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						<div class="goodgut-input-wrapper">
							<label style="float: left;">To Date : </label>

							<div class="goodgut-datepicker-wrapper date input-group"
								id="subdatepickertodate">
								<input class='goodgut-input' id="subosearchtodate" style="" type="text"
									class="form-control" name="date" /> <span
									class="input-group-addon add-on"><span
									class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="goodgut-input-wrapper"
							style="width: 100%; position: relative; float: left;">
							<input class='goodgut-input' id="search-order-form-submit" type="submit"
								value="Get Orders" />
						</div>

					</form>

				</div>
				<div class="suborder-search-box-result-wrapper"></div>

			</div>
			
			
			
			
			<!-- Edit MenuItem -->


		</div>
	</div>
	<div class="logsol-alert-container goodgut-nodisplay">
		<div class="logsol-alert-close"
			style="position: absolute; right: 14px; cursor: pointer; width: 20px; font-size: 20px;">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>
		<div class="logsol-alert-body" style="width: 500px;">
			<span class="type" style="font-weight: bolder;">Success!</span>&nbsp;
			<span class="text">you Have made It</span>

			<div class="edit-menuItem-box-main-div goodgut-nodisplay operations-forms-common">

					<div class="edit-menuItem-box-wrapper">
							
							<form id="edit-item-form">
							<div class="goodgut-input-wrapper">
								<label for="menuItem">SearchItem : </label> <input class="edit-oid"
									name="menuItem"
									title='Please enter menu item'
									type="text" />
							</div>
						
							<div class="goodgut-input-wrapper"
								style="float: left;padding:14px 0px 0px 14px;">
								<input id="edit-menuitem-form-submit" type="submit" value="Get Items" />
							</div>
							
							</form>
					
					</div>
					<div class="edit-menuItem-box-result-wrapper"></div>
				
			</div>

		</div>
	</div>
	
			

	

	<div class="goodgut-modal goodgut-modal-spinner goodgut-nodisplay">
		<div class="spinner-cntnr">
			<i class="fa fa-spinner"></i>
		</div>
	</div>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<script type="text/javascript">
		
	</script>
	<script src="<%=request.getContextPath()%>/js/goodgut_common.js"></script>
	<script src="<%=request.getContextPath()%>/js/operations.js"></script>
	<script src="<%=request.getContextPath()%>/js/BsMultiSelect.min.js"></script>

</body>
</html>
