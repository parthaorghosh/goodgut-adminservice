<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<body>
	<div class="panel panel-default">
		<div class="panel-heading">
			<%-- <img src="<%=request.getContextPath()%>/img/logo_original.png" alt="logo" class="logo-img" width="150px" /> --%>
			<span><spring:message code="login.description" /></span>
		</div>
		<div class="panel-body">
			<form action="/admin/login" method="post" class="form-horizontal" >
				<div class="login-form">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
							<input name="username" class="form-control" type="text" id="username" autofocus="autofocus" autocomplete="off" aria-invalid="false" placeholder="<spring:message code="login.login" />" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
							<input type="password" class="form-control" id="password" name="password" aria-invalid="false" placeholder="<spring:message code="login.password" />" />
						</div>
					</div>
					<c:if test="${! empty error}">
					<div class="alert error">
						<spring:message code="${error}"/><br/>
					</div>
					</c:if>
					<div class="form-group login-submit">
						<button name="submit" type="submit" class="btn btn-lg btn-primary">
							<spring:message code="login.button.submit" />
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>