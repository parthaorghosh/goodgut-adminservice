<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table cellpadding="0" cellspacing="0" border="0" 
								class="table table-striped table-fw-widget">
					
					<thead>
					<tr>
						<th>Order-Id</th>
						<th>Order-By</th>
						<th>Oder-Type</th>
						<th>Oder-Date</th>
						<th>Delivery-Date</th>
						<th>Order-Status</th>
						<th>Delivered-By</th>
						<th>Meal-Name</th>
						<th>Meal-Type</th>
						<th>VEG or NON-VEG</th>
						<th>Orignal-Price</th>
						<th>Discount-Price</th>
						<th>Coupon Code/th>
						<th>Edit-Order</th>
						<th>Get-Address</th>
						<th>Print</th>
					</tr>
					<thead>
					<tbody>
					<c:forEach items="${orders}" var="order" varStatus="loop">
						
						<tr goodgut-data="${order.orderId}" goodgut-delivery-id="${order.deliveryAddressId}">
							<td class="orderid">${order.orderId}</td>
							<td class="orderby">${order.user.userName}</td>
							<td class="ordertype">${order.orderCatagory}</td>
							<td class="orderdate">${order.formatedOrderDate}</td>
							<td class="deliverydate">${order.formateddeliveryDate}</td>
							<td class="orderstatus">${order.orderStatus}</td>
							<td class="deliveredby">${order.deliveredBy}</td>
							<td style="cursor:pointer;"class="meal-name">
								<div class="goodgut-nodisplay meal-menuitem-details">
									<c:forEach items="${order.meal.mealItems}" var="mi" varStatus="loop">
										<span style="display:block;" class="menuitem">${mi.itemName}</span>
									</c:forEach>
								</div>
								<span style="pointer-events:none" class="meal-popover-container">${order.meal.mealName}</span>
							</td>
							<td class="mealType">${order.meal.mealType}</td>
							<td class="vegOrNonVeg">${order.meal.mealSubscriptionCatagory}</td>
							<td class="mealOrignalPrice">${order.meal.mealPrice}</td>
							<td class="discountAmount">${order.discountAmount}</td>
							<td class="couponId">${order.couponId}</td>
							<td><i style="cursor: pointer;" class="edit-order fa fa-edit"></i>
								<i style="cursor: pointer;margin-right: 5px;" class="updateorder goodgut-nodisplay fa fa-check"></i>
								<i style="cursor: pointer;" class="updateorder-cancel goodgut-nodisplay fa fa-close"></i>
								<i style="position: absolute;font-size: 15px;margin-left: 25px;" class="oedit-upload-spinner fa-spin fa fa-spinner goodgut-nodisplay"></i>
							</td>
							<td class="get-delivery-address" style="cursor:pointer"><i style="pointer-events:none" class="address-popover fa fa-home"></i></td>
							<td><a class="printbill" href="#"><i class="fa fa-print"></i></a></td>
							   
						</tr>
					</c:forEach>
					</tbody>
				</table>