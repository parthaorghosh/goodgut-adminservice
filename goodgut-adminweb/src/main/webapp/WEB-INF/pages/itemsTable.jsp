<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table cellpadding="0" cellspacing="0" border="0" 
								class="table table-striped table-fw-widget">
					
					<thead>
					<tr>
						<th>Item-Id</th>
						<th>item-Image</th>
						<th>Item-Description</th>
						<th>Item-Nutrition</th>
						<th>Item-Name</th>
						<th>Item-Type</th>
						<th>Item-ViewColumn</th>
						<th>Item-ViewRow</th>
						<th>Meal-Type</th>
						<th>Offer-Available</th>
						<th>Item-offerprice</th>
						<th>Item-price</th>
						<th>Edit</th>
					</tr>
					<thead>
					<tbody>
					<c:forEach items="${items}" var="item" varStatus="loop">
						
						<tr goodgut-data="${item.itemId}">
							<td class="itemId">${item.itemId}</td>
							<td class="itemImage">${item.imageUrl}</td>
							<td class="itemDescription">${item.itemDescription}</td>
							<td class="itemNutrition">${item.itemMetadata}</td>
							<td class="itemName">${item.itemName}</td>
							<td class="itemType">${item.itemType}</td>
							<td class="itemViewRow">${item.itemViewRow}</td>
							<td class="itemViewColumn">${item.itemViewColumn}</td>
							<td class="itemMealType">${item.mealType}</td>
							<td class="itemOfferAvailable">${item.offerAvail}</td>
							<td class="itemOfferPrice">${item.offerPrice}</td>
							<td class="itemPrice">${item.price}</td>
							<td><i style="cursor: pointer;" class="edit-order fa fa-edit"></i>
								<i style="cursor: pointer;margin-right: 5px;" class="updateorder goodgut-nodisplay fa fa-check"></i>
								<i style="cursor: pointer;" class="updateorder-cancel goodgut-nodisplay fa fa-close"></i>
								<i style="position: absolute;font-size: 15px;margin-left: 25px;" class="oedit-upload-spinner fa-spin fa fa-spinner goodgut-nodisplay"></i>
							</td>							   
						</tr>
					</c:forEach>
					</tbody>
				</table>