<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table cellpadding="0" cellspacing="0" border="0" 
								class="table table-striped table-fw-widget">
					
					<thead>
					<tr>
						<th>Order-Id</th>
						<th>Order-By</th>
						<th>Order-Date</th>
						<th>Sub-Start-Date</th>
						<th>Sub-End-Date</th>
						<th>Sub-Name</th>
						<th>VEG or NON-VEG</th>
						<th>Orignal-Price</th>
						<th>Discount-Price</th>
						<th>Coupon Code/th>
						<th>Get-Address</th>
						<th>Print</th>
					</tr>
					<thead>
					<tbody>
					<c:forEach items="${suborders}" var="order" varStatus="loop">
						
						<tr goodgut-data="${order.orderId}" goodgut-delivery-id="${order.subscriptionAddr}">
							<td class="orderid">${order.orderId}</td>
							<td class="orderby">${order.user.userName}</td>
							<td class="orderdate">${order.formatedSubOrderDate}</td>
							<td class="subOrderStartDate">${order.formatedSubStartDate}</td>
							<td class="subOrderEndDate">${order.formatedSubEndDate}</td>
							<td style="cursor:pointer;"class="sub-name">${order.subscription.name}</td>
							<td class="vegOrNonVeg">${order.subscription.mealSubscriptionCatagory}</td>
							<td class="subOrignalPrice">${order.subscription.price}</td>
							<td class="discountAmount">${order.discountAmount}</td>
							<td class="couponId">${order.couponId}</td>
							<td class="get-subdelivery-address" style="cursor:pointer"><i style="pointer-events:none" class="address-popover fa fa-home"></i></td>
							<td><a class="subprintbill" href="#"><i class="fa fa-print"></i></a></td>
							   
						</tr>
					</c:forEach>
					</tbody>
				</table>