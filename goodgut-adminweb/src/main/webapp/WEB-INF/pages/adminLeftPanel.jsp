<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<li class="parent">
	<a href="#" class="disabled has-sub-menu"
		disabled="disabled"><i class="fa fa-cogs" aria-hidden="true"
		style="font-size: 38px;"></i> <i class="fa fa-shield"
		style="left: -4px; z-index: 2; position: relative; color: #f26b6b; font-size: 22px; top: 6px;"></i>
		<span><spring:message code="left.panel.administration.operations" /></span>
	</a>
	<ul class="sub-menu goodgut-nodisplay">
		<li class="title"><spring:message code="administration.operations.header.title.menu" /></li>
		<li class="nav-items">
			<ul>
				<li><a href="#" id="add-menu-item"> <span goodgut-toggle="menuitem-form"
						class="logsol-li-typea operations-sub-item-click"><i class="fa fa-plus"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.menu.add" /></span>
				</a></li>
				
				<li><a href="#" id="add-meal"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="meal-form-main-div"><i class="fa fa-plus"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.meal.add" /></span>
				</a></li>
				
				<li><a href="#" id="add-subs"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="subs-form-main-div"><i class="fa fa-plus"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.sub.add" /></span>
				</a></li>
				
				
				<li><a href="#" id="add-coupon"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="coupon-form-main-div"><i class="fa fa-plus"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.coupon.add" /></span>
				</a></li>
				
				
				<li><a href="#" id="search-order"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="order-search-box-main-div"><i class="fa fa-search"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.order.search" /></span>
				</a></li>
				
				<li><a href="#" id="search-order"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="suborder-search-box-main-div"><i class="fa fa-search"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.suborder.search" /></span>
				</a></li>
				
				<li><a href="#" id="edit-order"> <span
						class="logsol-li-typea operations-sub-item-click" goodgut-toggle="edit-menuItem-box-main-div"><i class="fa fa-edit"
							aria-hidden="true" 
							style="position: relative; top: 2px; margin-right: 5px;"></i> 
							<spring:message code="administration.operations.edit.Item" /></span>
				</a></li>
				
				
			</ul>

		</li>
	</ul>
</li>