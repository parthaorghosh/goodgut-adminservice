<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%! 
	final String cdn = null;
%>


<!DOCTYPE html>	
    <html>

    <head>
		<base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="My Gym Food">
            <title>
                My Gym Food Admin
            </title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
			<% if(cdn != null && !"none".equals(cdn)) {%>
			<link rel="stylesheet" type="text/css" href="<%= cdn %>/approve/da2661e66272d6e4b7c9.css"></link>
			<% } else { %>
			<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/static/da2661e66272d6e4b7c9.css"></link>
			<% } %>
    </head>


    <body style="overflow:hidden">
        <div id="root-element"><div id="loader"></div></div>
        <script>
        var root = location.protocol + '//' + location.host;
            var contextPath = "<%=request.getAttribute("contextPath")%>";
			var userNodeId = "<%=request.getAttribute("userNodeId")%>"
			var entries = [];
			var cdnPath;
			<% if(cdn != null && !"none".equals(cdn)) { %>
			var service = window.location.href.split("/")[2].split(".")[0];
			cdnPath = "<%= cdn %>" +  "/" + service;
			<% } else {%>
				console.log(cdnPath);
				cdnPath = "<%= request.getContextPath() %>/static";
			<% } %>
			
			
				console.log("/262c5d331d0cac160fa0.js");
				entries.push("/262c5d331d0cac160fa0.js");
			entries.forEach(function(entry) {
				var script = document.createElement("script");
				script.src = cdnPath + entry;
				document.body.appendChild(script);
			});
			
			
			
			</script>
       
    </body>

    </html>