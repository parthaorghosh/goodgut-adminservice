$(document).ready(function(){
	operations = {
			addMenuItem : function(data,button) {
				goodgut.showSpinner();
				$.ajax({
					url : "meal/addItem",
					type : 'POST',
					data : data,
					success : function(response) {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
						if (response != 'ERR' && response != '') {

							
							goodgut.showSuccess("Successfully Added Item.");

						} else {
						 goodgut.showError("Error While Adding New Item");
						}
					},
					error : function() {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
					goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			addCoupon:function(data,button){
				goodgut.showSpinner();
				$.ajax({
					url : "coupon/addCoupon",
					type : 'POST',
					data : data,
					success : function(response) {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
						if (response =="RESP_200") {
							goodgut.showSuccess("Successfully Added Couon.");

						} else {
						 goodgut.showError("Error While Adding");
						}
					},
					error : function() {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
					 goodgut.showError("Technical Problem Please Try Later");
					}
				});

				
			},
			addMeal : function(data,button) {
				goodgut.showSpinner();
				$.ajax({
					url : "meal/addmeal",
					type : 'POST',
					data : data,
					success : function(response) {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
						if (response =="RESP_200") {
							goodgut.showSuccess("Successfully Added Meal.");

						} else {
						 goodgut.showError("Error While Adding");
						}
					},
					error : function() {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
					 goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			addSubs : function(data,button) {
				goodgut.showSpinner();
				$.ajax({
					url : "meal/addSubscription",
					type : 'POST',
					data : data,
					success : function(response) {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
						if (response =="RESP_200") {
							goodgut.showSuccess("Successfully Added Subs.");

						} else {
						goodgut.showError("Error While Adding");
						}
					},
					error : function() {
						button.removeAttr('disabled');
						goodgut.hideSpinner();
						goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			editItem : function(data) {
				goodgut.showSpinner();
				$.ajax({
					url : "/admin/meal/getItemsByName",
					type : 'POST',
					data : data,
					success : function(response) {
						goodgut.hideSpinner();
						if (response.trim()!="") {
							$(".edit-menuItem-box-result-wrapper").html(response);
							$(".edit-menuItem-box-result-wrapper table").DataTable();
							
						} else {
						}
					},
					error : function() {
						goodgut.hideSpinner();
						//goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			getOrders : function(data) {
				goodgut.showSpinner();
				$.ajax({
					url : "orders/getOrder",
					type : 'POST',
					data : data,
					success : function(response) {
						goodgut.hideSpinner();
						if (response.trim()!="") {
							$(".order-search-box-result-wrapper").html(response);
							$(".order-search-box-result-wrapper table").DataTable(
									
									{
									    "fnDrawCallback": function( oSettings ) {
									    	operations.editOrders();
											operations.updateOrder();
											operations.cancelUpdate();
											operations.mealDetailPopup();
											operations.getDeliveryAddress();
											operations.printBill();
									    }
									  }
							
							);
							operations.editOrders();
							operations.updateOrder();
							operations.cancelUpdate();
							operations.mealDetailPopup();
							operations.getDeliveryAddress();
							operations.printBill();
							
						} else {
						 goodgut.showError("Error While Getting Orders");
						}
					},
					error : function() {
						goodgut.hideSpinner();
						 goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			getSubOrders : function(data) {
				goodgut.showSpinner();
				$.ajax({
					url : "orders/getSubOrder",
					type : 'POST',
					data : data,
					success : function(response) {
						console.log(response);
						goodgut.hideSpinner();
						if (response.trim()!="") {
							$(".suborder-search-box-result-wrapper").html(response);
							$(".suborder-search-box-result-wrapper table").DataTable(
									
									{
									    "fnDrawCallback": function( oSettings ) {
									    	
											
									    }
									  }
							
							);
							
						} else {
						 goodgut.showError("Error While Getting Orders");
						}
					},
					error : function() {
						goodgut.hideSpinner();
						 goodgut.showError("Technical Problem Please Try Later");
					}
				});

			},
			getOrderStatusOptions:function(currentStatus){
				var result="";
				if(currentStatus == "ORDERACTIVE"){
					result="<option selected>ORDERACTIVE</option><option>INKITCHEN</option>";
				}
				else if(currentStatus == "INKITCHEN"){
					result="<option selected>INKITCHEN</option><option>TRANSIT</option>";
				}
				else if(currentStatus == "TRANSIT"){
					result="<option selected>TRANSIT</option><option>DELIVERED</option>";
				}
				else{
					result="<option selected>ORDERACTIVE</option><option>INKITCHEN</option>" +
							"<option >PAYMENT_PENDING</option><option>TRANSIT</option>" +
							"<option >PAYMENT_FAILURE</option><option>DELIVERED</option>";
					
				}
				return result;
				
				
			},
			editOrders:function(){
				$(".edit-order").unbind();
				$(".edit-order").click(function(e){
					
						var deliveryDateSelector = $(this).parent().siblings(".deliverydate");
						var orderStatusSelector = $(this).parent().siblings(".orderstatus");
						var deliverdBySelector = $(this).parent().siblings(".deliveredby");
						var selectors=[];
						selectors.push(deliveryDateSelector);
						selectors.push(orderStatusSelector);
						selectors.push(deliverdBySelector);
						for(var i=0 ;i<selectors.length;i++){
							var oldValDate = selectors[i].text();	
							selectors[i].attr("previous-value",oldValDate);
						}
						var selectHtml = "<select>"+operations.getOrderStatusOptions(orderStatusSelector.text().trim())+"</select>"
						orderStatusSelector.html(selectHtml);
						var deliveryDate='<div class="date input-group" id="datepickereditdd" style="width:185px">'+
									'<input style="font-size:11px;" type="text" class="editdd form-control" name="date" />'+
										'<span class="input-group-addon add-on">'+
												'<span class="glyphicon glyphicon-calendar"></span></span></div>';
						deliveryDateSelector.html(deliveryDate);
						
						if(!deliverdBySelector.text())
							{
							var deliveredBy = "<input type='text'/ class='editdeliveryby'>"
							deliverdBySelector.html(deliveredBy);
							}
						
						$(this).addClass("goodgut-nodisplay");
						$(this).siblings(".updateorder-cancel,.updateorder").removeClass("goodgut-nodisplay");
						// activate datepicker
						deliveryDateSelector.find("#datepickereditdd").datetimepicker({
							format: 'DD/MM/YYYY HH:mm:ss'
						 });
						
				});
			},
			updateOrder:function(){
				$(".updateorder").unbind();
				$(".updateorder").click(function(e){
					var selector = $(this);
					$(this).addClass("goodgut-nodisplay");
					$(this).siblings(".updateorder-cancel,.updateorder").addClass("goodgut-nodisplay");
					$(this).siblings(".oedit-upload-spinner").removeClass("goodgut-nodisplay");
					var id = $(this).parent().parent().attr("goodgut-data");
					var deliveryDateSelector = $(this).parent().siblings(".deliverydate");
					var orderStatusSelector = $(this).parent().siblings(".orderstatus");
					var deliverdBySelector = $(this).parent().siblings(".deliveredby");
					deliveryDateSelector.addClass("goodgut-noclick");
					orderStatusSelector.addClass("goodgut-noclick");
					deliverdBySelector.addClass("goodgut-noclick");
					
					var oldddVal = deliveryDateSelector.attr("previous-value");
					var oldostatusVal = orderStatusSelector.attr("previous-value");
					var olddeliveryByVal = deliverdBySelector.attr("previous-value");
					
					var ddVal = deliveryDateSelector.find("input").val();
					var ostatusVal = orderStatusSelector.find("select").val();
					var deliveryByVal = deliverdBySelector.find("input").val();
					
					var data = {};
					data["id"] = id;
					data["dd"]=ddVal;
					data["status"]=ostatusVal;
					data["deliverBy"]=deliveryByVal;
					
					$.ajax({
						url : "orders/updateOrder",
						type : 'POST',
						data : data,
						success : function(response) {
							selector.siblings(".oedit-upload-spinner").addClass("goodgut-nodisplay");
							selector.siblings(".edit-order").removeClass("goodgut-nodisplay");

							deliveryDateSelector.removeClass("goodgut-noclick");
							orderStatusSelector.removeClass("goodgut-noclick");
							deliverdBySelector.removeClass("goodgut-noclick");
							if (response.trim()=="GA_200") {
								deliveryDateSelector.html(ddVal);
								orderStatusSelector.html(ostatusVal);
								deliverdBySelector.html(deliveryByVal);
								
								
							} else {
									
								deliveryDateSelector.html(oldddVal);
								orderStatusSelector.html(oldostatusVal);
								deliverdBySelector.html(olddeliveryByVal);
							}
						},
						error : function() {
							selector.siblings(".oedit-upload-spinner").addClass("goodgut-nodisplay");
							selector.siblings(".edit-order").removeClass("goodgut-nodisplay");
							
							deliveryDateSelector.html(oldddVal);
							orderStatusSelector.html(oldostatusVal);
							deliverdBySelector.html(olddeliveryByVal);

							deliveryDateSelector.removeClass("goodgut-noclick");
							orderStatusSelector.removeClass("goodgut-noclick");
							deliverdBySelector.removeClass("goodgut-noclick");
						}
					});
					
					
				});
			},
			cancelUpdate:function(){
				$(".updateorder-cancel").unbind();
				$(".updateorder-cancel").click(function(){
					var deliveryDateSelector = $(this).parent().siblings(".deliverydate");
					var orderStatusSelector = $(this).parent().siblings(".orderstatus");
					var deliverdBySelector = $(this).parent().siblings(".deliveredby");
					var oldddVal = deliveryDateSelector.attr("previous-value");
					var oldostatusVal = orderStatusSelector.attr("previous-value");
					var olddeliveryByVal = deliverdBySelector.attr("previous-value");
					deliveryDateSelector.html(oldddVal);
					orderStatusSelector.html(oldostatusVal);
					deliverdBySelector.html(olddeliveryByVal);
					$(this).siblings(".edit-order").removeClass("goodgut-nodisplay");
					$(this).siblings(".updateorder").addClass("goodgut-nodisplay");
					$(this).addClass("goodgut-nodisplay");
				});
			},
			mealDetailPopup:function(){
				$(".meal-name").unbind();
				$(".meal-name").click(function(){
				   // hover on
					var open = $(this).attr("popover-open");
					var popoverSelector=$(this).find(".meal-popover-container");
					if(!open){
					var html = $(this).find(".meal-menuitem-details").html();
						var  popoverJSON = {};
						$(this).attr("popover-open","true");
						popoverJSON["html"] = true;
						popoverJSON["placement"] = "right";
						popoverJSON["title"] = "Menu Items";
						popoverJSON["content"] = "<div class='popup-mi-wrapper'>"+html+"</div>";
						operations.showpopup(popoverSelector,popoverJSON);
					}
					else{
						$(this).removeAttr("popover-open");
						operations.closepopup(popoverSelector);
						
					}
			    });
			},
			showpopup:function(selector,popoverJson){
				if(popoverJson){
					selector.popover(popoverJson);
				}
				else{
					selector.popover(
					{	title: "<h1>Header</h1>",
						content: "Popover", 
						html: true, 
						placement: "right"}
					); 
				}
				selector.popover("show");
			},
			closepopup:function(selector){
				selector.popover("hide");
			},
			getDeliveryAddress:function(){
				$(".get-delivery-address").unbind();
				$(".get-delivery-address").click(function(){
					
					var pops = $(this).find(".address-popover");
					var self = $(this);
					if($(this).attr("has-addr")){
						
						if(!pops.attr("open"))
						{	
							pops.attr("open","true");
							pops.popover("show");	
						}
						else{
							$(this).find(".address-popover").removeAttr("open");
							$(this).find(".address-popover").popover("hide");
						}
						
					}
					else{	
					var addid = $(this).parent().attr("goodgut-delivery-id");
					goodgut.showSpinner();
					
					// do ajax show delivery address in popup
					
					$.ajax({
						url : "address/getAddressById?addId="+addid,
						type : 'GET',
						success : function(response) {
							
							goodgut.hideSpinner();
							if (response) {
								var al1 = response.addressLine1;
								var al2 =response.addressLine2;
								var al3 =response.addressLine3;
								var al4 =response.city+" "+response.city+"-"+response.pinCode;
								
								var html = "<span>"+al1+"</span>"+"<span>"+al2+"</span>"+"<span>"+al3+"</span>"+"<span>"+al4+"</span>";
								var popoverJSON={};
								popoverJSON["html"] = true;
								popoverJSON["placement"] = "left";
								popoverJSON["title"] = "Address";
								popoverJSON["content"] = "<div class='addres-popup-wrapper'>"+html+"</div>";
								pops.attr("open","true");
								pops.popover(popoverJSON);
								pops.popover("show");
								self.attr("has-addr","true");

							} else {
								
								 goodgut.showError("Technical Problem Please Try Later");
							}
						},
						error : function() {
							
							goodgut.hideSpinner();
						 goodgut.showError("Technical Problem Please Try Later");
						}
					});
					
					
				}
				});
			
			
			},
			printBill:function(){
				$(".printbill").unbind();
				$(".printbill").click(function(){
					var id = $(this).parent().parent().attr("goodgut-data");
					goodgut.showSpinner();
					$.ajax({
						url : "orders/printOrderBill?id="+id,
						type : 'GET',
						success : function(response) {
							
							goodgut.hideSpinner();
							if (response) {
								var _style = "<style media='print,screen'></style>";
								window.open().document.write(response);
							} 
							else {
								
								//show error
							}
						},
						error : function() {
							
							goodgut.hideSpinner();
							// goodgut.showError("Technical Problem Please Try
							// Later");
						}
					});
					
				});
				
			}
			
}	
	
$("#menuitem-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	var button = $("#menuitem-form #menuitem-form-submit");
	button.attr('disabled','disabled');
	var selectors = $("#menuitem-form").find("input,select").not("input[type='button'],input[type='submit']");
	var data = goodgut.collectFormData(selectors);
	console.log(data);
	operations.addMenuItem(data,button);
	
});

$("#meal-form").submit(function(e){
		e.preventDefault();
		e.stopPropagation();
		var button = $("#meal-form #meal-form-submit");
		button.attr('disabled','disabled');
		var selectors = $("#meal-form").find("input,select").not("input[type='button'],input[type='submit']");
		var data = goodgut.collectFormData(selectors);
		console.log(data);
		operations.addMeal(data,button);
		
	});	

$("#subs-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	var button = $("#subs-form #sub-form-submit");
	button.attr('disabled','disabled');
	var selectors = $("#subs-form").find("input,select").not("input[type='button'],input[type='submit']");
	var data = goodgut.collectFormData(selectors);
	var arrayOfMeals=[];
	var temp = "";
	$( ".submeal-selected-box>.selected-meals" ).each(function( index ) {
			
		var data = $(this).attr("goodgut-data");
		
		if(arrayOfMeals.indexOf(data)<0){
			arrayOfMeals.push(data);
			
			if(temp==""){
				temp = data 
			}
			else
				temp += ","+data
		
		}
		
	});
	data["meals"]=temp;
	console.log(data);
	operations.addSubs(data,button);
	
});	

$("#coupon-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	var button = $("#coupon-form-submit");
	button.attr('disabled','disabled');
	var selectors = $("#coupon-form").find("input,select").not("input[type='button'],input[type='submit']");
	var data = goodgut.collectFormData(selectors);
	operations.addCoupon(data,button);
	
});	



function selectDiv(){
$(".submeal-select-box>.select-meals").click(function(){
		$(".submeal-select-box>.select-meals").removeClass("sub-selected");
		$(this).addClass("sub-selected");
		
});
}
selectDiv()
function selectedDiv(){
$(".submeal-selected-box>.selected-meals").click(function(){
	$(".submeal-selected-box>.selected-meals").removeClass("sub-selected");
	$(this).addClass("sub-selected");
	
});
}
selectedDiv();
$(".switches .move-to-right").click(function(){
	var element = $(this).parent().siblings(".submeal-select-box").find(".sub-selected");
	if(element){
		var data = element.attr("goodgut-data");
		var text = element.text();
		var html = "<div class='selected-meals' goodgut-data='"+data+"'>"+text+"</div>";
		$(this).parent().siblings(".submeal-selected-box").append(html)
		element.remove();
		selectedDiv();
	}
});

$(".switches .move-to-left").click(function(){
	
	var element = $(this).parent().siblings(".submeal-selected-box").find(".sub-selected");
	if(element){
		var data = element.attr("goodgut-data");
		var text = element.text();
		var html = "<div class='select-meals' goodgut-data='"+data+"'>"+text+"</div>";
		$(this).parent().siblings(".submeal-select-box").append(html)
		element.remove();
		selectDiv();
	}
});

$("#search-order-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var orderid = $(".search-oid").val();
	var fromdate = $("#osearchfromdate").val();
	var todate = $("#osearchtodate").val();
	
	if(!orderid && !fromdate && !todate){
		
		alert("Please Enter Either Order ID or From & To Date");
		return;
	}
	var data={}
	
	if(orderid){
		
		data["id"] =orderid;
		
	}
	else{
		if(fromdate || todate){
			
			if(!fromdate || !todate){
				alert("Please Enter Both From & To Date");
				return
			}
			else{
				data["fromdate"] = fromdate
				data["todate"]= todate
			}
		}
	}	
	operations.getOrders(data);
	
});



//sub order 


$("#search-suborder-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var orderid = $(".search-sub-oid").val();
	var fromdate = $("#subosearchfromdate").val();
	var todate = $("#subosearchtodate").val();
	
	if(!orderid && !fromdate && !todate){
		
		alert("Please Enter Either Order ID or From & To Date");
		return;
	}
	var data={}
	
	if(orderid){
		
		data["id"] =orderid;
		
	}
	else{
		if(fromdate || todate){
			
			if(!fromdate || !todate){
				alert("Please Enter Both From & To Date");
				return
			}
			else{
				data["fromdate"] = fromdate
				data["todate"]= todate
			}
		}
	}	
	operations.getSubOrders(data);
	
});



//add multiselct for subs and meals


$("select[multiple='multiple']").bsMultiSelect({
    useCss: true,
    allSelectedText: 'All',
    includeSelectAllOption: true
});

$(".dropdown-menu").css({
	"height": "300px",
	"overflow": "auto",
	"font-size": "11px",
	"width": "500px",
})

$("#edit-item-form").submit(function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var itemname = $(".edit-oid").val();
	
	
	if(!itemname){
		
		alert("Please Enter ItemName");
		return;
	}
	var data={}
	
	if(itemname){
		alert("Please Enter ItemName"+itemname);
		data["itemname"] =itemname;
		
	}
		
	operations.editItem(data);
	
});


	
});