$(document).ready(function() {
	var timeoutId = 0;
	goodgut = {

		showSpinner : function() {
			$(".goodgut-modal-spinner").removeClass("goodgut-nodisplay");
		},
		hideSpinner : function() {
			$(".goodgut-modal-spinner").addClass("goodgut-nodisplay");
		},
		collectFormData : function(fields) {
			var data = {};
			for (var i = 0; i < fields.length; i++) {
				var $item = $(fields[i]);
				data[$item.attr('name')] = $item.val();
			}
			return data;
		},

		askConfirmation : function(message) {
			$(".logsol-confirm-modal-body>.text").html(message);
			$(".logsol-confirm-modal").removeClass("logsol-nodisplay");
		},
		showInfo : function(data, time) {
			clearTimeout(timeoutId);
			$(".logsol-alert-body>.type").html("Info!");
			$(".logsol-alert-body>.text").html(data);
			$(".logsol-alert-container").addClass("logsol-alert-info");
			$(".logsol-alert-container").removeClass("logsol-nodisplay");
			if (typeof time === typeof undefined)
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, 2000);
			else
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, time);
		},
		showWarning : function(data, time) {
			clearTimeout(timeoutId);
			$(".logsol-alert-body>.type").html("Warning!");
			$(".logsol-alert-body>.text").html(data);
			$(".logsol-alert-container").addClass("logsol-alert-warning");
			$(".logsol-alert-container").removeClass("logsol-nodisplay");
			if (typeof time === typeof undefined)
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, 2000);
			else
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, time);
		},
		showSuccess : function(data, time) {
			clearTimeout(timeoutId);
			$(".logsol-alert-body>.type").html("Success!");
			$(".logsol-alert-body>.text").html(data);
			$(".logsol-alert-container").addClass("logsol-alert-success");
			$(".logsol-alert-container").removeClass("logsol-nodisplay");
			if (typeof time === typeof undefined)
				setTimeout(function() {
					goodgut.close();
				}, 2000);
			else
				setTimeout(function() {
					goodgut.close();
				}, time);
		},
		showError : function(data, time) {
			clearTimeout(timeoutId);
			$(".logsol-alert-body>.type").html("Error!");
			$(".logsol-alert-body>.text").html(data);
			$(".logsol-alert-container").addClass("logsol-alert-error");
			$(".logsol-alert-container").removeClass("logsol-nodisplay");
			if (typeof time === typeof undefined)
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, 2000);
			else
				timeoutId = setTimeout(function() {
					goodgut.close();
				}, time);
		},
		close : function() {
			clearTimeout(timeoutId);
			$(".logsol-alert-container").addClass("logsol-nodisplay");
			$(".logsol-alert-container").removeClass("logsol-alert-error");
			$(".logsol-alert-container").removeClass("logsol-alert-info");
			$(".logsol-alert-container").removeClass("logsol-alert-success");
			$(".logsol-alert-container").removeClass("logsol-alert-warning");
		}

	};

	// datepicker setup

	$('#datepickertodate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	$('#datepickerfromdate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	$('#coupontodate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	$('#couponfromdate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	$('#subdatepickerfromdate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	$('#subdatepickertodate').datetimepicker({
		format : 'DD/MM/YYYY HH:mm:ss'
	});
	
	
	
	$(".sidebar-elements>.parent").hover(function(over) {
		$(this).children(".sub-menu").removeClass("goodgut-nodisplay");
		$(this).children(".sub-menu").addClass("goodgut-display");
	}, function(out) {
		$(this).children(".sub-menu").addClass("goodgut-nodisplay");
		$(this).children(".sub-menu").removeClass("goodgut-display");
	});

	$(".operations-sub-item-click").click(function(e) {
		var _open = $(this).attr("goodgut-toggle");

		$(".operations-forms-common").addClass("goodgut-nodisplay");
		$("." + _open).removeClass("goodgut-nodisplay");

	});

});