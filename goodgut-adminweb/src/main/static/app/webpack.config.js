const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require("path");
module.exports = merge(common, {
	mode: 'development',
	output: {
		filename: '[contenthash].js',
	},
	module: {
		rules: [{
				test: /\.css$/,
				use: [{
					loader: "style-loader",

				}, {
					loader: "css-loader"
				}, ]
			},
			{
				test: /\.less$/,
				use: [{
					loader: "style-loader",

				}, {
					loader: "css-loader"
				}, {
					loader: "less-loader"
				}]
			}
		]
	},
	devtool: 'inline-source-map',

});