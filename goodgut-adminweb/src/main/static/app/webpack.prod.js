const merge = require('webpack-merge');
const path = require("path");
const common = require('./webpack.common.js');
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
  	mode: 'production',
 	output: {
		filename: '[contenthash].js',
		sourceMapFilename: "../_sourcemaps/[file].map"
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [{
					loader: MiniCssExtractPlugin.loader,
					options: {
						publicPath: './'
					}
				}, {
					loader: "css-loader"
				}, ]
			},
			{
				test: /\.less$/,
				use: [{
					loader: MiniCssExtractPlugin.loader,
					options: {
						publicPath: './'
					}
				}, {
					loader: "css-loader"
				}, {
					loader: "less-loader"
				}]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: "[contenthash].css",
			chunkFilename: "[contenthash].css"
		}),
		new webpack.HashedModuleIdsPlugin()

	],
	devtool: "hidden-source-map"

});

