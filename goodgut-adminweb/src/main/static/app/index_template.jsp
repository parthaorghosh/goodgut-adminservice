<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%! 
	final String cdn = null;
%>


<!DOCTYPE html>	
    <html>

    <head>
		<base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="My Gym Food">
            <title>
                My Gym Food Admin
            </title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
{{#each htmlWebpackPlugin.files.css}}
			<% if(cdn != null && !"none".equals(cdn)) {%>
			<link rel="stylesheet" type="text/css" href="<%= cdn %>/approve{{this}}"></link>
			<% } else { %>
			<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/static{{this}}"></link>
			<% } %>
{{/each }} 
    </head>


    <body style="overflow:hidden">
        <div id="root-element"><div id="loader"></div></div>
        <script>
        var root = location.protocol + '//' + location.host;
            var contextPath = "<%=request.getAttribute("contextPath")%>";
			var userNodeId = "<%=request.getAttribute("userNodeId")%>"
			var entries = [];
			var cdnPath;
			<% if(cdn != null && !"none".equals(cdn)) { %>
			var service = window.location.href.split("/")[2].split(".")[0];
			cdnPath = "<%= cdn %>" +  "/" + service;
			<% } else {%>
				console.log(cdnPath);
				cdnPath = "<%= request.getContextPath() %>/static";
			<% } %>
			
			
			{{#each htmlWebpackPlugin.files.chunks}}
				console.log("{{this.entry}}");
				entries.push("{{this.entry}}");
			{{/each }} 
			entries.forEach(function(entry) {
				var script = document.createElement("script");
				script.src = cdnPath + entry;
				document.body.appendChild(script);
			});
			
			
			
			</script>
       
    </body>

    </html>