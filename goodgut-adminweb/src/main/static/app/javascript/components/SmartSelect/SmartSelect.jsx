import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';
import {asyncCall} from '../../helpers/async-service/service';
import { makeStyles, useTheme } from '@material-ui/core/styles';
// const options = [
//   { value: 'chocolate', label: 'Chocolate' },

// ];
const useStyles = makeStyles(theme => ({
  smartSelect: {
    minWidth:500,
     marginTop: 16,
    marginBottom: 8,
    marginLeft: 8,
    marginRight: 8
  }
}));
/**
 * primarySearchField = this is the field whose value will be the one entered by the user while making a search
 * extraSearchParams = these are the parameters that can be added tobe added as filter parameter whe ngetting data from 
 * database
 */
function SmartSelect({ ...props }) {
  const classes = useStyles();
  const state = { inputValue: '' };
  const {pathArray,onChange,placeholder,extraSearchParams,primarySearchField,defaultValue} = props;

  /**
   * 
   * Results has to be an array of JSON like below 
   * [{value: 'chocolate', label: 'Chocolate' }]
   * @param {*} inputValue 
   */
  const promiseOptions = inputValue =>
    new Promise(resolve => {
      /**
      * No search below 3 length
      */
      if (inputValue.length < 3) {
        return resolve([]);
      }else{
        /**
         * pathArray = {/admin/secured/select/{entityName}}
         */
        let queryParams ={};
        queryParams[primarySearchField] =inputValue;
         for(let key in extraSearchParams ){
          queryParams[key]=extraSearchParams[key]
        }
        asyncCall.post(pathArray,queryParams).then(result => {
          return resolve(result.results);
      })
      }
        // return resolve([{value: 'chocolate', label: 'Chocolate' },{value: 'vanilla', label: 'Vanilla' }]);

      
    });


  return (
    <AsyncSelect
      className={classes.smartSelect}
      isMulti
      cacheOptions
      defaultOptions
      defaultValue={defaultValue}
      loadOptions={promiseOptions}
      onChange={onChange}
      placeholder={placeholder}
    />
  );
}
export default (SmartSelect);
