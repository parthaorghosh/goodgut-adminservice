import React from "react";
import PropTypes from "prop-types";
import Icon from "@material-ui/core/Icon";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import MaterialTable from 'material-table';
// core components
import tableStyle from "assets/jss/techxi-styles/components/tableStyle.jsx";

function TechxiTable({ ...props }) {
  const { classes,title,columns, data, rowAddFunction,rowUpdateFunction,rowDeleteFunction,options,actions,tableRef} = props;
  const [state, setState] = React.useState({columns,data});
  return (
    <MaterialTable
      title={title}
      tableRef={tableRef}
      columns={state.columns}
      data={state.data}
      actions={actions}
      options={{...options,'search':false}}
      editable={{
        onRowAdd: rowAddFunction,
        onRowUpdate: rowUpdateFunction,
        onRowDelete: rowDeleteFunction,
      }}
    />
  );
}

TechxiTable.defaultProps = {
  tableHeaderColor: "gray"
};

TechxiTable.propTypes = {
};

export default withStyles(tableStyle)(TechxiTable);
