import React from 'react';
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Hidden from "@material-ui/core/Hidden";
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import { NavLink } from "react-router-dom";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import classNames from "classnames";
import sidebarStyle from "../../assets/jss/Techxi-Styles/components/sidebarStyle.jsx";
const TechxiSidebar = ({ ...props }) => {

  const [open, setOpen] = React.useState(true);

  function handleClick() {
    setOpen(!open);
  }

  // verifies if routeName is the one active (in browser input)
  function activeRoute(routeName) {
    return window.location.href.indexOf(routeName) > -1 ? true : false;
  }
  const { classes, color, logo, image, logoText, routes } = props;



  var links = (
    <List className={classes.list}>
      {routes.map((prop, key) => {

        var activePro = " ";

        var listItemClasses = classNames({
          [" " + classes[color]]: activeRoute(prop.pathPrefix + prop.path)
        });

        const whiteFontClasses = classNames({
          [" " + classes.whiteFont]: activeRoute(prop.pathPrefix + prop.path)
        });
        if (prop.sublist) {
         
          return (
            <div className={activePro + classes.item}>
              <ListItem button onClick={handleClick} key={key + "-itemhead"} className={classes.itemLink}>
                  
                  {prop.icon && typeof prop.icon === "string" ? (
                    <Icon
                      className={classNames(classes.itemIcon, whiteFontClasses)}
                    >
                      {prop.icon}
                    </Icon>
                  ) : (
                      <prop.icon
                        className={classNames(classes.itemIcon, whiteFontClasses)}
                      />
                    )}
                    
                <ListItemText primary={prop.name}
                  className={classNames(whiteFontClasses)}
                  disableTypography={true}>
                </ListItemText>
                {open ? <ExpandLess className={classNames(classes.itemIcon, whiteFontClasses)}/> : <ExpandMore className={classNames(classes.itemIcon, whiteFontClasses)}/>}
              </ListItem>
              <Collapse in={open} timeout="auto" unmountOnExit key={key + "-collapse"}>
                <List className={classes.list} key={key + "-list"}>
                  {prop.childlist.map((subListProp, subListKey) => {
                     var listItemSubClasses = classNames({
                          [" " + classes[color]]: activeRoute(prop.pathPrefix + prop.path + subListProp.path)
                    });
                    return (
                      
                      <NavLink
                        to={prop.pathPrefix + prop.path + subListProp.path}
                        exact
                        className={activePro + classes.item}
                        activeClassName="active"
                        key={subListProp.key}
                      >
                        <ListItem button className={classes.itemSubLink + listItemSubClasses} key={subListProp.key + "-li"}>
                            {subListProp.icon && typeof subListProp.icon === "string" ? (
                              <Icon
                                className={classNames(classes.itemIcon, whiteFontClasses)}
                              >
                                {subListProp.icon}
                              </Icon>
                            ) : (
                                <div/>
                              )}
                              {subListProp.icon && typeof subListProp.icon != "string" ? (
                              <subListProp.icon
                                  className={classNames(classes.itemIcon, whiteFontClasses)}
                                />
                            ) : (
                               <div/>
                              )}
                          <ListItemText primary={subListProp.name}
                            className={classNames(classes.itemText, whiteFontClasses)}
                            disableTypography={true}
                          />
                        </ListItem>
                      </NavLink>
                     
                    )

                  })}
                </List>
              </Collapse>
            </div>
          )

        } else {
          return (
            
            <NavLink
              to={prop.pathPrefix + prop.path}
              className={activePro + classes.item}
              activeClassName="active"
              key={key}
            >
              <ListItem button className={classes.itemLink + listItemClasses} key={key+"-li"}>
                {typeof prop.icon === "string" ? (
                  <Icon
                    className={classNames(classes.itemIcon, whiteFontClasses)}
                  >
                    {prop.icon}
                  </Icon>
                ) : (
                    <prop.icon
                      className={classNames(classes.itemIcon, whiteFontClasses)}
                    />
                  )}
                <ListItemText
                  primary={prop.name}
                  className={classNames(classes.itemText, whiteFontClasses)}
                  disableTypography={true}
                />
              </ListItem>
            </NavLink>
          )
        }

      })}
    </List>
  );
  var brand = (
    <div className={classes.logo}>
      <a
        href=""
        className={classNames(classes.logoLink, {
          [classes.logoLinkRTL]: props.rtlActive
        })}
        target="_blank"
      >
        <div className={classes.logoImage}>
          <img src={logo} alt="logo" className={classes.img} />
        </div>
        {logoText}
      </a>
    </div>
  );
  return (
    <div>
      <Hidden mdUp implementation="css">
        <Drawer
          variant="temporary"
          anchor={"right"}
          open={props.open}
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive
            })
          }}
          onClose={props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>
            {links}
          </div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          anchor={props.rtlActive ? "right" : "left"}
          variant="permanent"
          open
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive
            })
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  );
};

TechxiSidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  bgColor: PropTypes.oneOf(["purple", "blue", "green", "orange", "red"]),
  logo: PropTypes.string,
  image: PropTypes.string,
  logoText: PropTypes.string,
  routes: PropTypes.arrayOf(PropTypes.object),
  open: PropTypes.bool
};

export default withStyles(sidebarStyle)(TechxiSidebar);
