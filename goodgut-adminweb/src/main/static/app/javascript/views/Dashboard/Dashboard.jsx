import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
import {useEffect } from 'react';
// react plugin for creating charts

// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// core components
import dashboardStyle from "../../assets/jss/techxi-styles/views/dashboardStyle.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";
import "assets/css/material-dashboard-react.css";
import EntityOrder from "../Entities/EntityOrder.jsx";
import EntitySubscriptionOrder from "../Entities/EntitySubscriptionOrder.jsx";

function Dashboard(props) {

  const marginTop={
    marginTop:"20"
  }
  const { classes } = props;
  return (
    <div>
      
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <EntityOrder></EntityOrder>
        </GridItem>
        
        <GridItem xs={12} sm={12} md={12} >
          <EntitySubscriptionOrder></EntitySubscriptionOrder>
        </GridItem>
        
      </GridContainer>

    </div>
  );
}


Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
