import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
// import Navbar from "../../components/Navbars/Navbar.jsx";
// import Footer from "../../components/Footer/Footer.jsx";
import TechxiSidebar from "../../components/Sidebar/TechxiSideBar.jsx";
// import FixedPlugin from "../../components/FixedPlugin/FixedPlugin.jsx";

import routes from "../../navigations.js";

import dashboardStyle from "../../assets/jss/techxi-styles/main.jsx";

import image from "../../assets/img/sidebar-2.jpg";
import logo from "../../assets/img/reactlogo.png";
import PrivateRoute from "../../helpers/PrivateRoute.js";


let ps;

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.pathPrefix === "/admin") {
         if (prop.sublist) {
           return (
          prop.childlist.map((childprop, childkey) => {
              return (
                <PrivateRoute
                  path={prop.pathPrefix+ prop.path+ childprop.path}
                  component={childprop.component}
                  key={childprop.key}
                />
              );
            })
          )
        }
        else {
          return (
            <PrivateRoute
              path={prop.pathPrefix + prop.path}
              component={prop.component}
              key={key}
            />
          );
        }}
      return null;
    })}
    <Redirect exact from="/admin/secured/main" to="/admin/secured/main/dashboard" />
  </Switch>
);

class Main extends React.Component {
  state = {
    image: image,
    color: "blue",
    hasImage: true,
    fixedClasses: "dropdown show",
    mobileOpen: false
  };
  mainPanel = React.createRef();
  handleImageClick = image => {
    this.setState({ image: image });
  };
  handleColorClick = color => {
    this.setState({ color: color });
  };
  handleFixedClick = () => {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  };
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  getRoute() {
    return window.location.pathname !== "/admin/maps";
  }
  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  };
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.mainPanel.current);
    }
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.mainPanel.current.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
    window.removeEventListener("resize", this.resizeFunction);
  }
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.wrapper}>
        <TechxiSidebar
          routes={routes}
          logoText={"My Gym Food"}
          logo={"admin/static"+logo}
          image={"admin/static"+this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          {...rest}
        />
        <div className={classes.mainPanel} ref={this.mainPanel}>
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          ) : (
              <div className={classes.map}>{switchRoutes}</div>
            )}
          

        </div>
      </div>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Main);
