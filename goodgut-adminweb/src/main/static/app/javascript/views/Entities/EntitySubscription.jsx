import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import FormModalOpener from "../Forms/FormModalOpener.jsx"
import TechxiTable from "../../components/Table/TechxiTable.jsx";
import { asyncCall } from '../../helpers/async-service/service';
import EditFormModalOpener from '../Forms/EditFormModalOpener.jsx';


const styles = {
};
const column = [
  {
    field: 'subscriptionId',
    title: 'Edit',
    filtering:false,
    render: rowData => <EditFormModalOpener buttonLabel="Edit" formName="SubscriptionForm" entityId={rowData.subscriptionId} getEntity={getEntity}/>
  }, 
{ title: 'Sub Id', field: 'subscriptionId',filtering:false,type: 'numeric',hidden:true },
{ title: 'Sub Name', field:'name',filtering:false,editable: 'never'},
{ title: 'Description', field: 'subDescription',filtering:false,editable: 'never' },
{ title: 'Sub Period', field: 'subscriptionPeriodType',filtering:false,editable: 'never'},
{ title: 'Sub Category', field: 'mealSubscriptionCatagory',filtering:false,editable: 'never'},
{ title: 'Calories', field: 'calories', type: 'numeric',editable: 'never'},
{ title: 'Protein', field: 'protein', type: 'numeric',editable: 'never'},
{ title: 'Price', field: 'price', type: 'numeric',editable: 'never'},
{ title: 'Offer Price', field: 'offerPrice', type: 'numeric',editable: 'never'},
{ title: 'Offer Avail', field: 'offerAvail', type: 'boolean',editable: 'never'},
{ title: 'Active(1)/Deactive(0)', field: 'status', type: 'numeric',editable: 'never'}
];
/** 
 * Check if any filters passed then only it calls backend to get data 
 * and also if no Filter is applied
 * else display nothing
 * 
*/

const checkFilters=(filters)=>{
  let anyFilterPassed = false;
  let noFilters = true;
  if(filters.length==0)
    return noFilters;
  if(filters){
      filters.forEach((item ,index)=>{
        if(item.column && item.column.customFilterAndSearch){
            if(item.column.customFilterAndSearch(item.value)){
              anyFilterPassed = true;
            }
        }
      });
  }
  return anyFilterPassed;
}
/**
 * Get Meals
 * And all the filters follow some custom rules 
 * which when computed as true then only backend is called 
 * @param {*} query 
 */
const data = (query) => 
  new Promise(resolve => {
    let filters = query.filters;
    
    if(!checkFilters(filters)){
        resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });
    }else{
      asyncCall.post(["secured","search","subscription"],query).then(result => {
        
        if(result.GOODGUT_RESP_CODE=="G200"){
        resolve({
          data: result.data,
          page: result.page,
          totalCount: result.totalCount,
      })
      }else{
         resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });

      }});
    }})
const pushInArray = (json,key,element)=>{
  if(!json[key])
    json[key]=[];
  json[key].push(element)  
}
const pushInDefaultVal = (json,key,value,label)=>{
  if(!json[key])
    json[key]=[];
  json[key].push({"value":value, "label": label })  
}
const getEntity = (id)=>{
  return new Promise(resolve => {
    asyncCall.get(["secured","subs","getSubsById"],{"subid":id}).then(result => {
          let return_json = {};
          return_json["mondayArray"]=[]
          if(result){
              return_json["subscriptionId"]=result["subscriptionId"];
              return_json["imageUrl"]=result["imageUrl"];
              return_json["subDescription"]=result["subDescription"];
              return_json["name"]=result["name"];
              return_json["offerAvailable"]=result["offerAvail"];
              return_json["price"]=result["price"];
              return_json["offerPrice"]=result["offerPrice"];
              return_json["subperiod"]=result["subscriptionPeriodType"];
              return_json["calories"]=result["calories"];
              return_json["protein"]=result["protein"];
              return_json["mealSubscriptionCatagory"]=result["mealSubscriptionCatagory"];
              result["meals"].forEach((meal)=>{
                    if(meal["availableDays"].includes("1")){
                        pushInArray(return_json,"mondayArray",meal["mealId"]);
                        pushInDefaultVal(return_json,"mondayArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                      }  
                    else if(meal["availableDays"].includes("2")){
                      pushInArray(return_json,"tuesdayArray",meal["mealId"]);
                      pushInDefaultVal(return_json,"tuesdayArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                    }
                   else if(meal["availableDays"].includes("3")){
                      pushInArray(return_json,"wedArray",meal["mealId"]);
                      pushInDefaultVal(return_json,"wedArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                    }
                    else if(meal["availableDays"].includes("4")){
                      pushInArray(return_json,"thursdayArray",meal["mealId"]);
                      pushInDefaultVal(return_json,"thursdayArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                    }
                   else if(meal["availableDays"].includes("5")){
                       pushInArray(return_json,"fridayArray",meal["mealId"]);
                       pushInDefaultVal(return_json,"fridayArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                    }
                    else if(meal["availableDays"].includes("6")){
                      pushInArray(return_json,"saturdayArray",meal["mealId"]);
                      pushInDefaultVal(return_json,"saturdayArrayInitVal",meal["mealId"],meal["mealName"]+" "+meal["mealType"]);
                    }
                   
              })


          }

          resolve(return_json);
    })

  })

}

// const onRowDelete = (oldData) => {
//   new Promise(resolve => {
//     setTimeout(() => {
//       resolve();
//       const data = [...state.data];
//       data.splice(data.indexOf(oldData), 1);
//       setState({ ...state, data });
//     }, 600);
//   })
// }


function EntitySubscription(props) {
  const tableRef = React.createRef(); 
  return (
    <div>
    <FormModalOpener buttonLabel="Add Subscription" formName="SubscriptionForm"/>
    <TechxiTable
      title="Subscriptions"
      columns={column}
      data={data}
      tableRef={tableRef}
      actions={[
        {
          icon: 'refresh',
          tooltip: 'Refresh Data',
          isFreeAction: true,
          onClick: () => tableRef.current && tableRef.current.onQueryChange(),
        }
      ]}
      options={{
      pageSizeOptions: [5,10,30,50],
      maxBodyHeight:400,
      addRowPosition:'first',
      filtering: true
    }}
    >
    </TechxiTable>

</div>
  );
}
EntitySubscription.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(EntitySubscription);
