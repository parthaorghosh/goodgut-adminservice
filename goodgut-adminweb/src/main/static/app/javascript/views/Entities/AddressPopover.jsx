import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import { asyncCall } from '../../helpers/async-service/service';

const useStyles = makeStyles(theme => ({
  typography: {
    padding: theme.spacing(2),
  },
  addressPopOverUl:{
    padding:"20px",
    listStyle:"none"
  },
  listSeperator:{
    marginTop:"10px",
    marginBottom:"10px",
    fontWeight:"bold"
  },
  namePointer:{
    cursor:"pointer"
  }
}));

export default function AddressPopover({ ...props }) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const {username,addresses} = props;
  const [addressArray, setAddressArray] = React.useState([]);
  
  const handleClick = event => {
    let target = event.currentTarget;
    asyncCall.get(["secured", "address", "getSubscriptionAddress"],{"address":addresses}).then(result => {
        if (result) {
          setAddressArray(result);
          setAnchorEl(target);
            
        } 
      }).catch(err => {
        alert("Technical Error Please try later");
       
      });
      
    
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <a  className={classes.namePointer} aria-describedby={id} variant="contained" color="primary" onClick={handleClick}>
        {username}
      </a>
      
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
    <ul className={classes.addressPopOverUl}>
        {addressArray.map((item, key) => {
            return(<li>
              {(key==0 && item && item.name )?<div className={classes.listSeperator}>Breakfast</div>:""}
              {(key==1 && item && item.name )?<div className={classes.listSeperator}>Lunch</div>:""}
              {(key==2 && item && item.name )?<div className={classes.listSeperator}>Dinner</div>:""}
                   {(item && item.name  && item.contactNo)?<div> {item.name+"  "+item.contactNo}</div>:""} 
            
                 {(item && item.line1 )?
                    <div>{item.line1}</div>:""
                }
                 {(item && item.line2 )?
                    <div>{item.line2}</div>:""
                }
                {(item && item.line3 ) ?
                    <div>{item.line3}</div>:""
                }
                {(item && item.line4 )?
                    <div>{item.line4}</div>:""
                }
            </li>)

    })}
    </ul>
      </Popover>
    </div>
  );
}