import React,  { useRef, createRef, useState } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import TechxiTable from "../../components/Table/TechxiTable.jsx";
import { asyncCall } from '../../helpers/async-service/service';
import Print from '@material-ui/icons/Print';
import moment from 'moment';
import TechxiDatePicker from "../../helpers/util/TechxiDatePicker.jsx";
import TechxiPopover from '../../helpers/util//TechxiPopover.jsx';



function EntityOrder(props) {
  const useStyles = makeStyles(theme => ({
    font10: {
      fontSize : "10px",
      padding:"0px"
    },
    bold:{
      fontWeight:"600"
    },
    namePointer:{
      cursor:"pointer"
    },
    noBullet:{
      listStyle:"none",
      padding:"0px"
    }
    }));
    const classes = useStyles();
  const getCurrentDate=()=>{

    let todayTime = new Date();
    let month = todayTime .getMonth() + 1;
    let day = todayTime .getDate();
    let year =todayTime .getFullYear();
   return month + "/" + day + "/" + year;
}
const convertDate = (date) => {
  return new Date(date).toLocaleString();
}
const tableRef = React.useRef();

  const resolveAddress = (addrid, useraddresses) => {
   let temp={};
    if(useraddresses){
      useraddresses.forEach((item, index) => {
      if (item.id == addrid) {
        temp = item;
        return false;
      }
    });
  }
    return ( 
    <TechxiPopover
     displayName={temp.name}
     popoverHtml={ <ul className={classes.noBullet}>
     <li className={classes.bold}> {temp.name}</li>
     <li>{temp.contactNo}</li>
     <li>{temp.addressLine1}</li>
     <li>{temp.addressLine2}</li>
     <li>{temp.addressLine3}</li>
     <li>{temp.pinCode + " " + temp.city + " - " + temp.state}</li>
 </ul>}></TechxiPopover> )


}
  const doPrint = (orderId) => {
  
    let data = {};
    data["id"] = orderId;
    asyncCall.get(["secured", "orders", "printOrderBill", orderId]).then(result => {
      if (result) {
        window.open().document.write(result);
      }
      else
        alert("Technical Error , please try later");
  
    }).catch(err => {
      alert("Technical Error , please try later");
    });
  }
  const column = [
    {
      field: 'orderId',
      title: 'Print Bill',
      filtering: false,
      editable: 'never',
      render: rowData => <Print onClick={() => { doPrint(rowData.orderId) }} />
    },
    {
      title: 'Status',
      field: 'orderStatus',
      filtering: false,
      lookup: {
        'ORDERPLACED': 'ORDERPLACED', 'ORDERACTIVE': 'ORDERACTIVE', 'INKITCHEN': 'INKITCHEN', 'PREPARING': 'PREPARING', 'PICKEDUP': 'PICKEDUP', 'TRANSIT': 'TRANSIT'
        , 'DELIVERED': 'DELIVERED', 'PAYMENT_PENDING': 'PAYMENT_PENDING', 'PAYMENT_FAILURE': 'PAYMENT_FAILURE'
      },
    },
    { title: 'Id', field: 'orderId', filtering: false, type: 'numeric', hidden: true },
    { title: 'Order Catagory', field: 'orderCatagory', filtering: true, 
    lookup: { 'SINGLEORDER': 'SINGLEORDER', 'SUBSCRIPTIONORDER': 'SUBSCRIPTIONORDER', 'GUESTSINGLEORDER': 'GUESTSINGLEORDER'},
    editable: 'never' },
    {
      field: 'orderDate',
      title: 'Order Date',
      filtering: true,
      editable: 'never',
   
      render: rowData => convertDate(rowData.orderDate)
    },
    {
      field: 'deliveryDate',
      title: 'Delivery Date',
      filtering: false,
      defaultSort:'desc',
      render: rowData => convertDate(rowData.deliveryDate)
    },
    {
      field: 'mealType',
      title: 'Meal name',
      filtering: false,
      editable: 'never',
      render: rowData => rowData.meals[0].mealName
    },
    {
      field: 'deliveryAddressId',
      title: 'Delivery Address',
      filtering: false,
      editable: 'never',
      render: rowData => <div>{resolveAddress(rowData.deliveryAddressId, (rowData.user)?rowData.user.userAddresses:"")}</div>
    },
    {
      field: 'mealType',
      title: 'Meal Type',
      filtering: true,
      editable: 'never',
      lookup: { 'LUNCH': 'LUNCH', 'DINNER': 'DINNER', 'BREAKFAST': 'BREAKFAST', 'POSTBREAKFAST': 'POSTBREAKFAST', 'SNACKS': 'SNACKS', 'POSTDINNER': 'POSTDINNER' },
      render: rowData => rowData.meals[0].mealType
    },
    { title: 'payType', field: 'payType', filtering: false },
  
  ];
  /** 
   * Check if any filters passed then only it calls backend to get data 
   * and also if no Filter is applied
   * else display nothing
   * 
  */
  
  
  
  
  const checkFilters = (filters) => {
    let anyFilterPassed = true;
    let noFilters = true;
    if (filters.length == 0)
      return noFilters;
    if (filters) {
      filters.forEach((item, index) => {
        if (item.column && item.column.customFilterAndSearch) {
          if (item.column.customFilterAndSearch(item.value)) {
            anyFilterPassed = true;
          }
        }
      });
    }
    return anyFilterPassed;
  }
  
  

  /**
 * Get Orders
 * And all the filters follow some custom rules 
 * which when computed as true then only backend is called 
 * @param {*} query 
 */
const data = (query) =>
new Promise(resolve => {
  let filters = query.filters;

  if(!tableRef.current.startDate)
    tableRef.current.startDate = getCurrentDate();
   
  if(!tableRef.current.endDate)
    tableRef.current.endDate = getCurrentDate(); 

  addFilter("startDate","deliveryDate","Delivery Date",tableRef.current.startDate,">",filters);
  addFilter("endDate","deliveryDate","Delivery Date",tableRef.current.endDate,"<",filters);
  if (!checkFilters(filters)) {
    resolve({
      data: [],
      page: 0,
      totalCount: 0,
    });
  } else {
    asyncCall.post(["secured", "search", "order"], query).then(result => {
      if (result.GOODGUT_RESP_CODE == "G200") {
        resolve({
          data: result.data,
          page: result.page,
          totalCount: result.totalCount,
        })
      } else {
        resolve({
          data: [],
          page: 0,
          totalCount: 0,
        });

      }
    }).catch(err => {
      alert("Technical Error Please try later");
      resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });

    });
  }
})


 const getFilterObject=(id,field,title,value,operator)=>{
  
  
  let filterObject = {
    "column": {
      "field":"deliveryDate",
      "title":"Delivery Date",
      "tableData": {
        "columnOrder": 0,
        "filterValue": "",
        "groupSort": "asc",
        "id": 0
      }
    },
    "operator": "",
    "value": ""
  }

  let newObject = JSON.parse(JSON.stringify(filterObject));
  newObject["column"]["field"]=field;
  newObject["column"]["title"]=title;
  newObject["column"]["tableData"]["filterValue"]=value;
  newObject["column"]["tableData"]["id"]=id;
  newObject["value"]=value;
  newObject["operator"]=operator;
  
  return newObject;
 }
 const handleEvent=(event, picker)=> {


  if(event.type=='apply'){
    if(tableRef.current){
      
      let startDate = picker.startDate.format('MM/DD/YYYY');
      let endDate= picker.endDate.format('MM/DD/YYYY');
      tableRef.current.startDate=startDate;
      tableRef.current.endDate=endDate;
      let query = tableRef.current.state.query;
      let filters = query["filters"];
      addFilter("startDate","deliveryDate","Delivery Date",startDate,">",filters);
      addFilter("endDate","deliveryDate","Delivery Date",endDate,"<",filters);
      
      tableRef.current.onQueryChange()
    }
       
  }
 }

 const addFilter=(id,field,title,value,operator,filters)=>{
      if(!filters)
          return;
  
  let  found = false;
  filters.forEach((item ,index)=>{

    if(item["column"] && item["column"]["tableData"]["id"]==id && item["column"]["field"]==field && item["column"]["title"]==title){
      found = true;
      item["column"]["field"]=field;
      item["column"]["title"]=title;
      item["column"]["tableData"]["filterValue"]=value;
      item["value"]=value;
      item["operator"]=operator;
    }
    
  })
  if(!found)
  filters.push(getFilterObject(id,field,title,value,operator));
 }


  const onRowUpdate = (newData, oldData) => {

    return new Promise(resolve => {
      const updatedJson = {};
      updatedJson["primaryKey"] = oldData["orderId"];
      for (let key in newData) {
        if (newData[key] != oldData[key]) {
          updatedJson[key] = newData[key];
        }
      }
      asyncCall.post(["secured", "update", "order"], updatedJson).then(result => {
        resolve();
      }).catch(err => {
        resolve();
      });
    });
  }
const ranges ={

  'Today': [moment(), moment()],
  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  'This Month': [moment().startOf('month'), moment().endOf('month')],
  'Last Month': [moment().subtract(1, 'month').startOf('month')]
}
  return (
    <div>
      <TechxiDatePicker ranges={ranges} onEvent={handleEvent}></TechxiDatePicker>
      <TechxiTable
        title="Orders"
        columns={column}
        data={data}
        tableRef={tableRef}
        rowUpdateFunction={onRowUpdate}
        options={{
          pageSizeOptions: [20, 30, 50],
          pageSize: 20,
          maxBodyHeight: 400,
          addRowPosition: 'first',
          filtering: true
        }}
        actions={[
          {
            icon: 'refresh',
            tooltip: 'Refresh Data',
            isFreeAction: true,
            onClick: () => tableRef.current && tableRef.current.onQueryChange(),
          }
        ]}
      >
      </TechxiTable>

    </div>
  );
}
EntityOrder.propTypes = {
  classes: PropTypes.object
};

export default (EntityOrder);
