import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import TechxiTable from "../../components/Table/TechxiTable.jsx";
import { asyncCall } from '../../helpers/async-service/service';
const styles = {
};
const column = [{ title: 'Name', field: 'itemName', customFilterAndSearch: (input) => input.length > 3 },
{ title: 'Description', field: 'itemDescription', filtering: false },
{ title: 'Nutritional Info', field: 'itemMetadata', filtering: false },
{
  title: 'Item Type',
  field: 'itemType',
  lookup: { 'VEG': 'VEG', 'NONVEG': 'NONVEG' },
  editable:'never'
},
{
  title: 'Type',
  field: 'mealType',
  editable:'never',
  lookup: { 'LUNCH': 'LUNCH', 'DINNER': 'DINNER', 'BREAKFAST': 'BREAKFAST', 'POSTBREAKFAST': 'POSTBREAKFAST', 'SNACKS': 'SNACKS', 'POSTDINNER': 'POSTDINNER' },
},
{ title: 'Price', field: 'price', type: 'numeric' ,editable:'never'},
{ title: 'Offer Price', field: 'offerPrice', type: 'numeric',editable:'never' },
{ title: 'Offer Avail', field: 'offerAvail', type: 'boolean',editable:'never' },

{ title: 'MenuItem Id', field: 'itemId', filtering: false, type: 'numeric', hidden: true ,editable:'never'},
];
/** 
 * Check if any filters passed then only it calls backend to get data 
 * and also if no Filter is applied
 * else display nothing
 * 
*/

const checkFilters = (filters) => {
  let anyFilterPassed = false;
  let noFilters = true;
  if (filters.length == 0)
    return noFilters;
  if (filters) {
    filters.forEach((item, index) => {
      if (item.column && item.column.customFilterAndSearch) {
        if (item.column.customFilterAndSearch(item.value)) {
          anyFilterPassed = true;
        }
      }
    });
  }
  return anyFilterPassed;
}
/**
 * Get Menu Items
 * And all the filters follow some custom rules 
 * which when computed as true then only backend is called 
 * @param {*} query 
 */

const data = (query) =>
  new Promise(resolve => {
    let filters = query.filters;
    if (!checkFilters(filters)) {
      resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });
    } else {
      asyncCall.post(["secured", "search", "menuItem"], query).then(result => {
        if (result.GOODGUT_RESP_CODE == "G200") {
          resolve({
            data: result.data,
            page: result.page,
            totalCount: result.totalCount,
          })
        } else {
          resolve({
            data: [],
            page: 0,
            totalCount: 0,
          });

        }
      });
    }
  })


const addRow = (newData) => {
  new Promise(resolve => {
    setTimeout(() => {
      resolve();
      const data = [...state.data];
      data.push(newData);
      setState({ ...state, data });
    }, 600);
  })
}


function EntityMenuItem(props) {
  const { classes } = props;
  const tableRef = React.createRef();
  const onRowUpdate = (newData, oldData) => {

    return new Promise(resolve => {
      const updatedJson = {};
      updatedJson["primaryKey"] = oldData["itemId"];
      for (let key in newData) {
        if (newData[key] != oldData[key]) {
          updatedJson[key] = newData[key];
        }
      }
      asyncCall.post(["secured", "update", "menuItem"], updatedJson).then(result => {
        resolve();
      });
    });
  }
  return (
    <div>

      <TechxiTable
        title="Menu Items"
        columns={column}
        tableRef={tableRef}
        data={data}
        rowAddFunction={addRow}
        rowUpdateFunction={onRowUpdate}
        // rowDeleteFunction={onRowDelete}
        options={{
          pageSizeOptions: [5, 10, 30, 50],
          maxBodyHeight: 400,
          addRowPosition: 'first',
          filtering: true
        }}
      >
      </TechxiTable>

    </div>
  );
}
EntityMenuItem.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(EntityMenuItem);
