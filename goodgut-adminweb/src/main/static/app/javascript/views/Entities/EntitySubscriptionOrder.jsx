import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import TechxiTable from "../../components/Table/TechxiTable.jsx";
import { asyncCall } from '../../helpers/async-service/service';
import AddressPopover from './AddressPopover.jsx';
const styles = {
  
};
const convertDate = (date) => {
  return new Date(date).toLocaleString();
}
const column = [
  
  {
    title: 'Status',
    field: 'subscriptionOrderStatus',
    filtering: false,
    editable: 'never',
    lookup: {
      'ACTIVE': 'ACTIVE', 'DONE': 'DONE',  'PAYMENT_PENDING': 'PAYMENT_PENDING', 'PAYMENT_FAILURE': 'PAYMENT_FAILURE'
    },
  },
  { title: 'Id', field: 'orderId', filtering: false, type: 'numeric', hidden: true },
  { title: 'Sub Name', field: 'subscription.name', filtering: false,editable:'never'},
  {
    field: 'subscription.mealSubscriptionCatagory',
    title: 'Type',
    filtering: false,
    editable: 'never',
    lookup: {
      'VEG': 'VEG', 'NONVEG': 'NON-VEG'
    }
  },
  {
    field: 'subscriptionOrderDate',
    title: 'Order Date',
    filtering: false,
    editable: 'never',
    render: rowData => convertDate(rowData.subscriptionOrderDate)
  },
  {
    field: 'subscriptionStartDate',
    title: 'Start Date',
    filtering: false,
    editable: 'never',
    defaultSort:'desc',
    render: rowData => convertDate(rowData.subscriptionStartDate)
  },
  {
    field: 'subscriptionEndDate',
    title: 'End Date',
    filtering: false,
    editable: 'never',
    render: rowData => convertDate(rowData.subscriptionEndDate)
  },
  {
    field: 'name',
    title: 'Order From',
    filtering: false,
    editable: 'never',
    render: rowData => <AddressPopover addresses={rowData.subscriptionAddr} username ={rowData.user.firstName + " " + rowData.user.lastName}/>
  },
  { title: 'Period', field: 'subscription.subscriptionPeriodType', filtering: false, editable: 'never' },
  { title: 'Calories', field: 'subscription.calories', filtering: false, editable: 'never' },
  { title: 'payType', field: 'payType', filtering: false },

];
/** 
 * Check if any filters passed then only it calls backend to get data 
 * and also if no Filter is applied
 * else display nothing
 * 
*/

const checkFilters = (filters) => {
  let anyFilterPassed = false;
  let noFilters = true;
  if (filters.length == 0)
    return noFilters;
  if (filters) {
    filters.forEach((item, index) => {
      if (item.column && item.column.customFilterAndSearch) {
        if (item.column.customFilterAndSearch(item.value)) {
          anyFilterPassed = true;
        }
      }
    });
  }
  return anyFilterPassed;
}
/**
 * Get Orders
 * And all the filters follow some custom rules 
 * which when computed as true then only backend is called 
 * @param {*} query 
 */
const data = (query) =>
  new Promise(resolve => {
    let filters = query.filters;
    if (!checkFilters(filters)) {
      resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });
    } else {
      asyncCall.post(["secured", "search", "subscriptionOrder"], query).then(result => {
        if (result.GOODGUT_RESP_CODE == "G200") {
          resolve({
            data: result.data,
            page: result.page,
            totalCount: result.totalCount,
          })
        } else {
          resolve({
            data: [],
            page: 0,
            totalCount: 0,
          });

        }
      }).catch(err => {
        alert("Technical Error Please try later");
        resolve({
          data: [],
          page: 0,
          totalCount: 0,
        });

      });
    }
  })





function EntitySubscriptionOrder(props) {
  const { classes } = props;
  const tableRef = React.createRef();
  return (
    <div>
      <TechxiTable
        title="Subscription Order"
        columns={column}
        data={data}
        tableRef={tableRef}
        options={{
          pageSizeOptions: [20, 30, 50],
          pageSize: 20,
          maxBodyHeight: 400,
          addRowPosition: 'first',
          filtering: true
        }}
        actions={[
          {
            icon: 'refresh',
            tooltip: 'Refresh Data',
            isFreeAction: true,
            onClick: () => tableRef.current && tableRef.current.onQueryChange(),
          }
        ]}
      >
      </TechxiTable>

    </div>
  );
}
EntitySubscriptionOrder.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(EntitySubscriptionOrder);
