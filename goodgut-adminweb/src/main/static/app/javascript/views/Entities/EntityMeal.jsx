import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import FormModalOpener from "../Forms/FormModalOpener.jsx"
import TechxiTable from "../../components/Table/TechxiTable.jsx";
import { asyncCall } from '../../helpers/async-service/service';

const styles = {
};
const column = [{ title: 'Name', field: 'mealName',customFilterAndSearch: (input) => input.length>3,editable: 'never'},

{ title: 'Meal Id', field: 'mealId',filtering:false,type: 'numeric',hidden:true },
{ title: 'Description', field: 'mealDescription',filtering:false },
{ title: 'Meal Type', field: 'mealType',filtering:false,editable: 'never'},
{ title: 'Meal Category', field: 'mealCategory',filtering:false,editable: 'never'},
{ title: 'Available Days', field: 'availableDays',filtering:false,editable: 'never'},
{ title: 'Price', field: 'mealPrice', type: 'numeric',editable: 'never'},
{ title: 'Offer Price', field: 'offerPrice', type: 'numeric'},
{ title: 'Offer Avail', field: 'offerAvail', type: 'boolean'},
{ title: 'Active', field: 'sellable', type: 'boolean'}
];
/** 
 * Check if any filters passed then only it calls backend to get data 
 * and also if no Filter is applied
 * else display nothing
 * 
*/

const checkFilters=(filters)=>{
  let anyFilterPassed = false;
  let noFilters = true;
  if(filters.length==0)
    return noFilters;
  if(filters){
      filters.forEach((item ,index)=>{
        if(item.column && item.column.customFilterAndSearch){
            if(item.column.customFilterAndSearch(item.value)){
              anyFilterPassed = true;
            }
        }
      });
  }
  return anyFilterPassed;
}
/**
 * Get Meals
 * And all the filters follow some custom rules 
 * which when computed as true then only backend is called 
 * @param {*} query 
 */
const data = (query) => 
  new Promise(resolve => {
    let filters = query.filters;
    console.log(query);
    if(!checkFilters(filters)){
        resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });
    }else{
      asyncCall.post(["secured","search","meal"],query).then(result => {
      if(result.GOODGUT_RESP_CODE=="G200"){
        resolve({
          data: result.data,
          page: result.page,
          totalCount: result.totalCount,
      })
      }else{
         resolve({
        data: [],
        page: 0,
        totalCount: 0,
      });

      }});
    }})

function EntityMeal(props) {
  const tableRef = React.createRef();
  const onRowUpdate = (newData, oldData) => {
    
    return new Promise(resolve => {
      const updatedJson = {};
      updatedJson["primaryKey"]=oldData["mealId"];
      for(let key in newData ){
         if(newData[key]!=oldData[key]){
            updatedJson[key]=newData[key];
        }
      }
      asyncCall.post(["secured","update","meal"],updatedJson).then(result => {
        resolve();
      });
    });
}
  return (
    <div>
    <FormModalOpener buttonLabel="Add Meal" formName="MealForm"/>
    <TechxiTable
      title="Meals"
      tableRef={tableRef}
      columns={column}
      data={data}
      rowUpdateFunction={onRowUpdate}
      options={{
      pageSizeOptions: [5,10,30,50],
      maxBodyHeight:400,
      addRowPosition:'first',
      filtering: true
    }}
    >
    </TechxiTable>

</div>
  );
}
EntityMeal.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(EntityMeal);
