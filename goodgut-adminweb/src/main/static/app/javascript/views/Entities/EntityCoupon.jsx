import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components 
import FormModalOpener from "../Forms/FormModalOpener.jsx"



const styles = {
};

function EntityCoupon(props) {
  const tableRef = React.createRef(); 
  return (
    <div>
        <FormModalOpener buttonLabel="Add Coupon" formName="CouponForm"/>
    </div>
  );
}
EntityCoupon.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(EntityCoupon);
