import React, { useEffect } from 'react';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import promo1 from "../../assets/img/reactlogo.png";
import promo2 from "../../assets/img/sidebar-3.jpg";
import promo3 from "../../assets/img/sidebar-4.jpg";
import clsx from 'clsx';
import GridItem from "../../components/Grid/GridItem.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));


function Promotions(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={4}>
        <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="promotion" className={classes.avatar}>
            R
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Promotion Area One"
        subheader="Lets promote"
      />
      <CardMedia
        className={classes.media}
        image={"admin/static"+promo1}
        title="Promo Image"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            The quick brown fox jumps over the lazy dog is an English-language pangram—a sentence that contains all of the letters of the alphabet. 
            It is commonly used for touch-typing practice
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={()=>{
              alert("We can do more");
          }}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={4}>

        <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="promotion" className={classes.avatar}>
            B
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Promotion Area Two"
        subheader="Lets promote forest"
      />
      <CardMedia
        className={classes.media}
        image={"admin/static"+promo2}
        title="Promo Image"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        Forests are the dominant terrestrial ecosystem of Earth, and are distributed around the globe.[7] Forests account for 75% of the gross primary production of the Earth's biosphere, and contain 80% of the Earth's plant biomass.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={()=>{
              alert("We can do more");
          }}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    </Card>

        </GridItem>
        <GridItem xs={12} sm={6} md={4}>

        <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="promotion" className={classes.avatar}>
            M
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Promotion Area Three"
        subheader="Lets promote mountains"
      />
      <CardMedia
        className={classes.media}
        image={"admin/static"+promo3}
        title="Promo Image"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            A mountain is a large landform that rises above the surrounding land in a limited area, usually in the form of a peak.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={()=>{
              alert("We can do more");
          }}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
    </Card>


        </GridItem>
        </GridContainer> 
    </div>
  );
}
Promotions.propTypes = {
  classes: PropTypes.object
};

export default (Promotions);
