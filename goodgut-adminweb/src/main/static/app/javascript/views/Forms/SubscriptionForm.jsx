import React, { useEffect } from 'react';
import PropTypes, { bool } from "prop-types";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import classNames from "classnames";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import SmartSelect from "../../components/SmartSelect/SmartSelect.jsx";
import validate from "../../helpers/TechxiValidator.js";
import Switch from '@material-ui/core/Switch';
import { asyncCall } from '../../helpers/async-service/service';
const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  dense: {
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  menu: {
    width: 200,
  },
  submitBtn: {
    margin: theme.spacing(1),
  },
  techxiselect: {
    minWidth: 200
  },


}));
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


function getStyles(name, availableDays, theme) {
  return {
    fontWeight:
      availableDays.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

function SubscriptionForm(props) {
  const classes = useStyles();
  

  const setInitialValues = (val) => {
    let values = {};
    values["subscriptionId"] = val["subscriptionId"] ? val["subscriptionId"] : "";
    values["subDescription"] = val["subDescription"] ? val["subDescription"] : "";
    values["imageUrl"] = val["imageUrl"] ? val["imageUrl"] : "";
    values["name"] = val["name"] ? val["name"] : "";
    values["offerAvailable"] = val["offerAvailable"] ? val["offerAvailable"] : false;
    values["price"] = val["price"] ? val["price"] : 0;
    values["offerPrice"] = val["offerPrice"] ? val["offerPrice"] : 0;
    values["subscriptionPeriodType"] = val["subperiod"] ? val["subperiod"] : "";
    values["calories"] = val["calories"] ? val["calories"] : 0;
    values["protein"] = val["protein"] ? val["protein"] : 0;
    values["mealSubscriptionCatagory"] = val["mealSubscriptionCatagory"] ? val["mealSubscriptionCatagory"] : "";
    values["mondayArray"] = val["mondayArray"] ? val["mondayArray"] : [];
    values["tuesdayArray"] = val["tuesdayArray"] ? val["tuesdayArray"] : [];
    values["wedArray"] = val["wedArray"] ? val["wedArray"] : [];
    values["thursdayArray"] = val["thursdayArray"] ? val["thursdayArray"] : [];
    values["fridayArray"] = val["fridayArray"] ? val["fridayArray"] : [];
    values["saturdayArray"] = val["saturdayArray"] ? val["saturdayArray"] : [];
    values["mondayArrayInitVal"] = val["mondayArrayInitVal"] ? val["mondayArrayInitVal"] : [];
    values["tuesdayArrayInitVal"] = val["tuesdayArrayInitVal"] ? val["tuesdayArrayInitVal"] : [];
    values["wedArrayInitVal"] = val["wedArrayInitVal"] ? val["wedArrayInitVal"] : [];
    values["thursdayArrayInitVal"] = val["thursdayArrayInitVal"] ? val["thursdayArrayInitVal"] : [];
    values["fridayArrayInitVal"] = val["fridayArrayInitVal"] ? val["fridayArrayInitVal"] : [];
    values["saturdayArrayInitVal"] = val["saturdayArrayInitVal"] ? val["saturdayArrayInitVal"] : [];
    return values;
  }
const closeModal=()=>{
    if(props.closeModal)
        props.closeModal();
  };
  const [values, setValues] = React.useState(setInitialValues(props.values));


  const isValid = (field) => {
    if (field == "subDescription") {
      return validate(values.subDescription, "^[a-zA-Z0-9\\s,'-:]*$", true);
    }
    else if (field == "name") {
      return validate(values.name, "OnlyStringWithSpace");
    }
    else if (field == "price") {
      return validate(values.price, "OnlyNumber");
    }
    else if (field == "calories") {
      return validate(values.price, "OnlyNumber");
    }
    else if (field == "offerPrice") {
      return validate(values.price, "OnlyNumber");
    }
    return true;
  }
  const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];

  /**
   * Update values for all the textField and switches basically single value element
   * you can add validation also here
   */
  const updateValue = (inputKey, inputValue) => {
    var newValues = {};

    Object.keys(values).forEach(function (key) {
      if (key == inputKey)
        newValues[key] = inputValue;
      else
        newValues[key] = values[key];
    });

    setValues(newValues);

  }
  /**
   * Update available days
   * @param {*} inputKey 
   * @param {*} array 
   */
  const updateArrayValue = (inputKey, array) => {
    var newValues = {};
    Object.keys(values).forEach(function (key) {
      if (key == inputKey) {
        newValues[key] = array;
      }
      else
        newValues[key] = values[key];
    });
    setValues(newValues);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    let goodToSubmit = true;
    const submitJson = {};
    let mealArray = [];
    Object.keys(values).forEach(function (key) {
      const val = values[key];
      if (!isValid(key))
        goodToSubmit = false;
      else {
        if (Array.isArray(val) && key.indexOf("InitVal") < 0) {

          if (val && val.length > 0) {
            mealArray = mealArray.concat(val);
          }

        } else {
          if (val && key.indexOf("InitVal") < 0)
            submitJson[key] = val;
        }

      }
    });
    if (!goodToSubmit) return;
    submitJson["mealIds"] = mealArray;
    asyncCall.post(["secured", "subs", "addSubscription"], submitJson).then(result => {
      closeModal();
    }).catch(err => {
      closeModal();
      alert("Technical error please try later");
    });
  }
  return (
    <div>
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          required
          id="name"
          label="Name"
          value={values.name}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
          onChange={(event) => updateValue("name", event.target.value)}
        />
        <TextField
          required
          id="imageUrl"
          label="Image Url"
          onChange={(event) => updateValue("imageUrl", event.target.value)}
          defaultValue={values.imageUrl}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <TextField
          required
          error={!isValid("subDescription")}
          id="subDescription"
          label="Description"
          onChange={(event) => updateValue("subDescription", event.target.value)}
          defaultValue={values.subDescription}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <TextField
          id="calories"
          label="Subscription Calories"
          onChange={(event) => updateValue("calories", event.target.value)}
          type="number"
          defaultValue={values.calories}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <TextField
          id="protein"
          defaultValue={values.protein}
          label="Subscription Protein"
          onChange={(event) => updateValue("protein", event.target.value)}
          type="number"
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <TextField
          id="outlined-number"
          label="Sub Price"
          defaultValue={values.price}
          onChange={(event) => updateValue("price", event.target.value)}
          type="number"
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <FormControl className={classNames(classes.formControl, classes.techxiselect)}>
          <InputLabel htmlFor="subperiod">Subscription Period</InputLabel>
          <Select
            value={values.subperiod}
            onChange={(event) => updateValue("subperiod", event.target.value)}
            inputProps={{
              name: 'subperiod',
              id: 'subperiod',
            }}
          >
            <MenuItem value="WEEKLY">WEEKLY</MenuItem>
            <MenuItem value="TWOWEEKS">TWOWEEKS</MenuItem>
            <MenuItem value="FOURWEEKS">FOURWEEKS</MenuItem>
            <MenuItem value="MONTHLY">Monthly</MenuItem>
            <MenuItem value="QUATERLY">QUATERLY</MenuItem>
            <MenuItem value="HALFYEARLY">HALFYEARLY</MenuItem>
            <MenuItem value="YEARLY">YEARLY</MenuItem>


          </Select>
        </FormControl>
        <FormControl className={classNames(classes.formControl, classes.techxiselect)}>
          <InputLabel htmlFor="mealSubscriptionCatagory">Subscription Category</InputLabel>
          <Select
            value={values.mealSubscriptionCatagory}
            onChange={(event) => updateValue("mealSubscriptionCatagory", event.target.value)}
            inputProps={{
              name: 'mealSubscriptionCatagory',
              id: 'mealSubscriptionCatagory',
            }}
          >
            <MenuItem value="VEG">Veg</MenuItem>
            <MenuItem value="NONVEG">Non-Veg</MenuItem>
          </Select>
        </FormControl>
        <FormControlLabel
          control={
            <Switch
              checked={values.offerAvailable}
              onChange={(event) => updateValue("offerAvailable", event.target.checked)}
              color="primary"
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          }
          label="Offer Available"
        />
        <TextField
          id="outlined-number"
          label="Offer Price"
          defaultValue={values.offerPrice}
          onChange={(event) => updateValue("offerPrice", event.target.value)}
          type="number"
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />

        <SmartSelect className={classes.smartSelect}
          defaultValue={values.mondayArrayInitVal}
          placeholder="Select Monday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "1" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var mondayArray = [];
            inputValue.forEach((item, index) => {
              mondayArray.push(item["value"]);
            });
            updateArrayValue("mondayArray", mondayArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.tuesdayArrayInitVal}
          placeholder="Select Tuesday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "2" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var tuesdayArray = [];
            inputValue.forEach((item, index) => {
              tuesdayArray.push(item["value"]);
            });
            updateArrayValue("tuesdayArray", tuesdayArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.wedArrayInitVal}
          placeholder="Select Wednesday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "3" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var wedArray = [];
            inputValue.forEach((item, index) => {
              wedArray.push(item["value"]);
            });
            updateArrayValue("wedArray", wedArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.thursdayArrayInitVal}
          placeholder="Select Thursday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "4" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var thursdayArray = [];
            inputValue.forEach((item, index) => {
              thursdayArray.push(item["value"]);
            });
            updateArrayValue("thursdayArray", thursdayArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.fridayArrayInitVal}
          placeholder="Select Friday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "5" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var fridayArray = [];
            inputValue.forEach((item, index) => {
              fridayArray.push(item["value"]);
            });
            updateArrayValue("fridayArray", fridayArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.saturdayArrayInitVal}
          placeholder="Select Saturday Meal"
          pathArray={["secured", "search", "select", "meal"]}
          extraSearchParams={{ "availableDays": "6" }}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var saturdayArray = [];
            inputValue.forEach((item, index) => {
              saturdayArray.push(item["value"]);
            });
            updateArrayValue("saturdayArray", saturdayArray);
          }}
        >
        </SmartSelect>
        <Button
          className={classes.submitBtn}
          color="primary"
          onClick={handleSubmit}
          size="small"
          variant="contained"
        >Submit</Button>
      </form></div>

  );
}
SubscriptionForm.propTypes = {
  classes: PropTypes.object
};
export default (SubscriptionForm);