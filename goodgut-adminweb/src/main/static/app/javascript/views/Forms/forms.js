import MealForm from "./MealForm.jsx";
import SubscriptionForm from "./SubscriptionForm.jsx";
import CouponForm from "./CouponForm.jsx";
const techxiForms = {
  "MealForm":{
    component:MealForm,
    title:"Meal Form"
  },
  "SubscriptionForm":{
    component:SubscriptionForm,
    title:"Subscription Form"
  },
  "CouponForm":{
    component:CouponForm,
    title:"Add Coupon"
  },
  
};

export default techxiForms;
