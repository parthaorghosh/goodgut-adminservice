import React, { useEffect } from 'react';
import PropTypes from "prop-types";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import classNames from "classnames";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import SmartSelect from "../../components/SmartSelect/SmartSelect.jsx";
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  dense: {
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  menu: {
    width: 200,
  },
  submitBtn:{
     margin: theme.spacing(1),
  },
  techxiselect:{
    minWidth:200
  },
  
  
}));
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


function getStyles(name, availableDays, theme) {
  return {
    fontWeight:
      availableDays.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

function MealForm(props) {
  const classes = useStyles();
  const theme = useTheme();
  const setInitialValues=(values)=>{
    let initVal={};
    initVal["mealDesc"]=values["mealDesc"]?values["mealDesc"]:"";
    initVal["mealName"]=values["mealName"]?values["mealName"]:"";
    initVal["offerAvailable"]=values["offerAvailable"]?values["offerAvailable"]:false;
    initVal["mealPrice"]=values["mealPrice"]?values["mealPrice"]:0;
    initVal["offerPrice"]=values["offerPrice"]?values["offerPrice"]:0;
    initVal["mealType"]=values["mealType"]?values["mealType"]:"";
    initVal["mealCategory"]=values["mealCategory"]?values["mealCategory"]:"";
    initVal["availableDays"]=values["availableDays"]?values["availableDays"]:[];
    initVal["menuItemsArray"]=values["menuItemsArray"]?values["menuItemsArray"]:[];
    setValues(initVal);
  }

  useEffect(() => {
    // called Once only to update initial value
    if(props.values)
      setInitialValues(props.values);
  },[]);

  const [values, setValues] = React.useState({
    mealDesc: '',
    mealName: '',
    offerAvailable: false,
    mealPrice: 0,
    offerPrice: 0,
    mealType:'',
    mealCategory:'',
    availableDays:[],
    menuItemsArray:[]
  });

const days = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];
 
  /**
   * Update values for all the textField and switches basically single value element
   * you can add validation also here
   */
  const updateValue = (inputKey, inputValue) => {
    var newValues = {};

    Object.keys(values).forEach(function (key) {
      if (key == inputKey)
        newValues[key] = inputValue;
      else
        newValues[key] = values[key];
    });

    setValues(newValues);

  }
  /**
   * Update available days
   * @param {*} inputKey 
   * @param {*} options 
   */
  const updateAvailableDays = (event) => {
    var newValues = {};
    Object.keys(values).forEach(function (key) {
      if (key == 'availableDays') {
        newValues[key] = event.target.value;
      }
      else
        newValues[key] = values[key];
    });
    setValues(newValues);
  }
  /**
   * Update available days
   * @param {*} inputKey 
   * @param {*} array 
   */
  const updateArrayValue = (inputKey,array) => {
    var newValues = {};
    Object.keys(values).forEach(function (key) {
      if (key == inputKey) {
        newValues[key] = array;
      }
      else
        newValues[key] = values[key];
    });
    setValues(newValues);
  }

  const handleSubmit = (event) => {
    Object.keys(values).forEach(function (key) {
      console.log(key + ":" + values[key]);
    });
    event.preventDefault();
  }
  return (
    <div>
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          required
          id="mealName"
          label="Name"
          defaultValue=""
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
          onChange={(event) => updateValue("mealName", event.target.value)}
        />
        <TextField
          required
          id="mealDesc"
          label="Description"
          onChange={(event) => updateValue("mealDesc", event.target.value)}
          defaultValue=""
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <TextField
          id="outlined-number"
          label="Meal Price"
          onChange={(event) => updateValue("mealPrice", event.target.value)}
          type="number"
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <FormControl className={classNames(classes.formControl,classes.techxiselect)}>
        <InputLabel htmlFor="mealType">Meal Type</InputLabel>
        <Select
          value={values.mealType}
          onChange={(event) => updateValue("mealType", event.target.value)}
          inputProps={{
            name: 'mealType',
            id: 'mealType',
          }}
        >
          <MenuItem value="BREAKFAST">Breakfast</MenuItem>
          <MenuItem value="PBREAKFAST">Post Breakfast</MenuItem>
          <MenuItem value="LUNCH">Lunch</MenuItem>
          <MenuItem value="SNACKS">Snacks</MenuItem>
          <MenuItem value="DINNER">Dinner</MenuItem>
          <MenuItem value="PDINNER">Post Dinner</MenuItem>

        </Select>
      </FormControl>
      <FormControl className={classNames(classes.formControl,classes.techxiselect)}>
        <InputLabel htmlFor="mealCategory">Meal Category</InputLabel>
        <Select
          value={values.mealCategory}
          onChange={(event) => updateValue("mealCategory", event.target.value)}
          inputProps={{
            name: 'mealCategory',
            id: 'mealCategory',
          }}
        >
          <MenuItem value="VEG">Veg</MenuItem>
          <MenuItem value="NONVEG">Non-Veg</MenuItem>
              </Select>
      </FormControl>
       <FormControl className={classNames(classes.formControl,classes.techxiselect)}>
          <InputLabel htmlFor="select-multiple-chip">Available Days</InputLabel>
          <Select
            multiple
            value={values.availableDays}
            onChange={updateAvailableDays}
            input={<Input id="multiple-availableDays-chip" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {selected.map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {days.map(day => (
              <MenuItem key={day} value={day} style={getStyles(day, values.availableDays,theme)}>
                {day}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControlLabel
        control={
          <Switch
          checked={values.offerAvailable}
          onChange={(event) => updateValue("offerAvailable", event.target.checked)}
          color="primary"
          value="offerAvailable"
          inputProps={{ 'aria-label': 'primary checkbox' }}
          />
        }
        label="Offer Available"
      />
        <TextField
          id="outlined-number"
          label="Offer Price"
          onChange={(event) => updateValue("offerPrice", event.target.value)}
          type="number"
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
        />
       
        <SmartSelect className={classes.smartSelect}
          placeholder="Select Menu Items"
          pathArray={["secured","search","select","menuItem"]}
          extraSearchParams={{}}
          primarySearchField="itemName"
          onChange={(inputValue, { action }) => {
             
              var menuItemsArray=[];
              inputValue.forEach((item, index) =>{
                  menuItemsArray.push(item["value"]);
              });
              updateArrayValue("menuItemsArray",menuItemsArray);
                 }}
        >
        </SmartSelect>
        <Button
            className={classes.submitBtn}
            color="primary"
            onClick={handleSubmit}
            size="small"
            variant="contained"
          >Submit</Button>
        </form></div>
     
  );
}
MealForm.propTypes = {
  classes: PropTypes.object
};
export default (MealForm);