import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import techxiForms from "./forms.js";
import CircularProgress from '@material-ui/core/CircularProgress';
function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 20;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${left}%, -${top}%)`,
  };
}

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
  paper: {
    position: 'absolute',
    width: 800,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
   button: {
    margin: theme.spacing(1),
  },
}));


/**
 * This component acts like a button and open respective 
 * entity form in a modal
 */
function EditFormModalOpener(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [showLoader, setShowLoader] = React.useState(true);
  const [entityValues, setEntityValues] = React.useState({});
  
  const handleOpen = (entityId,getEntity) => {
    setOpen(true);
    console.log("WHy am i called")
    getEntity(entityId).then(result=>{
      setEntityValues(result);
      setShowLoader(false);
    });
    
  };
 
  const handleClose = () => {
    setOpen(false);
  };
  /**
   * Returns specific form and also init with values if provided
   */
  const getForm = (name,values)=>{
      if(techxiForms[name]){
        let Component = techxiForms[name]["component"];
        if(values)
          return <Component values={values["entityValues"]} closeModal={handleClose}/>;
        else
          return <Component />;
      }
     return "<div></div>";  
  }
  const getFormTitle = (name)=>{
      if(techxiForms[name]){
        return techxiForms[name]["title"];
      }
     return "Form";  
  }
  return (
     <div>
      <Button variant="outlined" color="primary" className={classes.button} onClick={()=>handleOpen(props.entityId,props.getEntity)}>
        {props.buttonLabel?props.buttonLabel:"Edit Form"}
      </Button>
        <Modal
          aria-labelledby="modal-title"
          open={open}
          onClose={handleClose}
        >
        <div style={modalStyle} className={classes.paper}>
          <h2 id="modal-title">
              {getFormTitle(props.formName)}
        </h2>
          {showLoader?(<CircularProgress className={classes.progress} />):
          getForm(props.formName,{entityValues})}
        </div>
      </Modal>
    </div>
     
  );
}

export default (EditFormModalOpener);