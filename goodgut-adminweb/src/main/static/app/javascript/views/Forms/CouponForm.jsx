import React, { useEffect } from 'react';
import PropTypes, { bool } from "prop-types";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import classNames from "classnames";
import SmartSelect from "../../components/SmartSelect/SmartSelect.jsx";

import { asyncCall } from '../../helpers/async-service/service';
const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  dense: {
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  menu: {
    width: 200,
  },
  submitBtn: {
    margin: theme.spacing(1),
  },
  techxiselect: {
    minWidth: 200
  },


}));
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


function getStyles(name, availableDays, theme) {
  return {
    fontWeight:
      availableDays.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

function SubscriptionForm(props) {
  const classes = useStyles();
  

const closeModal=()=>{
    if(props.closeModal)
        props.closeModal();
  };
  const [values, setValues] = React.useState({
    couponCode: '',
    marketingMessage: '',
    discountType: '',
    status: '',
    discountValue: 0,
    maxDiscountValue:0,
    mealArray:[],
    subArray:[]
  });


  const isValid = (field) => {
   
    return true;
  }
  

  /**
   * Update values for all the textField and switches basically single value element
   * you can add validation also here
   */
  const updateValue = (inputKey, inputValue) => {
    var newValues = {};

    Object.keys(values).forEach(function (key) {
      if (key == inputKey)
        newValues[key] = inputValue;
      else
        newValues[key] = values[key];
    });

    setValues(newValues);

  }
  /**
   * Update available days
   * @param {*} inputKey 
   * @param {*} array 
   */
  const updateArrayValue = (inputKey, array) => {
    var newValues = {};
    Object.keys(values).forEach(function (key) {
      if (key == inputKey) {
        newValues[key] = array;
      }
      else
        newValues[key] = values[key];
    });
    setValues(newValues);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    let goodToSubmit = true;
    const submitJson = {};
    let mealArray = [];
    let subArray = [];
    Object.keys(values).forEach(function (key) {
      const val = values[key];
      if (!isValid(key))
        goodToSubmit = false;
      else {
        if (Array.isArray(val) && key.indexOf("InitVal") < 0) {

          if (val && val.length > 0 && key =='mealArray') {
            mealArray = mealArray.concat(val);
          }
          if (val && val.length > 0 && key =='subArray') {
            subArray = subArray.concat(val);
          }

        } else {
          if (val && key.indexOf("InitVal") < 0)
            submitJson[key] = val;
        }

      }
    });
    if (!goodToSubmit) return;
    submitJson["mealArray"] = mealArray;
    submitJson["subArray"] = subArray;
    asyncCall.post(["secured", "coupon", "addCoupon"], submitJson).then(result => {
      closeModal();
    }).catch(err => {
      closeModal();
      alert("Technical error please try later");
    });
  }
  return (
    <div>
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          required
          id="couponCode"
          label="Coupon Code"
          value={values.couponCode}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
          onChange={(event) => updateValue("couponCode", event.target.value)}
        />
        <TextField
          required
          id="marketingMessage"
          label="Marketing Message"
          onChange={(event) => updateValue("marketingMessage", event.target.value)}
          defaultValue={values.imageUrl}
          className={classNames(classes.textField, classes.dense)}
          margin="dense"
          variant="outlined"
        />
        <FormControl className={classNames(classes.formControl, classes.techxiselect)}>
          <InputLabel htmlFor="discountType">Discount Type</InputLabel>
          <Select
            value={values.discountType}
            onChange={(event) => updateValue("discountType", event.target.value)}
            inputProps={{
              name: 'discountType',
              id: 'discountType',
            }}
          >
            <MenuItem value="PERCENTAGE">Percentage</MenuItem>
            <MenuItem value="RUPEE">Rupee</MenuItem>
          
          </Select>
        </FormControl>
        <FormControl className={classNames(classes.formControl, classes.techxiselect)}>
          <InputLabel htmlFor="status">Status</InputLabel>
          <Select
            value={values.status}
            onChange={(event) => updateValue("status", event.target.value)}
            inputProps={{
              name: 'status',
              id: 'status',
            }}
          >
            <MenuItem value="ACTIVE" selected>Active</MenuItem>
            <MenuItem value="INACTIVE" selected>In-Active</MenuItem>
          </Select>
        </FormControl>
        
        <TextField
          id="outlined-number"
          label="Discount Value"
          onChange={(event) => updateValue("discountValue", event.target.value)}
          type="number"
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
        />

        <TextField
          id="outlined-number"
          label="Max Discount Value"
          onChange={(event) => updateValue("maxDiscountValue", event.target.value)}
          type="number"
          className={classNames(classes.textField,classes.dense)}
          margin="dense"
          variant="outlined"
        />

        <SmartSelect className={classes.smartSelect}
          defaultValue={values.mondayArrayInitVal}
          placeholder="Select Meal"
          pathArray={["secured", "search", "select", "meal"]}
          primarySearchField="mealName"
          onChange={(inputValue, { action }) => {
            var mealArray = [];
            inputValue.forEach((item, index) => {
                mealArray.push(item["value"]);
            });
            updateArrayValue("mealArray", mondayArray);
          }}
        >
        </SmartSelect>
        <SmartSelect className={classes.smartSelect}
          defaultValue={values.tuesdayArrayInitVal}
          placeholder="Select Subscription"
          pathArray={["secured", "search", "select", "subscription"]}
          primarySearchField="name"
          onChange={(inputValue, { action }) => {
            var subArray = [];
            inputValue.forEach((item, index) => {
              subArray.push(item["value"]);
            });
            updateArrayValue("subArray", tuesdayArray);
          }}
        >
        </SmartSelect>
        
        <Button
          className={classes.submitBtn}
          color="primary"
          onClick={handleSubmit}
          size="small"
          variant="contained"
        >Submit</Button>
      </form>
      </div>

  );
}
SubscriptionForm.propTypes = {
  classes: PropTypes.object
};
export default (SubscriptionForm);