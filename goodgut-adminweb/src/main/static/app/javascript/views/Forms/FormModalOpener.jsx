import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import MealForm from "./MealForm.jsx";
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import techxiForms from "./forms.js";
function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 20;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${left}%, -${top}%)`,
  };
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 800,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
   button: {
    margin: theme.spacing(1),
  },
}));
/**
 * This component acts like a button and open respective 
 * entity form in a modal
 */
function FormModalOpener(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  /**
   * Returns specific form and also init with values if provided
   */
  const getForm = (name,values)=>{
      if(techxiForms[name]){
        let Component = techxiForms[name]["component"];
        if(values){
          return <Component values={values}/>;
        }
          
        else
          return <Component/>;
      }
     return "<div></div>";  
  }
  const getFormTitle = (name)=>{
      if(techxiForms[name]){
        return techxiForms[name]["title"];
      }
     return "Form";  
  }
  return (
     <div>
      <Button variant="outlined" color="primary" className={classes.button} onClick={handleOpen}>
        {props.buttonLabel?props.buttonLabel:"Open Form"}
      </Button>
        <Modal
          aria-labelledby="modal-title"
          open={open}
          onClose={handleClose}
        >
        <div style={modalStyle} className={classes.paper}>
          <h2 id="modal-title">
              {getFormTitle(props.formName)}
        </h2>
          {getForm(props.formName,props.values)}
        </div>
      </Modal>
    </div>
     
  );
}

export default (FormModalOpener);