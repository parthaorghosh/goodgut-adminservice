// Palette
import palette from '../palette';

export default {
  root: {
    borderBottom: `1px solid ${palette.divider}`,
    padding: '5px',
    
  },
  head: {
    fontSize: '14px',
    color: palette.text.primary
  }
};
