import Dashboard from "@material-ui/icons/Dashboard";
import TableChart from "@material-ui/icons/TableChart";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import MealPage from "views/Entities/EntityMeal.jsx";
import SubscriptionPage from "views/Entities/EntitySubscription.jsx";
import OrderPage from "views/Entities/EntityOrder.jsx";
import MenuItemPage from "views/Entities/EntityMenuItem.jsx";
import SubOrderPage from "views/Entities/EntitySubscriptionOrder.jsx";
import CouponPage from "views/Entities/EntityCoupon.jsx";


const dashboardRoutes = [
  {
    path: "/secured/main/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    pathPrefix: "/admin",
    sublist:false
  },
  {
    name:"Entities",
    sublist:true,
    icon:TableChart,
    path: "/secured/main/entities",
    pathPrefix: "/admin",
    childlist:[
      {
        key:"subKey",
        path:"subscription",
        name: "Subscription",
        component: SubscriptionPage,
    },
    {
        key:"mealKey",
        path:"/meal",
        name: "Meal",
        component: MealPage,
    }, 
    {
        key:"menuItemKey",
        path:"/menuItem",
        name: "Menu Item",
        component: MenuItemPage,
    },
    {
      key:"orderKey",
      path:"order",
      name: "Order",
      component: OrderPage,
  },
  {
    key:"subOrderKey",
    path:"subOrder",
    name: "Subscription Order",
    component: SubOrderPage,
},{
  key:"couponKey",
  path:"coupon",
  name: "Add Coupon",
  component: CouponPage,
},
  ],
  }

];

export default dashboardRoutes;
