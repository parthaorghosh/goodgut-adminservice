import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';

const useStyles = makeStyles(theme => ({
  typography: {
    padding: theme.spacing(2),
  },
  padding10:{
      padding:"10px"
  },
  addressPopOverUl:{
    padding:"20px",
    listStyle:"none"
  },
  listSeperator:{
    marginTop:"10px",
    marginBottom:"10px",
    fontWeight:"bold"
  },
  namePointer:{
    cursor:"pointer"
  }
}));

export default function TechxiPopover({ ...props }) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const {popoverHtml,displayName} = props;
  
  
  const handleClick = event => {
    let target = event.currentTarget;
    setAnchorEl(target);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <a  className={classes.namePointer} aria-describedby={id} variant="contained" color="primary" onClick={handleClick}>
        {displayName}
      </a>
      
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
         <div className={classes.padding10}> {popoverHtml} </div>
          
      </Popover>
    </div>
  );
}