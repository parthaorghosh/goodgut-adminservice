import React, { useEffect } from 'react';
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import '../../assets/css/bootstrap.min.css'
import '../../assets/css/daterangepicker.css'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
const useStyles = makeStyles(theme => ({
  dateContainer: {
    border: "1px solid #ccc",
    padding: "5px 10px",
  },
 calendar:{
    cursor:"pointer",
    background: "none",
    border:"none",
    verticalAlign: "middle"
  }
}));

export default function TechxiDatePicker(props) {
  const classes = useStyles();
  const getCurrentDate=()=>{

    let todayTime = new Date();
    let month = todayTime .getMonth() + 1;
    let day = todayTime .getDate();
    let year =todayTime .getFullYear();
   return month + "/" + day + "/" + year;
}
  const getToday=()=>{
      return getCurrentDate()+" - "+getCurrentDate();
  }
  const [displayDate, setDisplayDate] = React.useState(getToday());
  const {startDate,endDate,ranges,onEvent} = props;
  return (
<div>
<DateRangePicker 
startDate={startDate} 
endDate={endDate}
ranges={ranges}
onEvent={(event, picker)=>{
  if(event.type=='apply'){
    setDisplayDate(picker.startDate.format('MM/DD/YYYY')+" - "+picker.endDate.format('MM/DD/YYYY'));
  }
  onEvent(event, picker);
}}
>
<div className={classes.dateContainer}>
  <span>{displayDate}</span>
  <button className={classes.calendar}>
      <CalendarTodayIcon></CalendarTodayIcon>
  </button>
</div>
        
</DateRangePicker>

</div>
  );
}

