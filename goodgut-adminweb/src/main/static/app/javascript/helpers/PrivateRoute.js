
import  isLogin  from './checkLogin';
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
const PrivateRoute = ({ component: Comp, path, ...rest }) => {
  return (
    <Route
      path={path}
      {...rest}
      render={props => {
        return isLogin() ? <Comp {...props} /> : <Redirect to="/admin/signin" />;
      }}
    />
  );
};
export default PrivateRoute;