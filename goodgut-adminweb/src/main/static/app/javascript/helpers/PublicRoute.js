import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import isLogin  from './checkLogin';

const PublicRoute = ({component: Component, restricted,path,siginpage, ...rest}) => {
    return (
        // restricted = false meaning public route
        // restricted = true meaning restricted route
        //siginpage is present and user is logged in move them to dasboard
        <Route  path={path} {...rest} render={props => (
            (isLogin() && restricted)||(isLogin() && siginpage) ?
                <Redirect to="/admin/secured/main/dashboard" />
            : <Component {...props} />
        )} />
    );
};

export default PublicRoute;