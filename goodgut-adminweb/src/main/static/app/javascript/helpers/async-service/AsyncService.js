import _ from "underscore";

const serviceURL = service => {
  const host = window.location.host.split(".");
  host.splice(0, 1, service);
  return `${window.location.protocol}//${service}`;
};

const methods = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  PATCH: "PATCH",
  DELETE: "DELETE"
};

export default class Service {
  constructor(service, url = serviceURL(service)) {
    this._url = url;
  }

  /**
   *
   * @param {String} method
   * @param {Array.<String>} pathSegments path segments
   * @param {Object} [query]
   * @param {Object} [body] JS object
   * @param {Object} [headers]
   */
  async fetch(method = "GET", pathSegments, query = {}, body, headers) {
    if (sessionStorage.getItem("authToken") && headers) {
      headers["authToken"] = sessionStorage.getItem("authToken");
    }

    const path = pathSegments.map(encodeURIComponent).join("/");
    const queryStr = _.map(query, (val, key) => {
      const encode = (key, val) =>
        `${encodeURIComponent(key)}=${encodeURIComponent(val)}`;
      if (typeof val === "object") {
        // array (or object)
        return _.map(val, x => encode(key, x)).join("&");
      } else {
        return encode(key, val);
      }
    }).join("&");
    body = body ? JSON.stringify(body) : body;
    const response = await fetch(
      `${this._url}/${path}${queryStr ? `?${queryStr}` : ""}`,
      { method, headers, body, credentials: "include" }
    );
    const contentType = response.headers.get("content-type");
    if (response.status >= 400) {
      return Promise.reject(response);
    }
    if (contentType === "application/json;charset=UTF-8") {
      return response.json();
    } else if (contentType === "application/json") {
      return response.json();
    }
    else if (contentType.indexOf("text") > -1) {
      return response.text();
    } else {
      return response.text();
    }
  }

  /**
   * Does a get request to this service
   * @param {Array.<String>} pathSegments - segments of the path, not encoded
   * @param {Object} query - Object containing key value pairs for query parameters
   * @param {Object} headers - Object containing header key value pairs
   */
  get(pathSegments, query, headers) {
    return this.fetch(methods.GET, pathSegments, query, null, headers);
  }

  /**
   *
   * @param {Array.<String>} pathSegments
   * @param {Object} jsonBody
   * @param {Object} headers
   * @param {Object} [query]
   */
  post(pathSegments, jsonBody, headers, query) {
    return this.fetch(methods.POST, pathSegments, query, jsonBody, {
      "Content-Type": "application/json",
      ...headers
    });
  }

  postCSV(pathSegments, jsonBody, headers, query) {
    return this.fetch(methods.POST, pathSegments, query, jsonBody, {
      ...headers
    });
  }

  /**
   *
   * @param {Array.<String>} pathSegments
   * @param {Object} jsonBody
   * @param {Object} headers
   * @param {Object} [query]
   */
  put(pathSegments, jsonBody, headers, query) {
    return this.fetch(methods.PUT, pathSegments, query, jsonBody, {
      "Content-Type": "application/json",
      ...headers
    });
  }
}
