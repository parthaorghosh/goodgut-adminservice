const regexConatiner={
	"OnlyString":"^[a-zA-Z]$",
	"OnlyStringWithSpace":"^[\\sa-zA-Z]*$",
	"AlphaNumericString":"^[a-zA-Z0-9]$",
	"AlphaNumericStringWithSpace":"^[a-zA-Z0-9\\s]*$",
	"OnlyNumber":"^[0-9]+$",
};
/**
 * Validate fields 
 * pass custom regex if needed
 * in type
 */
function validate(value,type,customRegex){
	const regex = regexConatiner[type];
	if(regex){
		const regtester = new RegExp( regex, "g" );
		return regtester.test(value);
	}
	else if(customRegex){
		return new RegExp( type, "g" ).test(value);
	}
	return false;
}

export default (validate)
