import "babel-polyfill";
import React from "react";
import App from "./App";
import ReactDOM from "react-dom";


var div = document.createElement("div");
div.id = "root";
document.body.appendChild(div);
ReactDOM.render(
  <App />,
  div
);



