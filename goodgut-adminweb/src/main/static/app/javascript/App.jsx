import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
// Routes
import Routes from './Routes';
// Material helpers
import { ThemeProvider } from '@material-ui/styles';
import "assets/css/material-dashboard-react.css";

// Theme
import theme from './theme';
// Browser history
const browserHistory = createBrowserHistory();




export default class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Router history={browserHistory}>
          <Routes />
        </Router>
      </ThemeProvider>

    );
  }
}
