import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// Views
/**<Redirect from="/" to="/admin/dashboard"></Redirect>
				  <PrivateRoute component={Dashboard} path="/admin/dashboard" exact />
				   */
import SignIn from './views/SignIn';
import Main from './views/main/Main.jsx';
import Dashboard from './views/Dashboard/Dashboard.jsx';
import PrivateRoute from './helpers/PrivateRoute';
import PublicRoute from './helpers/PublicRoute';
export default class Routes extends Component {
	render() {
		return (
			<Switch>
				<PublicRoute component={SignIn} restricted={false} path="/admin/signin"/>
				<PrivateRoute component={Main} path="/admin/secured/main" />
				
			</Switch>
		);
	}
}
