const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const SRC_OUTPUT = "static";
const SOURCEMAPS_OUTPUT = "_sourcemaps";
const WEBAPP = "webapp";
const JS = "javascript";
const outputRoot = path.resolve(__dirname, "..", "..", WEBAPP);
console.log(path.resolve(__dirname, "/.."));

module.exports = {
  entry: {
        main: `./${JS}/main.jsx`
  },
  output: {
    path: path.resolve(outputRoot, SRC_OUTPUT),
    publicPath: "/"
  },
  resolve: {
        modules: ["../node_modules", path.resolve(`./${JS}`), path.resolve("."), './css','./assets'],
    alias: {},
    extensions: [".wasm", ".mjs", ".js", ".json", ".jsx"]
  },
  module: {
        rules: [
      {
        test: /\.jsx?$/,
        exclude: {
          or: [/node_modules\\(?!@esko)/]
        },
                use: [{
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
              plugins: [
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-proposal-class-properties"
              ]
            }
                }]
      },
      {
        test: /\.(png|PNG|jpg|JPG|gif|GIF|svg|SVG|cur|ico)$/,
                use: [{
            loader: "file-loader",
            options: {}
                }]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)(\?cl=0)?$/,
        loader: "file-loader"
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [
        path.join(outputRoot, SRC_OUTPUT),
        path.join(outputRoot, SOURCEMAPS_OUTPUT)
      ],
      dangerouslyAllowCleanPatternsOutsideProject: true,
      dry: false
    }),
    new HtmlWebpackPlugin({
      chunks: ["main"],
      inject: false,
      template: 'handlebars-loader!./index_template.jsp',
      filename: "../index.jsp"
    }),
    new HtmlWebpackPlugin({
        chunks: ["main"],
        inject: false,
        template: 'handlebars-loader!./orderBillPrint.jsp',
        filename: "../orderBillPrint.jsp"
      })
  ]
};
