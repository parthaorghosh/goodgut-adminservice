package com.techxi.goodgut.form;

public class UserForm {
	
	
	 	private String userName;
	    private String password;
	    private String retypePassword;
	    private String firstName;
	    private String lastName;
	    private String userStatus;
	    private String userAuthType;
	    private String city;
	    private String state;
	    private String pinCode;
	    private String primaryAddress;
		
	    //Default Constructor
	    public UserForm() {
			super();
		}

		

		public String getUserName() {
			return userName;
		}



		public void setUserName(String userName) {
			this.userName = userName;
		}



		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getRetypePassword() {
			return retypePassword;
		}

		public void setRetypePassword(String retypePassword) {
			this.retypePassword = retypePassword;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUserStatus() {
			return userStatus;
		}

		public void setUserStatus(String userStatus) {
			this.userStatus = userStatus;
		}

		public String getUserAuthType() {
			return userAuthType;
		}

		public void setUserAuthType(String userAuthType) {
			this.userAuthType = userAuthType;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		

		public String getPinCode() {
			return pinCode;
		}



		public void setPinCode(String pinCode) {
			this.pinCode = pinCode;
		}



		public String getPrimaryAddress() {
			return primaryAddress;
		}

		public void setPrimaryAddress(String primaryAddress) {
			this.primaryAddress = primaryAddress;
		}



		@Override
		public String toString() {
			return "UserForm [userName=" + userName + ", password=" + password + ", retypePassword=" + retypePassword
					+ ", firstName=" + firstName + ", lastName=" + lastName + ", userStatus=" + userStatus
					+ ", userAuthType=" + userAuthType + ", city=" + city + ", state=" + state + ", pinCode=" + pinCode
					+ ", primaryAddress=" + primaryAddress + "]";
		}

	
		
	    
}
