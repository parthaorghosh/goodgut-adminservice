package com.techxi.goodgut.server.admin.configuration;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 * Logistic Solution {@link ConfigurableSiteMeshFilter}
 */
public class GoodgutSiteMeshFilter extends ConfigurableSiteMeshFilter {

	private static final String THEME_LOGIN = "/WEB-INF/decorators/login-theme.jsp";
	private static final String THEME_BASIC = "/WEB-INF/decorators/basic-theme.jsp";
	private static final String THEME_NO_DECORATOR = "/WEB-INF/decorators/no-decorator-theme.jsp";

	/**
	 * Configure sitemesh decorator per URL, default decorator is basic-theme.jsp
	 *
	 * @param builder
	 */
	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		
		builder.addDecoratorPath("/*", THEME_BASIC)
				.addDecoratorPath("/auth/prelogin", THEME_LOGIN)
				.addDecoratorPath("/auth/logout", THEME_LOGIN)
				.addDecoratorPath("/orders/getOrder", THEME_NO_DECORATOR)
				.addDecoratorPath("/orders/getSubOrder", THEME_NO_DECORATOR)
				.addDecoratorPath("/meal/getItemsByName", THEME_NO_DECORATOR)
				.addDecoratorPath("/orders/printOrderBill", THEME_NO_DECORATOR)
				
				
				.addDecoratorPath("/changePassword", THEME_LOGIN)
				.addDecoratorPath("/statistics/view", THEME_NO_DECORATOR)
				.addExcludedPath("/kibana/*");
				;
	}

}
