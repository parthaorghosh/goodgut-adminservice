package com.techxi.goodgut.server.admin.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

@Configuration
@EnableWebSecurity
public class GoodgutWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	

	/**
	 * Set the authentication provider to the custom
	 * {@link com.sicap.mts.core.services.impl.IPAddressAuthenticationProvider}
	 *
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin@admin.com").password("admin").roles("ADMINISTRATOR");
	}


	/**
	 * Configure web pages to use form security
	 */
	@Configuration
	@Order(1)
	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

		@Value("${location.management.mock.enabled:false}")
		private boolean locationManagementMockEnabled = false;

		 @Override
	        public void configure(WebSecurity web) throws Exception {
	            web.ignoring().antMatchers("/js/**", "/img/**", "/styles/**","/css/**");
	        }
		/**
		 * Configure web pages to use form security
		 *
		 * @param http
		 *            to disable csrf
		 * @throws Exception
		 */

		/*@Override
		protected void configure(HttpSecurity http) throws Exception {
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http
					.csrf().disable().headers().frameOptions().sameOrigin().and().authorizeRequests()
					.antMatchers("/**").hasRole("ADMINISTRATOR");
				expressionInterceptUrlRegistry.antMatchers("/**").authenticated().and().formLogin()
					.loginPage("/auth/prelogin").permitAll().defaultSuccessUrl("/auth/login")
					.loginProcessingUrl("/login").permitAll().and().logout().logoutUrl("/auth/logout").permitAll();
				
				
				http.sessionManagement().sessionFixation().migrateSession();

		}*/
		 @Override
			protected void configure(HttpSecurity http) throws Exception {
				ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http
						.csrf().disable().headers().frameOptions().sameOrigin().and().authorizeRequests();

				expressionInterceptUrlRegistry.antMatchers("/secured/**").hasRole("ADMINISTRATOR").antMatchers("/secured/**").
				authenticated().and().formLogin().loginPage("/signin").loginProcessingUrl("/login")
						.permitAll().successForwardUrl("/login/done").failureForwardUrl("/login/fail").permitAll();

			}
	}
}