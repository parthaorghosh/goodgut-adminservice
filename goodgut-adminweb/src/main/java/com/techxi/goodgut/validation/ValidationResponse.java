package com.techxi.goodgut.validation;

import java.util.List;

import org.springframework.validation.FieldError;

/**
 * @author ext_norsys_vgroux
 * 
 */
public class ValidationResponse {
	/**
	 * In case response is a Failure
	 */
	public static final String VALIDATION_RESPONSE_FAILURE = "FAIL";
	/**
	 * In case response is a Success
	 */
	public static final String VALIDATION_RESPONSE_SUCCESS = "SUCCESS";

	/**
	 * Status of the response, Should be VALIDATION_RESPONSE_FAILURE or VALIDATION_RESPONSE_SUCCESS
	 */
	private String status;

	/**
	 * In case response is a failure, result is a list of error
	 */
	private List<FieldError> result;

	/**
	 * @return the {@link ValidationResponse} status field value
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the {@link ValidationResponse} fields error list
	 */
	public List<FieldError> getResult() {
		return this.result;
	}

	public void setResult(List<FieldError> result) {
		this.result = result;
	}
}
