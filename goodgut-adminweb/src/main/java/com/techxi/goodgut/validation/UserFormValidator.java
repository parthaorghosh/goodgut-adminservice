package com.techxi.goodgut.validation;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.techxi.goodgut.form.UserForm;

public class UserFormValidator implements Validator {

	//login should be an email
	String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
            "[a-zA-Z0-9_+&*-]+)*@" + 
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
            "A-Z]{2,7}$";
	@Autowired
	@Qualifier("webMessageSource")
	private ReloadableResourceBundleMessageSource messageSource;

	
	@Override
	public boolean supports(Class<?> clazz) {
		return UserForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		UserForm userForm = (UserForm) target;
		// Check if login is email or not
		Pattern pat = Pattern.compile(emailRegex);
				if (!pat.matcher(userForm.getUserName()).matches()) {
					errors.rejectValue("login", null,
							messageSource.getMessage("users.validation.loginmin", null, LocaleContextHolder.getLocale()));
				}
       // Login Field is mandatory
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));
		
		// primary address Field is mandatory
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "primaryAddress", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));
		
		// pincode Field is mandatory
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pinCode", null,
								messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));
		// pincode Field is mandatory
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));
		// pincode Field is mandatory
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));


	}

}
