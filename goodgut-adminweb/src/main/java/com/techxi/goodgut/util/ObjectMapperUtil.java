package com.techxi.goodgut.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.clients.model.SubscriptionOrder;


public class ObjectMapperUtil {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	public static ObjectMapper getMapper() {
		return MAPPER;
	}

	public static <E> List<E> getListOfObjects(String jsonArray, Class<E> type) throws IOException {
		
		TypeReference<?> ref= 	null;
		if (type.equals(Meals.class))
			ref = new TypeReference<List<Meals>>() {};
		else if (type.equals(MenuItem.class))
			ref = new TypeReference<List<MenuItem>>() {};
		else if (type.equals(Order.class))
			ref = new TypeReference<List<Order>>() {};
		else if (type.equals(SubscriptionOrder.class))
			ref = new TypeReference<List<SubscriptionOrder>>(){};
		else if (type.equals(Subscription.class))
				ref = new TypeReference<List<Subscription>>(){};	
		else
			return new ArrayList<>();
		
		return MAPPER.readValue(jsonArray, ref);
	}	
	public static Map<String,Object> getJsonMap(String jsonString) throws IOException {
		return MAPPER.readValue(jsonString, new TypeReference<HashMap<String, Object>>() {
		});
	}

	public static <E> E getObject(String json, Class<E> type) throws IOException {
		return  MAPPER.readValue(json, type);
	}

	public static <E> String writeValueAsString(E type) throws JsonProcessingException {
		return MAPPER.writeValueAsString(type);
	}
}
