package com.techxi.goodgut.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.User;
import com.techxi.goodgut.core.services.UserServiceSVCApi;
import com.techxi.goodgut.form.AddressForm;
import com.techxi.goodgut.form.UserForm;
import com.techxi.goodgut.validation.ValidationResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(UserController.USER_REQUEST_MAPPING_URL)
public class UserController {

	public static final String USER_REQUEST_MAPPING_URL = "/user";
	public static final String ADDUSER_REQUEST_MAPPING_URL = "/addUserAndAddress";
	public static final String EDITUSER_REQUEST_MAPPING_URL = "/editUser";
	public static final String USERFORM_LOGIN_FIELD_NAME = "login";
	
	@Autowired
	UserServiceSVCApi userSVCApi;
	/*@Autowired
	@Qualifier("webMessageSource")
	protected ReloadableResourceBundleMessageSource messageSource;*/
	
	@RequestMapping(value = ADDUSER_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public ValidationResponse add(@RequestBody  AddressForm addressForm, BindingResult bindingResult,HttpServletRequest request)
	{
		ValidationResponse validationResponse = new ValidationResponse();
		// we try to add an existing user.
				/*if (!"".equals(userForm.getLogin()) && userDao.findByPk(userForm.getLogin()) != null) {
					bindingResult.rejectValue(USERFORM_LOGIN_FIELD_NAME, null,
							messageSource.getMessage("users.validation.loginalreadyexist", null, LocaleContextHolder.getLocale()));
				}

				ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "password", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));

				ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "retypePassword", null,
						messageSource.getMessage("users.validation.required", null, LocaleContextHolder.getLocale()));*/
				
				System.out.println(addressForm);
				System.out.println("LLLL"+userSVCApi.getUserDetailByUser("Partha"));
				
       Address address = mapAddressFormtoAddress(addressForm);
       userSVCApi.addUserAndAddress(address);
		return validationResponse;
	}
	
	@RequestMapping(value = EDITUSER_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public ValidationResponse edit(@RequestBody  UserForm userForm, BindingResult bindingResult,HttpServletRequest request)
	{
		return null;
		
	}
	
	public Address mapAddressFormtoAddress(AddressForm addressForm)
	{
		Address address = new Address();
		User user = new User();
		
		address.setAddressLine1(addressForm.getAddressLine1());
		address.setAddressLine2(addressForm.getAddressLine2());
		address.setAddressLine3(addressForm.getAddressLine3());
		address.setCity(addressForm.getCity());
		address.setContactNo(addressForm.getContactNo());
		address.setName(addressForm.getName());
		address.setPinCode(addressForm.getPinCode());
		address.setState(addressForm.getState());
		
		UserForm userForm = addressForm.getUserForm();
		
		user.setUserName(userForm.getUserName());
		user.setPassword(userForm.getPassword());
		user.setStatus(userForm.getUserStatus());
		user.setUserAuthType(userForm.getUserAuthType());
		user.setFirstName(userForm.getFirstName());
		user.setLastName(userForm.getLastName());
		user.setCity(userForm.getCity());
		user.setState(userForm.getState());
		user.setPincode(userForm.getPinCode());
		user.setPrimaryAddress(userForm.getPrimaryAddress());
		address.setUserAddresses(user);;
		
		return address;
	}
}
