package com.techxi.goodgut.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.MealItemsSVCApi;
import com.techxi.goodgut.validation.ValidationResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(MealController.MEAL_REQUEST_MAPPING_URL)
public class MealController {
	
	public static final String MENUITEM_REQUEST_MAPPING_URL = "/addItem";
	public static final String GETMENUITEM_REQUEST_MAPPING_URL = "/getItems";
	public static final String MEAL_REQUEST_MAPPING_URL = "secured/meal";
	public static final String ADDMEAL_REQUEST_MAPPING_URL = "/addmeal";
	public static final String GETMEAL_REQUEST_MAPPING_URL = "/getMeal";
	public static final String ADDSUBSCRIPTION_REQUEST_MAPPING_URL = "/addSubscription";
	public static final String GETSUBSCRIPTION_REQUEST_MAPPING_URL = "/getSubscription";
	@Autowired
	MealItemsSVCApi mealItemsSVCApi;
	
	@RequestMapping(value = MENUITEM_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public ValidationResponse addMenuItem(HttpServletRequest request)
	{
		ValidationResponse validationResponse = new ValidationResponse();
		
		//add request validation;
		
		
		String name = request.getParameter("menuitemname");
		String desc = request.getParameter("menuitemdesc");
		float price = Float.parseFloat(request.getParameter("itemprice"));
		float itemOfferPrice =Float.parseFloat(request.getParameter("itemprice"));//offerprice = normalprice
		boolean offerAvailable = Boolean.valueOf(request.getParameter("offerAvailable"));
		if(offerAvailable)
			 itemOfferPrice = Float.parseFloat(request.getParameter("itemOfferPrice"));
		String itemType = request.getParameter("itemType");
		String itemAvailableFor = request.getParameter("itemAvailableFor");
		String itemquantity = request.getParameter("itemquantity");
		String itemunit = request.getParameter("itemunit");
		String calories = request.getParameter("calories");
		String protein = request.getParameter("protein");
		String carbs = request.getParameter("carbs");
		String fat = request.getParameter("fat");
		String fibre = request.getParameter("fibre");
		String imageurl = request.getParameter("imageurl");
		
		
		
		
		MenuItem menuItem = new MenuItem();
		
		menuItem.setImageUrl(imageurl);
		menuItem.setItemDescription(desc);
		menuItem.setItemName(name);
		menuItem.setItemViewColumn(-1);
		menuItem.setItemViewRow(-1);
		menuItem.setOfferAvail(offerAvailable);
		menuItem.setPrice(price);
		menuItem.setOfferPrice(itemOfferPrice);
		
		
		JSONObject obj = new JSONObject();
		
		obj.put("quantity", itemquantity);
		obj.put("unit", itemunit);
		obj.put("cal", calories);
		obj.put("fat", fat);
		obj.put("fibre", fibre);
		obj.put("pro", protein);
		obj.put("carb", carbs);
		
		String jsonMetadata = obj.toJSONString();
		
		menuItem.setItemMetadata(jsonMetadata);
		menuItem.setItemType(itemType);
		menuItem.setMealType(itemAvailableFor);
		
		mealItemsSVCApi.addMenutItem(menuItem);
		return validationResponse;
		
	}
	
	@RequestMapping(value = GETMENUITEM_REQUEST_MAPPING_URL, method = RequestMethod.GET)
	@ResponseBody
	public BaseEntityResponse getMenuItems(HttpServletRequest request)
	{
		
		return mealItemsSVCApi.getMenuItems(100,1);
	
		
	}
	
	@RequestMapping(value = ADDMEAL_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public String addMeal(HttpServletRequest request)
	{
		String resp ="";
		//validation needed
		String name = request.getParameter("mealname");
		String desc = request.getParameter("mealdesc");
		float price = Float.parseFloat(request.getParameter("mealprice"));
		float itemOfferPrice =Float.parseFloat(request.getParameter("mealprice"));//offerprice = normalprice
		boolean offerAvailable = Boolean.valueOf(request.getParameter("offerAvailable"));
		if(offerAvailable)
			 itemOfferPrice = Float.parseFloat(request.getParameter("mealOfferPrice"));
		String itemType = request.getParameter("itemType");
		String itemAvailableFor = request.getParameter("itemAvailableFor");
		String imageurl = request.getParameter("imageurl");
		String[] menuItemsArray = request.getParameterValues("menuitems[]");
		String[] availabLeDays = request.getParameterValues("availableOn[]");
		int row = Integer.parseInt(request.getParameter("row"));
		int column = Integer.parseInt(request.getParameter("column"));
		String cal = request.getParameter("calories");
		
		
		String productMealName =request.getParameter("productMealType");

		HashMap<Integer,MenuItem> menuItemMap = (HashMap<Integer,MenuItem>)request.getSession().getAttribute("menuItemsMap");
		
		if(menuItemMap==null || menuItemMap.isEmpty()){
			
			resp="RESP_503";
			return resp;
		}
		Set<MenuItem> menuItems = new HashSet<>();
		
		for (String string : menuItemsArray) {
			MenuItem item = menuItemMap.get(Integer.parseInt(string));
			menuItems.add(item);
		}
		
		String temp="";
		for (String days : availabLeDays) {
			
			if(temp=="")
				temp+=days;
			else	
				temp+=","+days;
			
		}
		
		
		Meals meal = new Meals();
		
		
		meal.setMealName(name);
		meal.setMealDescription(desc);
		meal.setMealViewColumn(column);
		meal.setMealViewRow(row);
		meal.setMealItems(menuItems);
		meal.setMealPrice(price);
		meal.setOfferPrice(itemOfferPrice);
		meal.setOfferAvail(offerAvailable);
		meal.setMealType(itemAvailableFor);
		meal.setMealSubscriptionCatagory(itemType);
		meal.setAvailableDays(temp);
		meal.setImageUrl(imageurl);
		meal.setMealProductName(productMealName);
		
		mealItemsSVCApi.addMeals(meal);
		
		
		
		return "RESP_200";
		
	}
	
	@RequestMapping(value = GETMEAL_REQUEST_MAPPING_URL, method = RequestMethod.GET)
	@ResponseBody
	public BaseEntityResponse getMeals(HttpServletRequest request)
	{
		return mealItemsSVCApi.getMeals();
		
	}
	
	@RequestMapping(value = ADDSUBSCRIPTION_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public String addSubscription(HttpServletRequest request)
	{
		String resp = "";
		//add validation
		String name = request.getParameter("subsname");
		String desc = request.getParameter("subsdesc");
		float price = Float.parseFloat(request.getParameter("subsprice"));
		float itemOfferPrice =Float.parseFloat(request.getParameter("subsprice"));//offerprice = normalprice
		boolean offerAvailable = Boolean.valueOf(request.getParameter("offerAvailable"));
		if(offerAvailable)
			 itemOfferPrice = Float.parseFloat(request.getParameter("subsOfferPrice"));
		String itemType = request.getParameter("itemType");
		String subscriptionPeriodType = request.getParameter("subperiod");
		String calories = request.getParameter("calories");
		String imageurl = request.getParameter("imageurl");
		String meals = request.getParameter("meals");
		String[] mealsArray = meals.split(",");
		for (String string : mealsArray) {
					
				
		}
		
		Subscription subs = new Subscription();
		
		HashMap<Integer,Meals> mealMap = (HashMap<Integer,Meals>)request.getSession().getAttribute("mealsMap");
		
		if(mealMap==null || mealMap.isEmpty()){
			
				resp="RESP_503";
				return resp;
		}
		Set<Meals> mealsSet = new HashSet<>();
		
		for (String string : mealsArray) {
			Meals item = mealMap.get(Integer.parseInt(string));
			mealsSet.add(item);
		}
		
		subs.setCalories(Integer.parseInt(calories));
		subs.setImageUrl(imageurl);
		subs.setMeals(mealsSet);
		subs.setMealSubscriptionCatagory(itemType);
		subs.setName(name);
		subs.setOfferAvail(offerAvailable);
		subs.setOfferPrice(itemOfferPrice);
		subs.setPrice(price);
		subs.setSubDescription(desc);
		subs.setSubscriptionPeriodType(subscriptionPeriodType);
		mealItemsSVCApi.addSubscription(subs);
		return resp;
		
	}
	
	@RequestMapping(value = GETSUBSCRIPTION_REQUEST_MAPPING_URL, method = RequestMethod.GET)
	@ResponseBody
	public List<Subscription> getSubscription(HttpServletRequest request)
	{
		return mealItemsSVCApi.getSubscription();
		
	}

}
