package com.techxi.goodgut.controller;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractController {
	private static String secret = "!@#$MySawr5hPajjw0rd";
	protected static final String DATE_PATTERN = "yyyy-MM-dd";
	protected static final String DATE_PATTERN_NORMAL = "dd-MM-yyyy";
	protected static final String DATE_TIME_MINSEC_PATTERN = "dd-MM-yyyy HH:mm:ss";
	protected static final String DATE_TIME_MIN_PATTERN = "yyyy-MM-dd HH:mm";
	protected static final String DATE_TIME_HR_PATTERN = "yyyy-MM-dd HH:00";
	protected static final String DATE_TIME_HR_MIN_AM_PMPATTERN = "dd/MM/yyyy HH:mm:ss";
	protected static final String PATTERN_WITH_MONTH_IMWORDS = "yyyy-MMM-dd";
	private static final String LOGSTASH_PATTERN = "MM/dd/yyyy HH:mm:sss";
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractController.class);
	protected static final Logger ERROR = LoggerFactory.getLogger("ERROR");
	protected static final String OPERATION_TYPE_LOCATION_UPDATED = "LOCATION_UPDATED";
	public static String INDIAN_TIME_ZONE = "GMT+5:30";

	protected static final Logger activityLog = LoggerFactory.getLogger("logisticActivity");
	public static String DATE_TIME_PATTERN_INCOMING = "dd/MM/yyyy H:mm:ss";
	public static SimpleDateFormat _sdf_incoming = new SimpleDateFormat(DATE_TIME_PATTERN_INCOMING);

	public static DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
	public static DateFormat dateFormatNormal = new SimpleDateFormat(DATE_PATTERN);
	public static DateFormat dateTimeMinSecFormat = new SimpleDateFormat(DATE_TIME_MINSEC_PATTERN);
	public static DateFormat dateTimeHrFormatIST = new SimpleDateFormat(DATE_TIME_HR_PATTERN);
	public static DateFormat dateTimeMinFormat = new SimpleDateFormat(DATE_TIME_MIN_PATTERN);
	public static DateFormat monthInwordsFormat = new SimpleDateFormat(PATTERN_WITH_MONTH_IMWORDS);
	public static DateFormat dateFormatInAMPM = new SimpleDateFormat(DATE_TIME_HR_MIN_AM_PMPATTERN);
	public SimpleDateFormat _sdfOrcale = new SimpleDateFormat("yyyy-MM-dd H:m");
	public SimpleDateFormat _sdfOrcaleTypetwo = new SimpleDateFormat("dd-MMM-yy");

	public static DateFormat logstashformat = new SimpleDateFormat(LOGSTASH_PATTERN);

	public String addTokenInSession(String tokenname, String decodedToken, HttpServletRequest req) {

		String encryptedToken = encode(decodedToken);

		req.getSession().setAttribute(tokenname + "_ENC", encryptedToken);
		req.getSession().setAttribute(tokenname + "_DEC", decodedToken);
		return encryptedToken;
	}

	public void removeTokenFromSession(String tokenname, HttpServletRequest req) {
		req.getSession().removeAttribute(tokenname);
	}

	public String getRandomString() {
		return RandomStringUtils.randomAlphanumeric(10);

	}

	/**
	 * Converts String to UTF8 bytes
	 *
	 * @param input
	 *            the input string
	 * @return UTF8 bytes
	 */
	private static byte[] getUTF8Bytes(String input) {
		return input.getBytes(StandardCharsets.UTF_8);
	}

	private static byte[] fixSecret(String s, int length) throws UnsupportedEncodingException {
		if (s.length() < length) {
			int missingLength = length - s.length();
			for (int i = 0; i < missingLength; i++) {
				s += " ";
			}
		}
		return s.substring(0, length).getBytes(StandardCharsets.UTF_8.name());
	}

	/**
	 * 
	 * Encode a String
	 * 
	 * 
	 */
	public String encode(String plain) {
		String encodedString = "";

		try {
			byte[] key = fixSecret(secret, 16);
			SecretKeySpec secretKey = new SecretKeySpec(key, "AES");

			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);

			byte[] plainText = getUTF8Bytes(plain);
			byte[] cipherText = cipher.doFinal(plainText);
			encodedString = Base64.getEncoder().encodeToString(cipherText);
			// return StringUtils.newString(cipherText, "UTF-8")

		} catch (Exception e) {

		}

		return encodedString;

	}

	/**
	 * Decode
	 * 
	 * @param encrypted
	 *            String
	 * @return
	 */
	public static String decode(String encryptedString) {
		String deceodedString = "";

		try {

			byte[] key = fixSecret(secret, 16);
			SecretKeySpec secretKey = new SecretKeySpec(key, "AES");

			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);

			byte[] encText = Base64.getDecoder().decode(encryptedString);
			byte[] decodedText = cipher.doFinal(encText);

			return org.apache.commons.codec.binary.StringUtils.newString(decodedText, "UTF-8");
		} catch (Exception e) {

		}
		return deceodedString;
	}

	/**
	 * 1 = int 2= onlystring 3=alphastring 4=strings With space
	 * 
	 * @param val
	 * @param type
	 * @return
	 */

	public boolean checkType(String val, int type) {

		switch (type) {
		case 1:
			return val.matches("[0-9]+");
		case 2:
			return val.matches("[a-zA-Z]+");
		case 3:
			return val.matches("[a-zA-Z0-9]+");
		case 4:
			return val.matches("[a-zA-Z\\s]+");
		default:
			break;
		}

		return true;
	}

	/**
	 * Checks the length of a string val[]
	 * 
	 * @param val
	 * @param length
	 * @return
	 */
	public boolean checkLength(String[] val, int length) {

		for (String string : val) {
			if (string.length() != length)
				return false;
		}
		return true;

	}

	public JSONObject getJsonObjectFromString(String json) throws ParseException {
		return (JSONObject) new JSONParser().parse(json);
	}

	public Date getDateAfterXDays(int numberOfDays, Date startDate) {

		Calendar cal = new GregorianCalendar();
		if (startDate != null)
			cal.setTime(startDate);

		cal.add(Calendar.DAY_OF_MONTH, numberOfDays);

		return cal.getTime();

	}

	/**
	 * 
	 * @param tokenname
	 * @param incomingToken
	 * @param req
	 * @return
	 */
	public static boolean checkToken(String completeTokenName, String incomingToken, HttpServletRequest req) {

		String decodedTokenFromReq = decode(incomingToken);
		String tokenInSession = (String) req.getSession().getAttribute(completeTokenName);

		return (!StringUtils.isEmpty(decodedTokenFromReq) && decodedTokenFromReq.equals(tokenInSession));
	}
}
