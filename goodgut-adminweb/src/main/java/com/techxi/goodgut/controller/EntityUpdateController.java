package com.techxi.goodgut.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.goodgut.admin.response.json.BaseResponse;
import com.techxi.goodgut.core.impl.MealItemsSCVCImpl;
import com.techxi.goodgut.core.services.EntitySearchSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(EntityUpdateController.PARENT_PATH)
public class EntityUpdateController extends AbstractController{
	public static final String PARENT_PATH = "secured/update";
	private static final List<String> EntityList = Arrays
			.asList(new String[] { "meal", "menuItem", "subscription", "order" });

	@Autowired
	EntitySearchSVCApi entitySearchSVCApi;
	@Autowired
	MealItemsSCVCImpl mealSvc;
	
	
	@PostMapping(path ="/{entity}")
	@ResponseBody
	public BaseResponse updateEntity(@PathVariable("entity") String entity,@RequestBody Map<?,?> updatedEntityMap) {
			
			if(EntityList.contains(entity))
				return entitySearchSVCApi.updateEntity(updatedEntityMap, entity);
			else
				return null;
		}
	
}