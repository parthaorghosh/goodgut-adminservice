package com.techxi.goodgut.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.SubscriptionAddressResponse;
import com.techxi.goodgut.core.services.AddressSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller

public class AddressController {

	public static final String ADDRESS_REQUEST_MAPPING_URL = "/address";
	public static final String GET_ADDRESS_BY_ID_PATH = "/address/getAddressById";
	public static final String GET_ADDRESS_OF_SUBS = "/secured/address/getSubscriptionAddress";
	@Autowired
	AddressSVCApi addressSVC;

	@RequestMapping(value = GET_ADDRESS_BY_ID_PATH, method = RequestMethod.GET)
	@ResponseBody
	public Address getAddressById(HttpServletRequest request, HttpServletResponse response) {

		// add request validation;

		Integer addId = -1;
		try {
			addId = Integer.parseInt(request.getParameter("addId"));
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		
		Address address = addressSVC.getAddressById(addId);
		
		if(address==null){
			return null;
		}
		
		return address;

	}
	
	@RequestMapping(value = GET_ADDRESS_OF_SUBS, method = RequestMethod.GET)
	@ResponseBody
	public List<SubscriptionAddressResponse> getSubscriptionOrderAdress(HttpServletRequest request, HttpServletResponse response) {
		
		
			
		try{
			List<SubscriptionAddressResponse> resp = new ArrayList<>();
			String[] addressArr = request.getParameter("address").split(",");
			if(addressArr.length>=3){
				for(int i = 0; i<addressArr.length;i++){
					
					Integer addrId = Integer.parseInt(addressArr[i]);
					SubscriptionAddressResponse temp = new SubscriptionAddressResponse();
					if(addrId>0){
						Address address = addressSVC.getAddressById(addrId);
						
						if(address!=null){
							temp.setLine1(address.getAddressLine1());
							temp.setLine2(address.getAddressLine2());
							temp.setLine3(address.getAddressLine3());
							temp.setContactNo(address.getContactNo());
							temp.setName(address.getName());
							temp.setLine4(address.getState()+"  "+address.getCity()+"-"+address.getPinCode());
							
						}
						
					}
					//sending empty also becuase sequence decide if it is brkfst , lunch or  dinner 
					resp.add(temp);
					
						
				}
			}
			return resp;
		}
		catch(Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return Collections.EMPTY_LIST;
		}
		
			
			
		

	}

}
