package com.techxi.goodgut.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.server.goodgut.admin.response.json.GoodgutResponseCode;
import com.server.goodgut.admin.response.json.LoginResponse;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.MenuItem;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.MealItemsSVCApi;

import lombok.extern.slf4j.Slf4j;

/**
 * CommonController
 * <p>
 * Author: ankush
 */
@Slf4j
@Controller
public class AuthController extends AbstractController {

	private String login;
	private int remainingTry;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
	private static final Logger activityLog = LoggerFactory.getLogger("logisticActivity");

	public static final String AUTH_REQUEST_MAPPING_URL = "signin";
	public static final String SECURED_PATH = "/secured/main/**";
	public static final String AUTH_INDEX_REQUEST_MAPPING_URL = "index";
	public static final String CUSTOM_SCRIPT_SEARCHES = "cstScriptSearches";
	private static final String LAND_PATH = "/";
	private static final String AUTH_REQUEST_LOGIN_PROCESSING_URL = "/login/done";
	private static final String AUTH_REQUEST_LOGIN_FAIL_PROCESSING_URL = "/login/fail";
	

	/**
	 * Access to the main default page corresponding the
	 * http://localhost:8080/mts/ URL.
	 *
	 * @return the default index page
	 */
	@Autowired
	MealItemsSVCApi mealItemsSVCApi;
	


	@RequestMapping(value = AUTH_REQUEST_MAPPING_URL, method = RequestMethod.GET)
	public ModelAndView defaultPage(Model model, HttpServletRequest req) {

		List<MenuItem> list = new ArrayList<>();
		List<Meals> mealsList = new ArrayList<>();
		List<Subscription> subsList = mealItemsSVCApi.getSubscription();
		
		model = setMealsInModel(model,mealsList);
		model.addAttribute("menuItems", list);
		model.addAttribute("meals", mealsList);
		model.addAttribute("subs", subsList);
		
		setMenuItemInSession(list, req);
		setMealsInSession(mealsList, req);
		setSubsInSession(subsList,req);
		
		
		return new ModelAndView(AUTH_INDEX_REQUEST_MAPPING_URL);
	}
	
	
	@RequestMapping(value = SECURED_PATH, method = RequestMethod.GET)
	public ModelAndView sucuredPath(Model model, HttpServletRequest req) {
		return new ModelAndView(AUTH_INDEX_REQUEST_MAPPING_URL);
	}

	/**
	 * Manage the users authentication login
	 *
	 * @param error
	 *            is set to true based on the when the authentication has
	 *            failed.
	 * @param model
	 * @return the login form view.
	 */
	/*@RequestMapping(value = "/auth/prelogin", method = RequestMethod.GET)
	public ModelAndView preLogin(@RequestParam(value = "error", required = false) String error, ModelMap model,
			HttpServletRequest request) {

		try {
			MDC.put("operationname", "preLogin");
			MDC.put("thread", Thread.currentThread().getName());

			log.debug("Entering prelogin method");

			String username = (String) request.getSession().getAttribute("LOGGED_IN_USER");

			if (username != null && username.equals("admin")) {
				ModelAndView modelAndView = new ModelAndView("redirect:/auth/login");
				log.info("Redirecting to /auth/login");
				return modelAndView;
			}

			else {

				if (error != null) {
					String errorMessage = getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION");
					model.put("error", errorMessage);
				}

				return new ModelAndView("login");
			}
		} finally {
			log.debug("Exiting prelogin method");
			MDC.remove("operationname");
			MDC.remove("user");
			MDC.remove("thread");

		}

	}
*/
	/**
	 * Login in the application and get the default page after a successful
	 * authentication.
	 *
	 * @return the main default view
	 */
	@RequestMapping(value = "/auth/login", method = RequestMethod.GET)
	public ModelAndView login(Principal principal, Model model, HttpServletRequest request) {

		MDC.put("operationname", "login");
		MDC.put("user", request.getUserPrincipal().getName());
		MDC.put("thread", Thread.currentThread().getName());

		log.info("Entering login method");

		List<MenuItem> list = new ArrayList<>();
		List<Meals> mealsList = new ArrayList<>();
		List<Subscription> subsList = mealItemsSVCApi.getSubscription();
		model = setMealsInModel(model,mealsList);
		
		model.addAttribute("menuItems", list);
		model.addAttribute("meals", mealsList);
		model.addAttribute("subs", subsList);
		
		setMenuItemInSession(list, request);
		setMealsInSession(mealsList, request);
		setSubsInSession(subsList,request);
		return new ModelAndView("/index");
	}

	private void setMealsInSession(List<Meals> mealsList, HttpServletRequest request) {
		
		
		HashMap<Integer, Meals> mealsSessionMap = new HashMap<>();

		for (Meals meals : mealsList) {

			mealsSessionMap.put(meals.getMealId(), meals);
		}
		request.getSession().setAttribute("mealsMap", mealsSessionMap);
	} 

	
	private void setSubsInSession(List<Subscription> subList, HttpServletRequest request) {
		
		
		HashMap<Integer, Subscription> subSessionMap = new HashMap<>();

		for (Subscription sub : subList) {

			subSessionMap.put(sub.getSubscriptionId(),sub);
		}
		request.getSession().setAttribute("subsMap", subSessionMap);
	} 
	
	private Model setMealsInModel(Model model, List<Meals> mealsList) {
		
		List<Meals> monList = new ArrayList<>();
		List<Meals> tueList = new ArrayList<>();
		List<Meals> wedList = new ArrayList<>();
		List<Meals> thuList = new ArrayList<>();
		List<Meals> friList = new ArrayList<>();
		List<Meals> satList = new ArrayList<>();
		
		
		
		for (Meals meals : mealsList) {
			
			String availDays = meals.getAvailableDays();
			if(availDays.contains("1")){
				monList.add(meals);
			}
			if(availDays.contains("2")){
				tueList.add(meals);
			}
			if(availDays.contains("3")){
				wedList.add(meals);
			}
			if(availDays.contains("4")){
				thuList.add(meals);
			}
			if(availDays.contains("5")){
				friList.add(meals);
			}
			if(availDays.contains("6")){
				satList.add(meals);
			}
			
		}
		
		
		model.addAttribute("monMeals",monList);
		model.addAttribute("tueMeals",tueList);
		model.addAttribute("wedMeals",wedList);
		model.addAttribute("thuMeals",thuList);
		model.addAttribute("friMeals",friList);
		model.addAttribute("satMeals",satList);

		
		return model;
	}

	/**
	 * Logout from the application
	 *
	 * @return the login form view.
	 */
	@RequestMapping(value = "/auth/logout", method = RequestMethod.GET)
	public ModelAndView logout() {

		return new ModelAndView("login");
	}

	/**
	 * Show a page containing a message to inform the authenticated user that he
	 * doesn't have required roles to access to the requested page.
	 *
	 * @return The deny view.
	 */
	@RequestMapping(value = "/auth/denied", method = RequestMethod.GET)
	public ModelAndView denied() {
		return new ModelAndView("/denied");
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public int getRemainingTry() {
		return remainingTry;
	}

	public void setRemainingTry(int remainingTry) {
		this.remainingTry = remainingTry;
	}

	private String getErrorMessage(HttpServletRequest request, String key) {

	        Object attribute = request.getSession().getAttribute(key);

	      if (attribute instanceof Exception) {

	            Exception exception = (Exception) attribute;
	            String error = exception.getMessage();
	            if(error.equalsIgnoreCase("Bad Credentials")){
	            	error="auth.error.bad.credentials";
	            }
	            return error;
	        } else {
	            return "";
	        }
	    }

	private void setMenuItemInSession(List<MenuItem> menuItemList,HttpServletRequest req){
		 

		HashMap<Integer, MenuItem> menuItemSessionMap = new HashMap<>();

		for (MenuItem menuItem : menuItemList) {

			menuItemSessionMap.put(menuItem.getItemId(), menuItem);
		}
		req.getSession().setAttribute("menuItemsMap", menuItemSessionMap);
	} 
	@GetMapping(value = LAND_PATH)
	public RedirectView initialLanding() {
		return new RedirectView("secured/dashboard");
	}
	@PostMapping(value = AUTH_REQUEST_LOGIN_PROCESSING_URL)
	@ResponseBody
	public LoginResponse login(Principal principal, HttpServletRequest request) {

		request.getSession().setAttribute("isUserLoggedIn", "true");
		addTokenInSession("USER_LOGGED_IN_TOKEN", getRandomString(), request);
		addTokenInSession("GLOBAL_AUTH_TOKEN", getRandomString(), request);
		LoginResponse response = new LoginResponse();
		try {
			response.setGlobalAuthToken((String) request.getSession().getAttribute("GLOBAL_AUTH_TOKEN_ENC"));
			response.setResponseCode(GoodgutResponseCode.SUCCESS);
	
		} catch (Exception e) {
			
			response.setResponseCode(GoodgutResponseCode.FAIL);
		}

		return response;

	}
	@PostMapping(value = AUTH_REQUEST_LOGIN_FAIL_PROCESSING_URL)
	@ResponseBody
	public LoginResponse loginFailed(Principal principal, HttpServletRequest request) {
		request.getSession().setAttribute("isUserLoggedIn", "false");
		LoginResponse response = new LoginResponse();
		try {
			response.setGlobalAuthToken((String) request.getSession().getAttribute("GLOBAL_AUTH_TOKEN_ENC"));
			response.setResponseCode(GoodgutResponseCode.FAIL);
	
		} catch (Exception e) {
			
			response.setResponseCode(GoodgutResponseCode.FAIL);
		}

		return response;

	}
}
