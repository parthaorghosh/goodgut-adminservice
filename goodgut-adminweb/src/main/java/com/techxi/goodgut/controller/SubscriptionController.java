package com.techxi.goodgut.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.goodgut.admin.response.json.GoodgutResponseCode;
import com.techxi.goodgut.clients.common.DBResponseCodes;
import com.techxi.goodgut.clients.model.AddSubscriptionRequest;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.SubscriptionSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(SubscriptionController.SUB_REQUEST_MAPPING_URL)
public class SubscriptionController {

	public static final String SUB_REQUEST_MAPPING_URL = "secured/subs";
	public static final String ADDSUBSCRIPTION_REQUEST_MAPPING_URL = "/addSubscription";
	public static final String GETSUBSCRIPTION_REQUEST_MAPPING_URL = "/getSubsById";
	public static final String GETSUBSCRIPTION_REQUEST_LAZY_MAPPING_URL = "/getSubsById/lazy";
	@Autowired
	SubscriptionSVCApi subsSVC;

	@PostMapping(value = ADDSUBSCRIPTION_REQUEST_MAPPING_URL)
	@ResponseBody
	public String addSubscription(@RequestBody AddSubscriptionRequest req) {

		try {
			if (DBResponseCodes.SUCCESS.equals(subsSVC.addSubscription(req)))
				return GoodgutResponseCode.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return GoodgutResponseCode.FAIL;

	}

	@RequestMapping(value = GETSUBSCRIPTION_REQUEST_MAPPING_URL, method = RequestMethod.GET)
	@ResponseBody
	public Subscription getSubscription(@RequestParam(value = "subid", required = true) int subscriptionId) {
		
		Optional<Subscription> sub = subsSVC.getSubsById(subscriptionId, false);
			if(sub.isPresent())
				return sub.get();
		return null;

	}
	@RequestMapping(value = GETSUBSCRIPTION_REQUEST_LAZY_MAPPING_URL, method = RequestMethod.GET)
	@ResponseBody
	public Subscription getSubscriptionLazy(@RequestParam(value = "subid", required = true) int subscriptionId) {
		
		Optional<Subscription> sub = subsSVC.getSubsById(subscriptionId, true);
			if(sub.isPresent())
				return sub.get();
		return null;

	}

}
