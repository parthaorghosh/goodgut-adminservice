package com.techxi.goodgut.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.goodgut.admin.response.json.BaseEntityResponse;
import com.techxi.goodgut.clients.model.ReactEntityQueryRequestModel;
import com.techxi.goodgut.clients.model.SelectSearchResponse;
import com.techxi.goodgut.core.services.EntitySearchSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(EntitySearchController.PARENT_PATH)
public class EntitySearchController extends AbstractController {
	public static final String PARENT_PATH = "secured/search";
	private static final List<String> EntityList = Arrays
			.asList(new String[] { "meal", "menuItem", "subscription", "order","subscriptionOrder" });

	@Autowired
	EntitySearchSVCApi entitySearchSVCApi;

	@PostMapping(path = "/select/{entity}")
	@ResponseBody
	public SelectSearchResponse getMenuItems(@PathVariable("entity") String entity,
			@RequestBody Map<?, ?> searchModel) {
		try {
			return entitySearchSVCApi.getSelectSearchEntityResponse(entity, searchModel);
		} catch (Exception e) {
			e.printStackTrace();
			return new SelectSearchResponse();
		}
	}

	@PostMapping(path = "/{entity}")
	@ResponseBody
	public BaseEntityResponse getEntityWithFilters(@PathVariable("entity") String entity,
			@RequestBody ReactEntityQueryRequestModel filterModel,HttpServletResponse resp) {
		
		if(!EntityList.contains(entity)){
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		return entitySearchSVCApi.getEntityWithFilters(filterModel, entity);
	}

}