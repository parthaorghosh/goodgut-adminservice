package com.techxi.goodgut.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.server.goodgut.admin.request.AddCoupon;
import com.techxi.goodgut.clients.common.DiscountType;
import com.techxi.goodgut.clients.model.Coupon;
import com.techxi.goodgut.clients.model.Meals;
import com.techxi.goodgut.clients.model.Subscription;
import com.techxi.goodgut.core.services.CouponSVCApi;
import com.techxi.goodgut.core.services.MealItemsSVCApi;
import com.techxi.goodgut.core.services.SubscriptionSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(CouponController.COUPON_REQUEST_MAPPING_URL)
public class CouponController extends AbstractController {

	public static final String ADD_COUPON_URL = "/addCoupon";
	public static final String COUPON_REQUEST_MAPPING_URL = "/secured/coupon/";
	@Autowired
	CouponSVCApi couponSVCApi;

	@Autowired
	SubscriptionSVCApi subsSVC;

	@Autowired
	MealItemsSVCApi mealItemsSVCApi;

	/*
	 * @PostMapping(value = ADD_COUPON_URL)
	 * 
	 * @ResponseBody public String addMenuItem(HttpServletRequest request) {
	 * 
	 * 
	 * String couponcode = request.getParameter("couponcode"); String
	 * marketingMessage = request.getParameter("marketingMessage"); String
	 * discountType =request.getParameter("discountType"); String[] subids =
	 * request.getParameterValues("availableForSubs[]"); String[] mealids =
	 * request.getParameterValues("availableForMeals[]"); String discountValue =
	 * request.getParameter("discountValue"); String maxDiscountValue =
	 * request.getParameter("maxDiscountValue"); String couponfromdate =
	 * request.getParameter("couponfromdate"); String coupondptodate =
	 * request.getParameter("coupondptodate"); String status =
	 * request.getParameter("status"); String allSubs =
	 * request.getParameter("allSubs"); String allMeals =
	 * request.getParameter("allMeals"); Map<Integer,Meals> mealMap =
	 * (HashMap<Integer,Meals>)request.getSession().getAttribute("mealsMap");
	 * Map<Integer,Subscription> subMap =
	 * (HashMap<Integer,Subscription>)request.getSession().getAttribute(
	 * "subsMap"); Set<Meals> setOfMeals = new HashSet<>(); Set<Subscription>
	 * setOfSubs = new HashSet<>(); Coupon coupon = new Coupon();
	 * coupon.setId(couponcode); coupon.setConsumptionPerUser(100);
	 * if(mealids!=null){ for (String id : mealids) { Meals sub =
	 * mealMap.get(Integer.parseInt(id)); if(sub!=null) setOfMeals.add(sub); }
	 * if(!setOfMeals.isEmpty()){
	 * coupon.setCouponApplicableForMeals(setOfMeals); } } if(subids!=null){ for
	 * (String id : subids) { Subscription sub =
	 * subMap.get(Integer.parseInt(id)); if(sub!=null){
	 * 
	 * setOfSubs.add(sub); }
	 * 
	 * } if(!setOfSubs.isEmpty()){
	 * 
	 * coupon.setCouponApplicableForSubs(setOfSubs); }}
	 * 
	 * 
	 * coupon.setCreatedOn(new Date()); try {
	 * coupon.setValidFrom(_sdf_incoming.parse(couponfromdate));
	 * coupon.setValidTo(_sdf_incoming.parse(coupondptodate)); } catch
	 * (ParseException e) { e.printStackTrace(); return "RESP_400"; }
	 * coupon.setDiscountType(DiscountType.valueOf(discountType));
	 * coupon.setDiscountValue(Integer.parseInt(discountValue));
	 * coupon.setStatus(status); coupon.setMarketingMessage("");
	 * coupon.setMaxDiscountValue(Integer.parseInt(maxDiscountValue));
	 * coupon.setCouponApplicableForMeals(setOfMeals);
	 * //coupon.setCouponApplicableForSubs(setOfSubs);
	 * couponSVCApi.addCoupon(coupon);
	 * 
	 * 
	 * return "RESP_200";
	 * 
	 * }
	 */

	@PostMapping(value = ADD_COUPON_URL)
	@ResponseBody
	public String addMenuItem(@RequestBody AddCoupon addCouponReqeust, HttpServletRequest request) {

		String couponcode = addCouponReqeust.getCouponCode();
		String marketingMessage = addCouponReqeust.getMarketingMessag();
		String discountType = addCouponReqeust.getDiscountType();
		String[] subids = addCouponReqeust.getSubArray();
		String[] mealids = addCouponReqeust.getMealArray();
		String discountValue = String.valueOf(addCouponReqeust.getDiscountValue());
		String maxDiscountValue = String.valueOf(addCouponReqeust.getMaxDiscountValue());
		String status = addCouponReqeust.getStatus();

		List<Meals> listMeals = mealItemsSVCApi.getAllMeals();
		List<Subscription> subscriptionLists = mealItemsSVCApi.getSubscription();
		Map<Integer, Meals> mealMap = new HashMap<>();

		Map<Integer, Subscription> subMap = new HashMap<>();
		listMeals.forEach((m) -> {
		});
		subscriptionLists.forEach(item -> {
			subMap.put(item.getSubscriptionId(), item);
		});
		listMeals.forEach(item -> {
			mealMap.put(item.getMealId(), item);
		});
		Set<Meals> setOfMeals = new HashSet<>();
		Set<Subscription> setOfSubs = new HashSet<>();
		Coupon coupon = new Coupon();
		coupon.setId(couponcode);
		coupon.setConsumptionPerUser(100);
		if (mealids != null && mealids.length > 0) {
			for (String id : mealids) {
				Meals sub = mealMap.get(Integer.parseInt(id));
				if (sub != null)
					setOfMeals.add(sub);
			}
			if (!setOfMeals.isEmpty()) {
				coupon.setCouponApplicableForMeals(setOfMeals);
			}
		} else {
			// set all meals for discount
			mealMap.forEach((k, v) -> {
				setOfMeals.add(v);
			});

		}
		if (subids != null && subids.length > 0) {
			for (String id : subids) {
				Subscription sub = subMap.get(Integer.parseInt(id));
				if (sub != null) {
					setOfSubs.add(sub);
				}

			}
			if (!setOfSubs.isEmpty()) {
				coupon.setCouponApplicableForSubs(setOfSubs);
			}
		} else {
			// set all subs for discount
			subMap.forEach((k, v) -> {
				setOfSubs.add(v);
			});
			coupon.setCouponApplicableForSubs(setOfSubs);
		}
		if (setOfMeals.isEmpty() || setOfSubs.isEmpty()) {
			return "RESP_400";
		}
		coupon.setCreatedOn(new Date());
		try {

			LocalDate ld = new LocalDate();

			coupon.setValidFrom((ld.now().toDate()));
			coupon.setValidTo((ld.plusDays(90).toDate()));
		} catch (Exception e) {
			e.printStackTrace();
			return "RESP_400";
		}
		coupon.setDiscountType(DiscountType.valueOf(discountType));
		coupon.setDiscountValue(Integer.parseInt(discountValue));
		coupon.setStatus(status);
		coupon.setMarketingMessage("");
		coupon.setMaxDiscountValue(Integer.parseInt(maxDiscountValue));

		coupon.setCouponApplicableForMeals(setOfMeals);
		coupon.setCouponApplicableForSubs(setOfSubs);

		couponSVCApi.addCoupon(coupon);

		return "RESP_200";

	}

}
