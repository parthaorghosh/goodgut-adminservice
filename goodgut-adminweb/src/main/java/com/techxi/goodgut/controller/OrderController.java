package com.techxi.goodgut.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.techxi.goodgut.clients.model.Address;
import com.techxi.goodgut.clients.model.Order;
import com.techxi.goodgut.clients.model.SubscriptionOrder;
import com.techxi.goodgut.core.services.AddressSVCApi;
import com.techxi.goodgut.core.services.OrderSVCApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(OrderController.ORDER_REQUEST_MAPPING_URL)
public class OrderController {

	public static final String ORDERS_REQUEST_MAPPING_URL = "/getOrder";
	public static final String SUB_ORDERS_REQUEST_MAPPING_URL = "/getSubOrder";
	public static final String UPDATE_ORDERS_REQUEST_MAPPING_URL = "/updateOrder";
	public static final String PRINT_ORDERS_BILL_REQUEST_MAPPING_URL = "/printOrderBill/{id}";
	public static final String ORDER_REQUEST_MAPPING_URL = "secured/orders";
	private static String DATE_TIME_PATTERN_FROM_ORACLE = "yyyy-MM-dd H:mm";
	private static String DATE_TIME_PATTERN_TO_ORACLE = "yyyy-MM-dd H:mm";
	private static String DATE_TIME_PATTERN_INCOMING = "dd/MM/yyyy H:mm:ss";
	private static SimpleDateFormat _sdf_incoming = new SimpleDateFormat(DATE_TIME_PATTERN_INCOMING);
	private static SimpleDateFormat _sdf_to_oracle = new SimpleDateFormat(DATE_TIME_PATTERN_TO_ORACLE);
	private static SimpleDateFormat _sdf_from_oracle = new SimpleDateFormat(DATE_TIME_PATTERN_FROM_ORACLE);

	@Autowired
	OrderSVCApi orderSvc;
	@Autowired
	AddressSVCApi addressSvc;

	@RequestMapping(value = ORDERS_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView getOrders(Model model, HttpServletRequest request) {

		// add request validation;

		String orderId = request.getParameter("id");
		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		String searchCriteria = "";
		if (orderId != null) {
			searchCriteria = "where order_id=" + orderId;
		} else {
			try {

				Date fromDate = _sdf_incoming.parse(fromdate);// coming in IST
				Date toDate = _sdf_incoming.parse(todate);// coming in IST
				searchCriteria = "where order_date>=to_date('" + _sdf_from_oracle.format(dateToUTCFromIST(fromDate))
						+ "','yyyy-MM-dd HH24:MI')" + " and order_date<=to_date('"
						+ _sdf_to_oracle.format(dateToUTCFromIST(toDate))
						+ "','yyyy-MM-dd HH24:MI') order by order_date desc";
				
			} catch (ParseException e) {
				
				e.printStackTrace();
				return null;
			}

		}
		HashMap<Integer, Order> sessionOrderMap = new HashMap<Integer, Order>();
		List<Order> orders = orderSvc.getOrders(searchCriteria);

		for (Order order : orders) {
			sessionOrderMap.put(order.getOrderId(), order);
			order.setFormateddeliveryDate(_sdf_incoming.format(dateFromUTCToIST(order.getDeliveryDate())));
			order.setFormatedOrderDate(_sdf_incoming.format(dateFromUTCToIST(order.getOrderDate())));
		}
		request.getSession().setAttribute("session_order_map", sessionOrderMap);

		model.addAttribute("orders", orders);

		return new ModelAndView("/ordersTable");

	}

	@RequestMapping(value = SUB_ORDERS_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView getSubOrders(Model model, HttpServletRequest request) {

		String orderId = request.getParameter("id");
		String fromdate = request.getParameter("fromdate");
		String todate = request.getParameter("todate");
		String searchCriteria = "";
		if (orderId != null) {
			searchCriteria = "where order_id=" + orderId;
		} else {
			try {

				Date fromDate = _sdf_incoming.parse(fromdate);// coming in IST
				Date toDate = _sdf_incoming.parse(todate);// coming in IST
				searchCriteria = "where subscriptionOrderDate>=to_date('"
						+ _sdf_from_oracle.format(dateToUTCFromIST(fromDate)) + "','yyyy-MM-dd HH24:MI')"
						+ " and subscriptionOrderDate<=to_date('" + _sdf_to_oracle.format(dateToUTCFromIST(toDate))
						+ "','yyyy-MM-dd HH24:MI') order by subscriptionOrderDate desc";
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}

		}

		List<SubscriptionOrder> orders = orderSvc.getSubOrders(searchCriteria);

		for (SubscriptionOrder suborder : orders) {

			suborder.setFormatedSubEndDate(_sdf_incoming.format(dateFromUTCToIST(suborder.getSubscriptionEndDate())));
			suborder.setFormatedSubStartDate(
					_sdf_incoming.format(dateFromUTCToIST(suborder.getSubscriptionStartDate())));
			suborder.setFormatedSubOrderDate(
					_sdf_incoming.format(dateFromUTCToIST(suborder.getSubscriptionOrderDate())));
		}
		model.addAttribute("suborders", orders);
		return new ModelAndView("/subOrdersTable");

	}

	@RequestMapping(value = UPDATE_ORDERS_REQUEST_MAPPING_URL, method = RequestMethod.POST)
	@ResponseBody
	public String updateOrders(HttpServletRequest request) {

		// add request validation;
		String resp = "";

		String orderId = request.getParameter("id");
		String orderStatus = request.getParameter("status");
		String deliveredBy = request.getParameter("deliverBy");
		String dd = request.getParameter("dd");
		// do validation

		String updateCriteria = "";
		if (orderStatus != null && orderStatus != "") {
			if (updateCriteria == "")
				updateCriteria = "set order_status='" + orderStatus + "'";
			else
				updateCriteria += " ,order_status='" + orderStatus + "'";
		}
		if (deliveredBy != null && deliveredBy != "") {
			if (updateCriteria == "")
				updateCriteria = "set delivered_by='" + deliveredBy + "'";
			else
				updateCriteria += " ,delivered_by='" + deliveredBy + "'";
		}
		try {
			if (dd != null && dd != "") {
				if (updateCriteria == "")
					updateCriteria = "set delivery_date='" + _sdf_from_oracle.format(_sdf_incoming.parse(dd)) + "'";
				else
					updateCriteria += " ,delivery_date='" + _sdf_from_oracle.format(_sdf_incoming.parse(dd)) + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

		updateCriteria += " where order_id=" + orderId;
		String serviceresp = orderSvc.updateOrders(updateCriteria);

		if (serviceresp.equals("GDB_200"))
			return "GA_200";

		return "GA_500";

	}

	@GetMapping(value = PRINT_ORDERS_BILL_REQUEST_MAPPING_URL)
	@ResponseBody
	public ModelAndView printOrderBill(Model model, @PathVariable("id") int id, HttpServletResponse resp) {

		Optional<Order> response = orderSvc.getOrder(id);

		if (response.isPresent()) {

			Order order = response.get();
			// get address
			Address add;
			try {
				if (order.getOrderCatagory().getKey() == 3) {
					// guestOrder
					add = order.getGuestuser().getAddress();
				} else {
					add = addressSvc.getAddressById(order.getDeliveryAddressId());
				}
				model.addAttribute("address", add);

			} catch (Exception e) {
				resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return null;
			}

			model.addAttribute("order", order);
			// TODO fix needed
			model.addAttribute("meal", order.getMeals().get(0));

			try {
				// todo fix needed here
				double price = (order.getMeals().get(0).getMealPrice() / 1.05);
				model.addAttribute("couponApplied", false);
				model.addAttribute("mp", String.format("%.2f", price));
				if (order.isCouponApplied()) {
					model.addAttribute("couponApplied", true);
					price = price - order.getDiscountAmount();
					model.addAttribute("discount", String.valueOf(order.getDiscountAmount()));
				}

				double cgst = (price * 2.5 / 100);
				float total = (float) (price + 2 * cgst);
				model.addAttribute("tax", String.format("%.2f", cgst));
				model.addAttribute("total", String.format("%.2f", total));
				System.out.println("End");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new ModelAndView("orderBillPrint");
		} else {

			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}

	}

	public Date dateFromUTCToIST(Date date) {
		return new Date(date.getTime() + TimeZone.getTimeZone("Asia/Kolkata").getOffset(date.getTime()));
	}

	public Date dateToUTCFromIST(Date date) {
		return new Date(date.getTime() - TimeZone.getTimeZone("UTC").getOffset(date.getTime()));
	}
}
